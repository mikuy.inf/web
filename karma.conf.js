// Karma configuration
// Generated on Wed Feb 22 2023 08:08:10 GMT-0500 (hora de Ecuador)

module.exports = function (config) {
  process.env.CHROME_BIN = require('puppeteer').executablePath(); // IMPORTANT!
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],

    // list of files / patterns to load in the browser
    files: [
      //'public/js/*.js',
      //'"test/**/*Spec.js"',
      //'js/*.js',
      //'"js/*.js/"',
      //'test/*Spec.js',
      //'test/*spec.ts',
      //'test/*Spect.ts',
      //'ts/*.ts',
      //'**/*.js',
      //'test/**/*Spec.js',
      //'**/*Spec.js'
      "**/*.spec.ts",
      "**/*.d.ts",
      "**/*.ts"
    ],


    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://www.npmjs.com/search?q=keywords:karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://www.npmjs.com/search?q=keywords:karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://www.npmjs.com/search?q=keywords:karma-launcher
    /*browsers: ['Chrome', 'Firefox', 'IE'],
    customLaunchers: {
      ChromeHeadlessCI: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },*/
    browsers: ['Chrome','ChromeHeadlessNoSandbox'],
    content_copybrowsers: ['ChromeHeadlessCI'], 
    singleRun: false,
    restartOnFileChange: true,
    customLaunchers: {
      ChromeHeadlessCI: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'],
        binary: process.env.CHROME_BIN
      }
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits

    // Concurrency level
    // how many browser instances should be started simultaneously
    concurrency: Infinity
  })
}
