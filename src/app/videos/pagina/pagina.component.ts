
import { Component, OnInit, HostBinding } from '@angular/core';
import { Videos } from 'src/app/models/Videos';
import { VideosService } from 'src/app/services/videos/videos.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Funciones } from 'src/app/models/Funciones';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import jwt_decode from "jwt-decode";
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  page = 1;
  btnModificar = 0;
  btnCrear = 1;
  pageSize = 12;
  videos: any = [];
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;

  video: Videos = {
    id: 0,
    nombre: '',
    url: '',
    usuario: 0,
  };



  videoBlanco: Videos = {
    id: 0,
    nombre: '',
    url: '',
    usuario: 0,
  };

  closeResult = '';
  nombreEliminar = '';

  nombreB: string = '';

  longNombre: number | undefined = 0;
  longURL: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strURL: string = "* URL Obligatoria";
  errorNombre: boolean = false;
  errorURL: boolean = false;
  private idUs: number = 0;


  constructor(private videosService: VideosService,
    public modal: NgbModal,
    private funcionesService: FuncionesService,
    private router: Router,
    private mantenimientoService: MantenimientoService,
    private usuariosService: UsuariosService
  ) { }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {

      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);
      this.idUs = datos[0]['id'];

      this.litarVideos();
      this.cargarFuncion();


    } else {
      this.router.navigate(['/login']);
    }


  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Videos';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.videos.forEach((element, index, array) => {
      info.push([element.id, element.nombre, element.url])

    })

    autoTable(doc, {
      head: [["Id", "Nomrbe", "URL"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_videos.pdf");
  }


  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'videos').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }


  controlErrores(): boolean {

    this.longNombre = this.video['nombre']?.length;
    this.longURL = this.video['url']?.length;

    if (this.longNombre == 0) {
      this.errorNombre = true;
      this.strNombre = "El Nombre es Obligatorio";
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longURL == 0) {
      this.errorURL = true;
      this.strURL = "La URL es Obligatoria";
      return false;
    } else {
      this.errorURL = false;

    }

    return true;
  }



  litarVideos() {
    this.videosService.getVideos().subscribe(
      res => {
        this.videos = res;
        //this.roles=this.roles.slice(0,1);
      },
      err => console.error(err)
    );
  }

  eliminarVideo(id: string) {
    this.usuariosService.controlSesion();
    this.videosService.eliminarVideo(id)
      .subscribe(
        res => {

          this.guardarMantenimiento("Eliminar", "Recetas");
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.video.id;
      this.video.usuario = this.idUs;
      this.videosService.guardarVideo(this.video)
        .subscribe(
          res => {
            this.guardarMantenimiento("Crear", "Recetas");
            window.location.reload();
            //this.litarVideos();

          },
          err => console.error(err)
        )
    }
  }

  seleccionarVideo(id: string) {
    this.usuariosService.controlSesion();
    this.videosService.getVideo(id).subscribe(
      res => {
        this.btnModificar = 1;
        this.btnCrear = 0;
        this.video = res[0];
      },
      err => console.error(err)
    );
  }

  actualizar() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {

      this.videosService.actualizarVideo(this.video.id, this.video)
        .subscribe(
          res => {
            //console.log(res);
            this.guardarMantenimiento("Actualizar", "Recetas");
            window.location.reload();
            //this.litarVideos();
            //this.video = this.videoBlanco;
          },
          err => console.error(err)
        )
    }
  }

  cancelar() {
    this.usuariosService.controlSesion();
    this.video = { ...this.videoBlanco};
    this.btnModificar = 0;
    this.btnCrear = 1;
  }

  limpiar() {
    this.usuariosService.controlSesion();
    this.video = { ...this.videoBlanco};
    this.btnModificar = 0;
    this.btnCrear = 1;
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarVideo(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }



  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.videosService.getVideoXNombre(this.nombreB).subscribe(
        res => {
          this.videos = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => {
          //console.error(err)
          this.videos = [];
        }
      );
    } else {
      this.litarVideos();
    }

  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }

}
