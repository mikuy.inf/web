import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VentasF } from 'src/app/models/VentasF';
import { VentasD } from 'src/app/models/VentasD';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { VentasFService } from 'src/app/services/ventasf/ventasf.service';
import { VentasDService } from 'src/app/services/ventasd/ventasd.service';
import { DomSanitizer } from '@angular/platform-browser';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import { forkJoin, combineLatest } from 'rxjs';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  page = 1;
  pageSize = 12;
  valIdentificacion: string = '';
  valOrden: string = '';

  ventasf: any = [];
  ventasfR: any = [];
  ventasd: any = [];
  nombreEliminar = '';
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;
  public compras = 0;
  private idUs: number = 0;
  private vaIdVenta: number = 0;
  public vaSubTo: number = 0;
  public vaTo: number = 0;
  public vaIva: number = 0;
  public estFactura: number = 0;
  public idSelFactura: number = 0;
  valorSeleccionado: string ="4" 
  
  edit: boolean = false;

  envVentaF: VentasF = {
    id: 0,
    usuario: 0,
    estado: 0,
    subtotal: 0,
    total: 0,
    fpago: '',
    iva: 0,
    cedula: '',
    telefono: ''
  };
  envVentaD: VentasD = {
    id: 0,
    ventasf: 0,
    producto: 0,
    precio: 0,
    cantidad: 0,
    iva: 0
  };


  public archivos: any = []

  nombreB: string = '';
  identificacionB: string = '';
  ordenB: string = '';
  vaCedula: string = '';
  vaCliente: string = '';
  vaPasaporte: string = '';
  vaTelefono: string = '';
  vaFInicio: string = '';
  vaFFin: string = '';
  errorFecha: string = '';
  estadoFecha: Boolean = false;
  public listaIVA: any = [];

  constructor(private ventasDService: VentasDService,
    private ventasFService: VentasFService,
    private funcionesService: FuncionesService,
    public modal: NgbModal,
    private router: Router,
    private sanitizer: DomSanitizer,
    private usuariosService: UsuariosService,
    private mantenimientoService: MantenimientoService,) { }



  ngOnInit(): void {
    const token = localStorage.getItem('token');

    if (token) {
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];
      this.usuariosService.controlSesion();
    this.litarVentasF();
    


    this.cargarFuncion();}
    else {
      this.router.navigate(['/login']);
    }
  }


  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'producto').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  buscarPorFechas() {
    this.usuariosService.controlSesion();
    
    this.estadoFecha = false
    if (this.vaFInicio.length > 0) {
      if (this.vaFFin.length <= 0) {
        this.errorFecha = "Las fechas no pueden estar vacías"
        this.estadoFecha = true
        return;
      }
    }
    if (this.vaFFin.length > 0) {
      if (this.vaFInicio.length <= 0) {
        this.errorFecha = "Las fechas no pueden estar vacías"
        this.estadoFecha = true
        return;
      }
    }

    if (this.vaFInicio.length <= 0 && this.vaFFin.length <= 0) {
      this.ventasFService.getVentasFsinFecha(this.identificacionB,this.valorSeleccionado,this.ordenB).subscribe(
        res => {
          this.ventasf = res;
          this.litarVentasRsinFecha();
        },
        err => { this.ventasf = [];
        }
      );
    }else{
        this.estadoFecha = false
        var envfinicio = this.vaFInicio['year'] + "-" + this.vaFInicio['month'] + "-" + this.vaFInicio['day'];
        var envffin = this.vaFFin['year'] + "-" + this.vaFFin['month'] + "-" + this.vaFFin['day'];
        let fIn = new Date(envfinicio);
        let fFin = new Date(envffin);
        if (fIn.getTime() > fFin.getTime()) {
          this.errorFecha = "Las fecha de Inicio no puede ser mayor a la Fecha Final"
          this.estadoFecha = true
          return;
        }
      
        this.ventasFService.getVentasFXFecha(envfinicio, envffin, this.identificacionB,this.valorSeleccionado,this.ordenB).subscribe(
          res => {
            this.ventasf = res;
            this.litarVentasRxFecha(envfinicio, envffin);
          },
          err => { this.ventasf = [];
          }
        );
      }

  }

  buscarTodos() {
    window.location.reload();
  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Ventas Realizadas';
    const footer = 'Page';
    doc.text(header, 200, 45, { baseline: 'top' });
    let info = []
    let sumTotal = 0
    let margin = { top: 70 };

    this.ventasfR.forEach(async (element, index, array) => {
      
      //  info.push([element.cliente, element.id, element.fpago, element.cedula,element.pasaporte , element.telefono, element.subtotal, element.iva, element.total])
      let rowData = [element.cliente, element.id, element.fpago, element.cedula, element.pasaporte, element.telefono, element.subtotal, element.iva, element.total];
      if(Number(element.cod1)==1){
        sumTotal = sumTotal + Number(element.total)
        
      }
      if (rowData[0] !== "") {
        rowData = rowData.map((value) => {
          return { content: value, styles: { textColor: "#ffffff", fillColor: "#508AA7" } };
        });
      }
      info.push(rowData);
    });
    info.push(["", "", "", "", "", "", "", "", "______"])
    info.push(["", "", "", "", "", "", "", "TOTAL:", sumTotal.toFixed(2)])
    autoTable(doc, {
      head: [["Cliente", "# Orden", "Fecha de Pago", "Cédula", "Pasaporte", "Teléfono", " ", " ", "Total"]],
      body: info,
      margin: margin,
      showHead: false,
      
    });

    
    doc.save("reporte_ventas.pdf");
  }

 
  litarVentasF() {
    this.usuariosService.controlSesion();
    this.ventasFService.getVentasF().subscribe(
      res => {
        this.ventasf = res;
        this.litarVentasR();
      },
      err => console.error(err)
    );
  }


litarVentasR() {
    
    this.ventasFService.getVentasFR().subscribe(
      res => {
        this.ventasfR = res;
      },
      err => console.error(err)
    );
  }

  litarVentasRxFecha(envfinicio:any, envffin:any) {
    
    this.ventasFService.getVentasFXFechaR(envfinicio, envffin, this.identificacionB,this.valorSeleccionado,this.ordenB).subscribe(
      res => {
        this.ventasfR = res;
        console.log(this.ventasfR)
      },
      err => console.error(err)
    );
  }

  litarVentasRsinFecha() {
    
    this.ventasFService.getVentasFsinFechaR(this.identificacionB,this.valorSeleccionado,this.ordenB).subscribe(
      res => {
        this.ventasfR = res;
      },
      err => console.error(err)
    );
  }


  seleccionarVentasF(id: string, subtotal: string, iva: string, total: string, cedula: string, pasaporte: string, cliente: string, telefono: string, estado: string) {
    this.usuariosService.controlSesion();
    this.ventasDService.getVentasDSeleccionado(id).subscribe(
      res => {
        this.compras = 1;
        this.vaSubTo = Number(subtotal);
        this.vaTo = Number(total);
        this.vaIva = Number(iva);
        this.estFactura = Number(estado);
        this.idSelFactura = Number(id);
        this.vaCedula = cedula;
        this.vaPasaporte = pasaporte;
        this.vaCliente = cliente;
        this.vaTelefono = telefono;
        this.ventasd = res;
      },
      err => { this.compras = 0; }
    );
    this.litarVentasIVA(id);
  }

  actualizar() {


    this.usuariosService.controlSesion();
    this.ventasFService.actualizarVentasFPagado(this.idSelFactura,this.idUs.toString())
    .subscribe(
      res => {
        //console.log(res);
        if (res['ok'] === false && res['mensaje'].includes('error stock')) {
          alert("No hay sufiente stock para esta venta, primero verificar el stock")
      } else {
        if (res['ok'] === true && res['mensaje'].includes('Proceso Correcto')) {
          this.guardarMantenimiento("Actualizar", "Ventas");
          window.location.reload();
        } else {
          console.log(res['mensaje']);
        }
      }
        

      },
      err => console.error(err)
    )
  }

  actualizarC() {

    this.usuariosService.controlSesion();
    this.ventasFService.actualizarVentasFPagadoEliminar(this.idSelFactura,this.idUs.toString())
    .subscribe(
      res => {
        //console.log(res);
        this.guardarMantenimiento("Cancelar", "Ventas");
        window.location.reload();

      },
      err => console.error(err)
    )
  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }

  litarVentasIVA(id: string) {
    try {
      this.ventasDService.getVentasDSeleccionadoIVA(id).subscribe(
        res => {
          console.log(res)
          this.listaIVA = res;

        },
        err => {
          console.log(err)
        }
      );
    }
    catch { }
  }

  public buscarxIdentificacion() {
    this.usuariosService.controlSesion();
    if (this.identificacionB.length > 0) {
      this.ventasFService.getVentaIdentificacion(this.identificacionB).subscribe(
        res => {
          this.ventasf = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => console.error(err)
      );
    } else {
      this.litarVentasF();
    }

  }

  public buscarxOrden() {
    this.usuariosService.controlSesion();
    if (this.ordenB.length > 0) {
      this.ventasFService.getVentaFOrden(this.ordenB).subscribe(
        res => {
          this.ventasf = res;
 
        },
        err => console.error(err)
      );
    } else {
      this.litarVentasF();
    }

  }

  cancelar() {
    //this.ventasf = this.ventasfBlanco;
    // this.btnModificar = 0;
    //this.btnCrear = 1;
  }

  procesoSeleccionado(event: Event) {
    this.valorSeleccionado = (event.target as HTMLSelectElement).value;
    
  }

}
