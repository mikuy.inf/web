import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PaginaComponent } from './pagina.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JWT_OPTIONS } from '@auth0/angular-jwt';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';

describe('Categoría', () => {

    
    let categoriaService: CategoriaService;
    let fixture: ComponentFixture<PaginaComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
            ],
            declarations: [
                PaginaComponent,
                
            ],
            providers: [
              { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
              JwtHelperService,
              CategoriaService,
            ]
            
        }).compileComponents();

      
        
    });

   
   

    it('Controlar crear Ventana Categoría', () => {
        const fixture = TestBed.createComponent(PaginaComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();

    });

  
    it('Cargar las categorías al iniciar la ventana', () => {
        const fixture = TestBed.createComponent(PaginaComponent);
        const app = fixture.componentInstance;
        app.litarCategoria();
        expect(app.categorias.length).toBe(0);
      });
      
});
