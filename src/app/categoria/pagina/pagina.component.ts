import { Component, OnInit, HostBinding,ElementRef } from '@angular/core';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Funciones } from 'src/app/models/Funciones';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { Categorias } from 'src/app/models/Categorias';
import { DomSanitizer } from '@angular/platform-browser';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { FormsModule } from '@angular/forms';


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent {

  @HostBinding('class') classes = 'row';
  private idUs: number = 0;
  page = 1;
  pageSize = 12;
  roles: any = [];
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;

  categorias: any = [];

  categoria: Categorias = {
    id: 0,
    nombre: '',
    descripcion: '',
    imagen: ''
  };

  funcion: Funciones = {
    id: 0,
    rol: 0,
    tabla: '',
    crear: 0,
    actualizar: 0,
    leer: 0,
    eliminar: 0
  };

  categoriaBlanco: Categorias = {
    id: 0,
    nombre: '',
    descripcion: '',
    imagen: '',
  };
  btnModificar = 0;
  btnCrear = 1;
  closeResult = '';
  nombreEliminar = '';
  public archivos: any = []

  nombreB: string = '';

  longNombre: number | undefined = 0;
  longDescripcion: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strDescripcion: string = "* Descripción Obligatorio";
  errorNombre: boolean = false;
  errorDescripcion: boolean = false;

  constructor(private categoriasService: CategoriaService,
    public modal: NgbModal,
    private funcionesService: FuncionesService,
    private mantenimientoService: MantenimientoService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private usuariosService: UsuariosService,
    private el: ElementRef) { }

  ngOnInit(): void {
    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];
      this.litarCategoria();
      this.cargarFuncion();
    } else {
      this.router.navigate(['/login']);
    }

  }

  capturarImagen(event: any): any {
    this.usuariosService.controlSesion();
    this.limpiarImage();
    const imagenCapturada = event.target.files[0];
    this.extraerBase64(imagenCapturada).then((imagen: any) =>
      this.categoria.imagen = imagen.base
    )
    this.archivos.push(imagenCapturada)
  }

  limpiarImage(): any {
    this.categoria.imagen = '';
    this.archivos = [];
  }

  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    // try {
    const unsafeImg = window.URL.createObjectURL($event);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    const reader = new FileReader();
    reader.readAsDataURL($event);
    reader.onload = () => {
      resolve({
        base: reader.result
      });

    };
    reader.onerror = error => {
      resolve({
        base: null
      });
    };

    /*} catch (e) {
     
      return null;
    }*/
  })

  controlErrores(): boolean {

    this.longNombre = this.categoria['nombre']?.length;
    this.longDescripcion = this.categoria['descripcion']?.length;

    if (this.longNombre == 0) {
      this.errorNombre = true;
      this.strNombre = "El Nombre es Obligatorio";
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longDescripcion == 0) {
      this.errorDescripcion = true;
      this.strDescripcion = "La Descripcion es Obligatoria";
      return false;
    } else {
      this.errorDescripcion = false;

    }

    return true;
  }

  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'categoria').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarCategoria() {
    this.categoriasService.getCategorias().subscribe(
      res => {
        this.categorias = res;
      },
      err => console.error(err)
    );
  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Categorías';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.categorias.forEach((element, index, array) => {
      info.push([element.id, element.nombre, element.descripcion])

    })

    autoTable(doc, {
      head: [["Id", "Nombre", "Descripción"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_categorias.pdf");
  }

  eliminar(id: string) {
    this.usuariosService.controlSesion();
    this.categoriasService.eliminarCategoria(id)
      .subscribe(
        res => {
          this.guardarMantenimiento("Eliminar", "Categoría");
          window.location.reload();
        },
        err => {
          alert("No puede eliminar, dato tiene relación con otras tablas");
          console.log();
        }
      )
  }

  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.categoria.id;
      
      this.categoriasService.guardarCategoria(this.categoria,this.archivos[0])
        .subscribe(
          res => {


            this.guardarMantenimiento("Crear", "Categoría");
            window.location.reload();
          },
          err => console.error(err)
        )
    }
  }

  seleccionarCategoria(id: string) {
    this.usuariosService.controlSesion();
    this.categoriasService.getCategoria(id).subscribe(
      res => {
        this.btnModificar = 1;
        this.btnCrear = 0;
        this.categoria = res[0];
      },
      err => console.error(err)
    );
  }

  actualizar() {


    this.usuariosService.controlSesion();
    this.categoriasService.actualizarCategoria(this.categoria,this.archivos[0])
      .subscribe(
        res => {
          this.guardarMantenimiento("Actualizar", "Categoría");
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  cancelar() {
    this.usuariosService.controlSesion();
    this.categoria = { ...this.categoriaBlanco};
    this.limpiarImage();
    const inputImagen = this.el.nativeElement.querySelector('#imagen');
    inputImagen.value = null;
    this.btnModificar = 0;
    this.btnCrear = 1;
  }


  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminar(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.categoriasService.getCategoriasXNombre(this.nombreB).subscribe(
        res => {
          this.categorias = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => {
          //console.error(err)
          this.categorias = [];
        }
      );
    } else {
      this.litarCategoria();
    }

  }

  guardarMantenimiento(proceso: String, tabla: String) {
    this.usuariosService.controlSesion();
    try {
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }


}
