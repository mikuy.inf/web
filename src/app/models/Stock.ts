export interface Stock {
    id?: number,
    producto?: number,
    nombreproducto?: string,
    usuario?: number,
    nombreusuario?: string,
    fecha?: string,
    proceso?: number,
    valor?: number,
}