export interface Mantenimiento {
    id?: number,
    fecha?: string,
    proceso?: string,
    tabla?: string,
    usuario?: number,
}