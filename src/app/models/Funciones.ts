export interface Funciones {
    id?: number,
    rol?: number,
    tabla?: string,
    crear?: number,
    leer?: number,
    actualizar?: number,
    eliminar?: number,
    nombreRol?: string,   
}