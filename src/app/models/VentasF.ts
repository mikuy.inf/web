export interface VentasF {
    id?: number;
    usuario?: number;
    estado?: number;
    subtotal?: number;
    total?: number;
    fpago?: string;
    iva?: number;
    cedula?: string;
    pasaporte?: string;
    telefono?: string;

}