import { NonNullableFormBuilder } from "@angular/forms";

export interface Productos {
    id?: number,
    nombrecategoria?: string,
    categoria?: number,
    nombre?: string,
    descripcion?: string,
    imagen?: string,
    precio?: string,
    estado?: number,
    cantidad?: number,
    descuento?: number,
    IVA?: number,
}