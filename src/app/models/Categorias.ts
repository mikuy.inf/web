export interface Categorias {
    id?: number;
    nombre?: string;
    descripcion?: string;
    imagen?: string,
    estado?: number;
}