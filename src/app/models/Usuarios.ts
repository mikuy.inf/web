export interface Usuarios {
    id?: number;
    nombre?: string;
    apellido?: string;
    email?: string;
    imagen?: string;
    direccion?: string;
    contrasena?: string;
    fingreso?: Date;

    session_token?: string;

}