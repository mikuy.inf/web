export interface UsuariosRoles {
    id?: number;
    usuario?: number;
    rol?: number;
    nomUsuario?: string;
    nomRol?: string;
}