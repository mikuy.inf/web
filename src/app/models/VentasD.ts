export interface VentasD {
    id?: number;
    ventasf?: number;
    producto?: number;
    precio?: number;
    cantidad?: number;
    iva?: number;
    descuento?: number;
}