
import { Component, OnInit, HostBinding } from '@angular/core';
import { Mantenimiento } from 'src/app/models/Mantenimiento';

import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Funciones } from 'src/app/models/Funciones';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  btnModificar = 0;
  btnCrear = 1;
  page = 1;
  pageSize = 75;
  mantenimiento: any = [];
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;

  mante: Mantenimiento = {
    id: 0,
    usuario: 0,
    fecha: '',
    tabla: '',
    proceso: '',
  };

  funcion: Funciones = {
    id: 0,
    rol: 0,
    tabla: '',
    crear: 0,
    actualizar: 0,
    leer: 0,
    eliminar: 0
  };



  closeResult = '';
  nombreEliminar = '';

  nombreB: string = '';

  longNombre: number | undefined = 0;
  longDescripcion: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strDescripcion: string = "* Descripción Obligatorio";
  errorNombre: boolean = false;
  errorDescripcion: boolean = false;
  private idUs: number = 0;


  constructor(public modal: NgbModal,
    private funcionesService: FuncionesService,
    private mantenimientoService: MantenimientoService,
    private router: Router
  ) { }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {

      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];

      this.litarMantenimiento();
      this.cargarFuncion();
    } else {
      this.router.navigate(['/login']);
    }

  }


  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'mantenimiento').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarMantenimiento() {
    console.log('cargando')
    this.mantenimientoService.getMantenimiento().subscribe(
      res => {
        this.mantenimiento = res;
        //this.roles=this.roles.slice(0,1);
      },
      err => console.error(err)
    );
  }

  generarPdf() {
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Mantenimiento';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.mantenimiento.forEach((element, index, array) => {
      info.push([element.id, element.tabla, element.proceso, element.usuario, element.fecha])

    })

    autoTable(doc, {
      head: [["Id", "Tabla", "Proceso", "Usuario", "Fecha"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_mantenimiento.pdf");
  }

  public buscarxNombre() {
    if (this.nombreB.length > 0) {
      this.mantenimientoService.getMantenimientoXNombre(this.nombreB).subscribe(
        res => {
          this.mantenimiento = res;
        },
        err => {
          //console.error(err)
          this.mantenimiento = []
        }
      );
    } else {
      this.litarMantenimiento();
    }

  }
}
