import { Component, OnInit, HostBinding , ElementRef} from '@angular/core';
import { UsuariosService } from '../../services/usuarios/usuarios.service'
import { Roles } from 'src/app/models/Roles'
import { Usuarios } from 'src/app/models/Usuarios';
import { RolesService } from 'src/app/services/roles/roles.service';

import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';
//import {Md5} from 'ts-md5/dist/esm/md5';
import * as fs from 'fs'
import * as CryptoJS from 'crypto-js';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import validator from 'validator';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {
  @HostBinding('class') classes = 'row';
  //md5 = new Md5();
  usuarios: any = [];
  page = 1;
  pageSize = 12;
  roles: any = [];
  btnModificar = 0;
  btnCrear = 1;
  private idUs: number = 0;

  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;
  ocultar = 0;

  usuario: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(Date.now() + 24 * 60 * 60 * 1000),

  }

  usuarioBlanco: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(Date.now() + 24 * 60 * 60 * 1000),

  }



  public archivos: any = []
  public previsualizacion: string | undefined = ''

  nombreB: string = '';
  nombreEliminar = '';

  longNombre: number | undefined = 0;
  longApellido: number | undefined = 0;
  longEmail: number | undefined = 0;
  longCedula: number | undefined = 0;
  longTelefono: number | undefined = 0;
  longDireccion: number | undefined = 0;
  longContrasena: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strApellido: string = "* Apellido Obligatorio";
  strEmail: string = "* Email Obligatorio";
  strCedula: string = "* Cédula Obligatoria";
  strTelefono: string = "* Teléfono Obligatorio";
  strDireccion: string = "* Dirección Obligatorio";
  strContrasena: string = "* Contraseña Obligatorio";
  strRol: string = "* Rol Obligatorio";

  errorNombre: boolean = false;
  errorApellido: boolean = false;
  errorEmail: boolean = false;
  errorCedula: boolean = false;
  errorTelefono: boolean = false;
  errorDireccion: boolean = false;
  errorContrasena: boolean = false;
  errorRol: boolean = false;

  constructor(private funcionesService: FuncionesService,
    private usuariosService: UsuariosService,
    private rolesService: RolesService,
    private sanitizer: DomSanitizer,
    private mantenimientoService: MantenimientoService,
    public modal: NgbModal,
    private router: Router,
    private el: ElementRef
  ) { }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];
      this.litarUsuario();
      this.litarRoles();
      this.cargarFuncion();
    } else {
      this.router.navigate(['/login']);
    }


  }

  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'usuarios').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarUsuario() {
    this.usuariosService.getUsuarios().subscribe(
      res => {
        this.usuarios = res;
      },
      err => {
        console.error(err)
        //window.location.reload();
      }
    );
  }

  generarPdf() {
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Usuarios';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.usuarios.forEach((element, index, array) => {
      info.push([element.id, element.nombre, element.apellido, element.email, element.direccion])

    })

    autoTable(doc, {
      head: [["Id", "Nombre", "Apellido", "Email", "Dirección"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_usuarios.pdf");
  }

  litarRoles() {
    this.rolesService.getRoles().subscribe(
      res => {
        this.roles = res;
      },
      err => console.error(err)
    );
  }

  seleccionarUsuario(id: string) {
    this.usuariosService.controlSesion();
    this.limpiarImage();
    this.usuariosService.getUsuario(id).subscribe(
      res => {
        this.usuario = res[0];
        this.usuario.contrasena = ""
        this.btnModificar = 1;
        this.btnCrear = 0;

      },
      err => console.error(err)
    );
  }

  blobToBase64(blob: any) {
    return new Promise((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result);
      reader.readAsDataURL(blob);
    });
  }


  eliminarUsuario(id: string) {
    this.usuariosService.controlSesion();
    this.usuariosService.eliminarUsuario(id)
      .subscribe(
        res => {
          this.guardarMantenimiento("Eliminar", "Usuarios");
          window.location.reload();
        },
        err => {
          alert("No puede eliminar, dato tiene relación con otras tablas");
         // console.error(err)
        }
      )
  }
  actualizar() {

    this.usuariosService.controlSesion();
    //delete this.usuario.fingreso;
    //delete this.usuario.session_token;

    if (this.usuario.contrasena != '') {
      const en: string = this.usuario.contrasena as string;
      const hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(en));
      const md5 = hash.toString(CryptoJS.enc.Hex)

      this.usuario.contrasena = md5;

    } else {
      //delete this.usuario.contrasena
    }
    var email=1

    this.usuariosService.actualizarUsuario(this.usuario,this.archivos[0],email)
      .subscribe(
        res => {

          this.guardarMantenimiento("Actualizar", "Usuarios");
          window.location.reload();
        },
        err => console.error(err)
      )


  }
 
  cancelar() {
    
    this.usuariosService.controlSesion();
    this.limpiarImage();
    const inputImagen = this.el.nativeElement.querySelector('#imagen');
    inputImagen.value = null;
    this.usuario = this.usuarioBlanco;
    this.btnModificar = 0;
    this.btnCrear = 1;
   
  }

  limpiar(){
    this.usuariosService.controlSesion();
    this.limpiarImage();
    const inputImagen = this.el.nativeElement.querySelector('#imagen');
    inputImagen.value = null;
    this.usuario = { ...this.usuarioBlanco };
    this.btnModificar = 0;
    this.btnCrear = 1;
    console.log(this.usuarioBlanco)
  }

  controlErrores(): boolean {

    this.longNombre = this.usuario['nombre']?.length;
    this.longApellido = this.usuario['apellido']?.length;
    this.longEmail = this.usuario['email']?.length;
    //this.longCedula = this.usuario['cedula']?.length;
    this.longDireccion = this.usuario['direccion']?.length;
    //this.longTelefono = this.usuario['telefono']?.length;
    this.longContrasena = this.usuario['contrasena']?.length;

    if (this.longNombre == 0) {
      this.errorNombre = true;
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longApellido == 0) {
      this.errorApellido = true;
      return false;
    } else {
      this.errorApellido = false;

    }

    if (this.longEmail == 0) {
      this.errorEmail = true;
      this.strEmail = "* Email Obligatorio";
      return false;
    } else {
      this.errorEmail = false;

    }

    const valEma: string = this.usuario['email'] as string;

    if (!this.esEmailValido(valEma)) {
      this.errorEmail = true;
      this.strEmail = "El Correo Electrónico es Incorrecto";
      return false;
    } else {
      this.errorEmail = false;

    }

    /* if (this.longCedula == 0) {
       this.errorCedula = true;
       return false;
     } else {
       this.errorCedula = false;
 
     }
 */
    if (this.longDireccion == 0) {
      this.errorDireccion = true;
      return false;
    } else {
      this.errorDireccion = false;

    }

    /*if (this.longTelefono == 0) {
      this.errorTelefono = true;
      return false;
    } else {
      this.errorTelefono = false;

    }
*/
    if (this.longContrasena == 0) {
      this.errorContrasena = true;
      return false;
    } else {

      const contContr = validator.isStrongPassword(this.usuario['contrasena'], {
        minLength: 8,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1,
        returnScore: false,
        pointsPerUnique: 1,
        pointsPerRepeat: 0.5,
        pointsForContainingLower: 10,
        pointsForContainingUpper: 10,
        pointsForContainingNumber: 10,
        pointsForContainingSymbol: 10,
      })
      if (contContr == false) {
        console.log("Entro");
        this.errorContrasena = true;
        this.strContrasena = "La Contraseña es insegura. (long 8, 1+ Num, 1+ Mayus, 1+ Minus, 1+ Simb)";
        return false;
      } else {
        this.errorContrasena = false;

      }


    }





    return true;
  }


  esEmailValido(email: string): boolean {
    let mailValido = false;
    'use strict';

    var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (email.match(EMAIL_REGEX)) {
      mailValido = true;
    }
    return mailValido;
  }

  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.usuario.id;

      delete this.usuario.fingreso;
      this.usuario.session_token = '';
      const en: string = this.usuario.contrasena as string;
      const hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(en));
      const md5 = hash.toString(CryptoJS.enc.Hex)
      this.usuario.contrasena = md5;
      this.usuariosService.guardarUsuario(this.usuario,this.archivos)
        .subscribe(
          res => {

            this.usuariosService.getAuth(this.usuario.email, md5).
            subscribe((resp: any) => {

              const datos: any = jwt_decode(resp);
          
                  this.usuariosService.enviarToken(resp,this.usuario.email)
                  .subscribe(
                    res => {
                      this.guardarMantenimiento("Crear", "Usuarios");;
                       window.location.reload();
                    },
                    err => {
                     
                    }
                  )
             

            }, (err) => {

              this.errorEmail = true;
              this.strEmail = "El Correo Electrónico No está registrado";

            }
            );
            //this.guardarMantenimiento("Crear", "Usuarios");;
           // window.location.reload();
          },
          err => {
            this.errorEmail = true;
            this.strEmail = "El Correo Electrónico ya existe";
            this.usuario.contrasena = '';
          }
        )

    }
    //this.usuario.imagen=this.previsualizacion;


  }



  capturarImagen(event: any): any {
    this.usuariosService.controlSesion();
    this.limpiarImage();
    const imagenCapturada = event.target.files[0];
    this.extraerBase64(imagenCapturada).then((imagen: any) =>
      this.usuario.imagen = imagen.base
    )
    this.archivos.push(imagenCapturada)
  }


  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    // try {
    const unsafeImg = window.URL.createObjectURL($event);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    const reader = new FileReader();
    reader.readAsDataURL($event);
    reader.onload = () => {
      resolve({
        base: reader.result
      });

    };
    reader.onerror = error => {
      resolve({
        base: null
      });
    };

    /*} catch (e) {
     
      return null;
    }*/
  })


  /**
   * Limpiar imagen
   */

  limpiarImage(): any {
    this.usuario.imagen = '';
    this.archivos = [];
  }

  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.usuariosService.getUsuariosXNombre(this.nombreB).subscribe(
        res => {

          this.usuarios = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => {
          //console.error(err)
          this.usuarios = [];
        }
      );
    } else {
      this.litarUsuario();
    }

  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarUsuario(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }


}
