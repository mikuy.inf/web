import { Component, OnInit, HostBinding, ViewChild } from '@angular/core';
import { UsuariosService } from '../../services/usuarios/usuarios.service'
import { Roles } from 'src/app/models/Roles'
import { Usuarios } from 'src/app/models/Usuarios';
import { RolesService } from 'src/app/services/roles/roles.service';
import { Observable, Subject, merge, OperatorFunction } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

import { DomSanitizer } from '@angular/platform-browser';
//import {Md5} from 'ts-md5/dist/esm/md5';
import * as fs from 'fs'
import * as CryptoJS from 'crypto-js';
import { ModalDismissReasons, NgbModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import validator from 'validator';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosRolesService } from 'src/app/services/usuariosroles/usuariosroles.service';
import { UsuariosRoles } from 'src/app/models/UsuariosRoles';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {
  @HostBinding('class') classes = 'row';
  //md5 = new Md5();
  usuariosRoles: any = [];
  page = 1;
  pageSize = 12;
  roles: any = [];
  usuarios: any = [];
  btnModificar = 0;
  btnCrear = 1;
  private idUs: number = 0;

  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;
  ocultar = 0;

  usuario: UsuariosRoles = {
    id: 0,
    nomRol: '',
    nomUsuario: '',
    usuario: 0,
    rol: 0,
  }

  usuarioBlanco: UsuariosRoles = {
    id: 0,
    nomRol: '',
    nomUsuario: '',
    usuario: 0,
    rol: 0,
  }



  public archivos: any = []
  public previsualizacion: string | undefined = ''

  nombreB: string = '';
  nombreEliminar = '';

  longNombre: number | undefined = 0;
  longApellido: number | undefined = 0;
  longEmail: number | undefined = 0;
  longCedula: number | undefined = 0;
  longTelefono: number | undefined = 0;
  longDireccion: number | undefined = 0;
  longContrasena: number | undefined = 0;

  strUsuario: string = "* Usuario Obligatorio";

  strRol: string = "* Rol Obligatorio";
  nombreSeleccionado: string = "";

  errorUsuario: boolean = false;

  errorRol: boolean = false;
  public selectedValue: any;
  public filteredList: any = [];

  constructor(private funcionesService: FuncionesService,
    private usuariosService: UsuariosService,
    private usuariosRolesService: UsuariosRolesService,
    private rolesService: RolesService,
    private sanitizer: DomSanitizer,
    private mantenimientoService: MantenimientoService,
    public modal: NgbModal,
    private router: Router
  ) { this.litarUsuario(); }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];
      this.litarDatos();
      this.litarUsuario();
      this.litarRoles();
      this.cargarFuncion();
    } else {
      this.router.navigate(['/login']);
    }


  }


  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'usuarios').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarDatos() {
    this.usuariosRolesService.getUsuarios().subscribe(
      res => {
        this.usuariosRoles = res;
      },
      err => {
        console.error(err)
        //window.location.reload();
      }
    );
  }

  litarUsuario() {
    this.usuariosService.getUsuarios().subscribe(
      res => {
        this.usuarios = res;
        //console.log("****************")
      },
      err => {
        console.error(err)
        //window.location.reload();
      }
    );
  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Role - Usuarios';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.usuariosRoles.forEach((element, index, array) => {
      info.push([element.id, element.nomUsuario, element.nomRol])

    })

    autoTable(doc, {
      head: [["Id", "Usuario", "Rol"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_usuarios_roles.pdf");
  }

  litarRoles() {
    this.rolesService.getRoles().subscribe(
      res => {
        this.roles = res;
      },
      err => console.error(err)
    );
  }

  seleccionarUsuario(id: string) {
    this.usuariosService.controlSesion();
    this.usuariosRolesService.getUsuario(id).subscribe(
      res => {
        this.usuario = res[0];
        this.btnModificar = 1;
        this.btnCrear = 0;
        this.selectedCar = this.usuario.usuario.toString()
      },
      err => console.error(err)
    );
  }



  eliminarUsuario(id: string) {
    this.usuariosService.controlSesion();
    this.usuariosRolesService.eliminarUsuario(id)
      .subscribe(
        res => {
          this.guardarMantenimiento("Eliminar", "Usuarios_Roles");
          window.location.reload();
        },
        err => console.error(err)
      )
  }
  actualizar() {
    this.usuariosService.controlSesion();
    delete this.usuario.nomUsuario;
    delete this.usuario.nomRol;

    this.usuariosRolesService.actualizarUsuario(this.usuario.id, this.usuario)
      .subscribe(
        res => {

          this.guardarMantenimiento("Actualizar", "Usuarios_Roles");
          window.location.reload();
        },
        err => console.error(err)
      )


  }

  cancelar() {
    this.usuariosService.controlSesion();
    this.usuario = { ...this.usuarioBlanco};
    this.btnModificar = 0;
    this.btnCrear = 1;
    this.selectedCar="";
  }

  controlErrores(): boolean {
    if (this.selectedCar) {
      this.usuario.usuario = parseInt(this.selectedCar, 10);
    } else {
      this.usuario.usuario = 0;
    }
    if (this.usuario.usuario == null || this.usuario.usuario <= 0) {
      this.errorUsuario = true;

      return false;
    } else {
      this.errorUsuario = false;

    }


    if (this.usuario.rol == null || this.usuario.rol <= 0) {
      this.errorRol = true;

      return false;
    } else {
      this.errorRol = false;

    }




    return true;
  }

  guardarUsuario(event: any) {
    this.usuariosService.controlSesion();
    const id = event.target.value
    console.log(id)
    this.usuario.usuario = id
  }

  selectedCar = "";



  onChange = () => {
    //this.selectedCarObj = this.cars.find((c)=> c  .make==this.selectedCar);
    try {
      this.usuario.usuario = Number(this.selectedCar.toString());
    } catch {
      this.usuario.usuario = Number("0");
    }
    if (isNaN(this.usuario.usuario)) {
      this.usuario.usuario = Number("0");
    }
    console.log(this.usuario.usuario)
  }



  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.usuario.id;
      delete this.usuario.nomRol;
      delete this.usuario.nomUsuario;

      console.log(this.usuario)
      this.usuariosRolesService.guardarUsuario(this.usuario)
        .subscribe(
          res => {
            if (res['ok'] === false && res['mensaje'].includes('Duplicate entry')) {
                this.errorUsuario = true;
                this.strUsuario = "Usuario ya registrado";
            } else {
              if (res['ok'] === true && res['mensaje'].includes('Proceso Correcto')) {
               // this.guardarMantenimiento("Crear", "Usuarios_Roles");;
                window.location.reload();
               //console.log(res)

              } else {
                console.log(res['mensaje']);
              }
            }
           
            //this.guardarMantenimiento("Crear", "Usuarios_Roles");;
            //window.location.reload();
          },
          err => {
            console.log("error");
          }
        )

    }
    //this.usuario.imagen=this.previsualizacion;


  }


  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.usuariosRolesService.getUsuariosXNombre(this.nombreB).subscribe(
        res => {
          //console.log(res);
          this.usuariosRoles = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => {
          //console.error(err)
          this.usuariosRoles = [];
        }
      );
    } else {
      this.litarUsuario();
    }

  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarUsuario(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }


}
