import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRolesRoutingModule } from './usuariosroles-routing.module';
import { PaginaComponent } from './pagina/pagina.component';
import { ElementosModule } from '../elementos/elementos.module';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbModule, NgbTypeahead, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { UtilidadesModule } from '../utilidades/utilidades.module';

@NgModule({
  declarations: [
    PaginaComponent
  ],
  imports: [
    CommonModule,
    UsuariosRolesRoutingModule,
    ElementosModule,
    FormsModule,
    NgbPaginationModule,
    UtilidadesModule,
    NgbModule,
    NgbTypeahead,
    NgbTypeaheadModule
  ]
})
export class UsuariosRolesModule { }
