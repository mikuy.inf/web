
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Productos } from 'src/app/models/Productos';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { ProductosService } from 'src/app/services/productos/productos.service';
import { DomSanitizer } from '@angular/platform-browser';
import { VentasFService } from 'src/app/services/ventasf/ventasf.service';
import jwt_decode from "jwt-decode";
import { VentasF } from 'src/app/models/VentasF';
import { VentasDService } from 'src/app/services/ventasd/ventasd.service';
import { VentasD } from 'src/app/models/VentasD';
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import html2canvas from 'html2canvas';
import { ConstantPool } from '@angular/compiler';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  public productos: any = [];

  public categorias: any = [];
  public ventasF: any = [];
  public ventasD: any = [];
  public compras = 0;
  public idUs: number = 0;
  public vaIdVenta: number = 0;
  public vaSubTo: number = 0;
  public vaTo: number = 0;
  public vaIva: number = 0;
  public listaIVA: any = [];

  controlCedula = 1;
  controlPasaporte = 0;
  nombreEliminar = '';
  generarFactura = 0;

  envVentaF: VentasF = {
    id: 0,
    usuario: 0,
    estado: 0,
    subtotal: 0,
    total: 0,
    fpago: '',
    iva: 0,
    cedula: '',
    telefono: '',
    pasaporte: '',
  };
  envVentaD: VentasD = {
    id: 0,
    ventasf: 0,
    producto: 0,
    precio: 0,
    cantidad: 0,
    iva: 0
  };
  valCedula: string = "";
  valPasaporte: string = "";
  valTelefono: string = "";
  valNombre: string = "";
  valOrdenCompra: number = 0;
  estadoFactura: number = 0;
  control: number = 0;
  value: any = [];
  catBuscar = 0;
  valFecha: Date = null;

  strCedula: string = "* Cédula Obligatoria";
  strPasaporte: string = "* Pasaporte Obligatorio";
  strTelefono: string = "* Teléfono Obligatorio";
  errorCedula: boolean = false;
  errorPasaporte: boolean = false;
  errorTelefono: boolean = false;
  longCantidad: number | undefined = 0;
  longCedula: number | undefined = 0;
  longPasaporte: number | undefined = 0;
  longTelefono: number | undefined = 0;

  constructor(private productosService: ProductosService,
    private categoriaService: CategoriaService,
    private funcionesService: FuncionesService,
    private ventasFService: VentasFService,
    private ventasDService: VentasDService,
    private rutaActiva: ActivatedRoute,
    public modal: NgbModal,
    public modalF: NgbModal,
    private mantenimientoService: MantenimientoService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private usuariosService: UsuariosService) {


  }

  ngOnInit(): void {



    const token = localStorage.getItem('token');

    if (token) {

      this.usuariosService.controlSesion();
      this.valFecha = new Date();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];
      this.valNombre = datos[0]['nombre'] + " " + datos[0]['apellido'];
      this.litarVentasF(this.idUs.toString());
    }
    // this.buscarxCategoriaInicio(this.catBuscar);
    // this.litarCategorias();

  }

  crearPDFComprobante1() {
    const datos: any = document.getElementById('tabla1');

    const doc = new jsPDF('p', 'pt', 'a4');

    const opciones = {
      background: 'white',
      scale: 3,
    }
    html2canvas(datos, opciones)
      .then((canvas) => {
        const img = canvas.toDataURL('image/PNG');

        // Add image Canvas to PDF
        const bufferX = 15;
        const bufferY = 15;
        const imgProps = (doc as any).getImageProperties(img);
        const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
        return doc;
      }).then((docResult) => {
        this.facturar();
        docResult.save(`${new Date().toISOString()}.pdf`);

      });

  }

  crearPDFComprobante2() {
    const datos: any = document.getElementById('tabla');

    const doc = new jsPDF('p', 'pt', 'a4');

    const opciones = {
      background: 'white',
      scale: 3,
    }
    html2canvas(datos, opciones)
      .then((canvas) => {
        const img = canvas.toDataURL('image/PNG');

        // Add image Canvas to PDF
        const bufferX = 15;
        const bufferY = 15;
        const imgProps = (doc as any).getImageProperties(img);
        const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
        return doc;
      }).then((docResult) => {
        this.facturar();
        docResult.save(`${new Date().toISOString()}.pdf`);

      });

  }
  crearPDFComprobante() {
    const pdfFinal: any = {
      content: [
        {
          text: 'Comprabante de Compra'
        }
      ]
    }

  }

  litarProductos() {
    this.productosService.getProductosLista().subscribe(
      res => {

        this.productos = res;

      },
      err => console.error(err)
    );
  }

  litarCategorias() {
    this.categoriaService.getCategorias().subscribe(
      res => {

        this.categorias = res;
      },
      err => { console.error("Error---> : " + err) }
    );
  }

  litarVentasF(id: string) {
    this.ventasFService.getVentaFUsuario(id).subscribe(
      res => {
        this.ventasF = res;
        this.vaSubTo = this.ventasF[0]['subtotal'];
        this.vaTo = this.ventasF[0]['total'];
        this.vaIva = this.ventasF[0]['iva'];
        this.vaIdVenta = this.ventasF[0]['id'];
        this.valOrdenCompra = this.vaIdVenta;
        this.compras = 1;
        this.litarVentasD(id);
        this.litarVentasIVA(id);
      },
      err => {
        this.compras = 0;
      }
    );
  }

  litarVentasD(id: string) {
    this.ventasDService.getVentasDFinal(id).subscribe(
      res => {
        this.ventasD = res;

      },
      err => {

      }
    );
  }

  calcularValor(datos: any): number {
    const subtotal = datos.precio * datos.cantidad;
    /*const descuento = (subtotal * (datos.descuento / 100.0)).toFixed(2);
    const totalDescuento = subtotal - Number(descuento);
    const iva = (totalDescuento * (datos.iva / 100.0)).toFixed(2);
    const total = totalDescuento - Number(iva);*/
    const total = subtotal;
    // Redondear y cortar a dos decimales
    return Number(total.toFixed(2));
  }

  litarVentasIVA(id: string) {
    try {
      this.ventasDService.getVentasDFinalIVA(id).subscribe(
        res => {
          console.log(res)
          this.listaIVA = res;

        },
        err => {
          console.log(err)
        }
      );
    }
    catch { }
  }

  buscarxCategoria(event: any) {

    const id = event.target.value

    if (id == 0) {
      this.litarProductos();
    } else {
      this.productosService.getProductosXCategoria(id).subscribe(
        res => {
          this.productos = res;
        },
        err => {
          //console.error(err)
          this.productos = [];
        }
      );
    }

  }

  cambiarIdentificacion(event: any) {

    const id = event.target.value

    if (id == 0) {
      this.controlCedula = 1;
      this.controlPasaporte = 0;
    } else {
      this.controlCedula = 0;
      this.controlPasaporte = 1;
    }

  }

  buscarxCategoriaInicio(val: any) {

    const id = val

    if (id == 0) {
      this.litarProductos();
    } else {
      this.productosService.getProductosXCategoria(id).subscribe(
        res => {
          this.productos = res;
        },
        err => {
          //console.error(err)
          this.productos = [];
        }
      );
    }

  }

  buscarxNombre(event: any) {

    const nombre = event.target.value

    if (nombre.length == 0) {
      this.litarProductos();
    } else {

      this.productosService.getProductosXNombre(nombre).subscribe(
        res => {
          this.productos = res;
        },
        err => console.error(err)
      );
    }

  }

  controlErrores(): boolean {

    this.longCantidad = this.productos['cantidad']?.length;
    


    return true;
  }

  controlErroresFactura(): boolean {

    this.longCedula = this.valCedula?.length;
    this.longPasaporte = this.valPasaporte?.length;
    this.longTelefono = this.valTelefono?.length;

    if (this.controlCedula == 1) {
      if (this.longCedula == 0) {
        this.errorCedula = true;
        this.strCedula = "Cédula es Obligatoria";
        return false;
      } else {
        this.errorCedula = false;
      }
    }
    if (this.controlPasaporte == 1) {
      if (this.longCedula == 0) {
        this.errorCedula = true;
        this.strCedula = "Pasaporte es Obligatorio";
        return false;
      } else {
        this.errorPasaporte = false;
      }
    }

    if (this.longTelefono == 0) {
      this.errorTelefono = true;

      return false;
    } else {
      this.errorTelefono = false;
    }

    return true;
  }


  guardarCarrito(valor: string, id: string, precio: string, iva: string) {


    const token = localStorage.getItem('token');
    if (token) {
      if (this.compras == 0) {
        if (this.controlErrores()) {
          delete this.envVentaF.id;
          this.envVentaF.usuario = this.idUs;
          this.envVentaF.estado = 0;
          this.envVentaF.subtotal = 0;
          this.envVentaF.total = 0;
          this.envVentaF.fpago = '';
          this.envVentaF.iva = 0;

          this.ventasFService.guardarVentasF(this.envVentaF)
            .subscribe(
              res => {
                //this.guardarProducto(valor,id)
                // this.litarVentasF(this.idUs.toString());
              },
              err => console.error(err)
            )
        }
      }
      this.guardarProducto(valor, id, precio, iva)
      this.litarVentasF(this.idUs.toString());
    } else {
      this.router.navigate(['/login']);
    }
  }

  guardarProducto(valor: string, id: string, precio: string, iva: string) {


    if (this.compras == 1) {

      delete this.envVentaD.id;
      this.envVentaD.ventasf = this.vaIdVenta;
      this.envVentaD.producto = Number(id);
      this.envVentaD.precio = Number(precio);
      this.envVentaD.cantidad = Number(valor);
      if (iva != null) {
        this.envVentaD.iva = Number(iva);
      } else {
        this.envVentaD.iva = Number("12");
      }


      this.ventasDService.guardarVentasD(this.envVentaD)
        .subscribe(
          res => {

            this.litarVentasF(this.idUs.toString());
          },
          err => console.error(err)
        )

    }
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarVentaD(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  openF(content: any) {
    this.nombreEliminar = "";
    this.control = 1;
    this.modalF.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {
        if (result == 1) {
          this.estadoFactura = 1;
          if (this.controlErroresFactura()) {
            this.generarFactura = 1;
            this.crearPDFComprobante1();

          } else {
            this.generarFactura = 0;
            alert("Para Facturar todos los campos son obligatorios");
          }
        } else {
          this.generarFactura = 2;
          this.crearPDFComprobante2();
        }
        this.control = 0;
        console.log("Cerrar" + result)


        // this.eliminarVentaD(id);
      },
      (reason) => {
        this.control = 0;
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  eliminarVentaD(id: string) {
    this.ventasDService.eliminarVentasD(id)
      .subscribe(
        res => {
          console.log(res);
          //this.litarVentasF(this.idUs.toString());
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  facturar() {

    
    if (this.generarFactura == 2) {
      delete this.envVentaF.cedula;
      delete this.envVentaF.pasaporte;
      delete this.envVentaF.telefono;
      delete this.envVentaF.estado;
      delete this.envVentaF.fpago;
      delete this.envVentaF.id;
      delete this.envVentaF.iva;
      delete this.envVentaF.subtotal;
      delete this.envVentaF.total;
      delete this.envVentaF.iva;
      delete this.envVentaF.usuario;

    } else {
      if (this.controlCedula == 1) {
        this.envVentaF.cedula = this.valCedula;
        delete this.envVentaF.pasaporte;
      } else {
        this.envVentaF.pasaporte = this.valCedula;
        delete this.envVentaF.cedula;
      }

      this.envVentaF.telefono = this.valTelefono;
      delete this.envVentaF.estado;
      delete this.envVentaF.fpago;
      delete this.envVentaF.id;
      delete this.envVentaF.iva;
      delete this.envVentaF.subtotal;
      delete this.envVentaF.total;
      delete this.envVentaF.iva;
      delete this.envVentaF.usuario;

    }
   
    this.ventasFService.actualizarVentasF(this.idUs.toString(), this.envVentaF, this.idUs.toString())
      .subscribe(
        res => {
          //console.log(res);
          this.guardarMantenimiento("Registro", "Ventas");
          window.location.reload();

        },
        err => console.error(err)
      )
  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }

  actualizar(id: string, valor: number, maxi: number) {

    if (this.controlErrores()) {
      if (valor < 1) {
        valor = 1; // Establecer el valor mínimo si es menor a 1
      } else if (valor > maxi) {
        valor = maxi; // Establecer el valor máximo si es mayor al máximo permitido
      }
      this.ventasDService.actualizarVentasD(id, valor)
        .subscribe(
          res => {

            // this.rol = this.rolBlanco;
            window.location.reload();
          },
          err => console.error(err)
        )
    }
  }
}
