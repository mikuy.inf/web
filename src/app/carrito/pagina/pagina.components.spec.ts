import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PaginaComponent } from './pagina.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JWT_OPTIONS } from '@auth0/angular-jwt';
import { VentasFService } from 'src/app/services/ventasf/ventasf.service';
import { of } from 'rxjs';

describe('Carrito', () => {

    let ventasFService: VentasFService;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
            ],
            declarations: [
                PaginaComponent,
                
            ],
            providers: [
              { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
              JwtHelperService,
              VentasFService
            ]
            
        }).compileComponents();

      
        
    });

   
    it('Controlar crear Ventana Carrito', () => {
        const fixture = TestBed.createComponent(PaginaComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();

    });

    it('Verificar que la lista de productos este vacía sino a iniciado sesión', () => {
        const fixture = TestBed.createComponent(PaginaComponent);
        const app = fixture.componentInstance;
        app.litarProductos();
        expect(app.productos.length).toBe(0);
      });

      
});
