
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Productos } from 'src/app/models/Productos';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { ProductosService } from 'src/app/services/productos/productos.service';
import { DomSanitizer } from '@angular/platform-browser';
import { VentasFService } from 'src/app/services/ventasf/ventasf.service';
import jwt_decode from "jwt-decode";
import { VentasF } from 'src/app/models/VentasF';
import { VentasDService } from 'src/app/services/ventasd/ventasd.service';
import { VentasD } from 'src/app/models/VentasD';
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import html2canvas from 'html2canvas';
import { ConstantPool } from '@angular/compiler';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  public productos: any = [];
  public categorias: any = [];
  public ventasF: any = [];
  public ventasD: any = [];
  public listaIVA: any = [];
  public compras = 0;
  private idUs: number = 0;
  private vaIdVenta: number = 0;
  public vaSubTo: number = 0;
  public vaTo: number = 0;
  public vaIva: number = 0;
  public guardarValor: string;
  public guardarId: string;
  public guardarPrecio: string;
  public guardarIva: string;
  public guardarDescuento: string;
  public controlGuardar = 0;
  public idCategoria = 0;

  nombreEliminar = '';

  envVentaF: VentasF = {
    id: 0,
    usuario: 0,
    estado: 0,
    subtotal: 0,
    total: 0,
    fpago: '',
    iva: 0
  };
  envVentaD: VentasD = {
    id: 0,
    ventasf: 0,
    producto: 0,
    precio: 0,
    cantidad: 0,
    iva: 0,
    descuento: 0,
  };
  valCedula: string = "";
  valTelefono: string = "";
  valNombre: string = "";
  estadoFactura: number = 0;
  control: number = 0;
  value: any = [];
  catBuscar = 0;

  strCedula: string = "* Cédula Obligatoria";
  strTelefono: string = "* Teléfono Obligatorio";
  errorCedula: boolean = false;
  errorTelefono: boolean = false;
  longCantidad: number | undefined = 0;
  constructor(private productosService: ProductosService,
    private categoriaService: CategoriaService,
    private funcionesService: FuncionesService,
    private ventasFService: VentasFService,
    private ventasDService: VentasDService,
    private rutaActiva: ActivatedRoute,
    public modal: NgbModal,
    public modalF: NgbModal,
    private sanitizer: DomSanitizer,
    private router: Router,
    private usuariosService: UsuariosService,
    private location: Location) {


  }

  ngOnInit(): void {

    
    this.controlGuardar = 0;
    const token = localStorage.getItem('token');
    this.value = this.rutaActiva.snapshot.params;
    try {

      this.catBuscar = this.value['cat'];
    } catch {
      this.catBuscar = 0;
    }
    if (token) {
      this.usuariosService.controlSesion();


      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];
      this.valNombre = datos[0]['nombre'] + " " + datos[0]['apellido'];
      this.litarVentasF(this.idUs.toString());
    }
    this.buscarxCategoriaInicio(this.catBuscar);
    this.litarCategorias();

  }

  crearPDFComprobante1() {
    const datos: any = document.getElementById('tabla');

    const doc = new jsPDF('p', 'pt', 'a4');

    const opciones = {
      background: 'white',
      scale: 3,
    }
    html2canvas(datos, opciones)
      .then((canvas) => {
        const img = canvas.toDataURL('image/PNG');

        // Add image Canvas to PDF
        const bufferX = 15;
        const bufferY = 15;
        const imgProps = (doc as any).getImageProperties(img);
        const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
        return doc;
      }).then((docResult) => {
        this.facturar();
        docResult.save(`${new Date().toISOString()}.pdf`);

      });

  }
  crearPDFComprobante() {
    const pdfFinal: any = {
      content: [
        {
          text: 'Comprabante de Compra'
        }
      ]
    }

  }

  litarProductos() {
    this.productosService.getProductosLista().subscribe(
      res => {

        this.productos = res;

      },
      err => console.error(err)
    );
  }

  litarCategorias() {
    this.categoriaService.getCategorias().subscribe(
      res => {

        this.categorias = res;
      },
      err => { console.error("Error---> : " + err) }
    );
  }

  litarVentasF(id: string) {
    this.ventasFService.getVentaFUsuario(id).subscribe(
      res => {
        this.ventasF = res;
        this.vaSubTo = this.ventasF[0]['subtotal'];
        this.vaTo = this.ventasF[0]['total'];
        this.vaIva = this.ventasF[0]['iva'];
        this.vaIdVenta = this.ventasF[0]['id'];
        this.compras = 1;
        if (this.controlGuardar == 1) {
          this.guardarProducto('1', this.guardarId, this.guardarPrecio, this.guardarIva, this.guardarDescuento);
        } else {
          this.litarVentasD(id);
          this.litarVentasIVA(id);
        }

      },
      err => {
        this.compras = 0;
      }
    );
  }

  litarVentasD(id: string) {
    try {
      this.ventasDService.getVentasDFinal(id).subscribe(
        res => {
          console.log(res)
          this.ventasD = res;

        },
        err => {
          console.log(err)
        }
      );
    }
    catch { }
  }

  litarVentasIVA(id: string) {
    try {
      this.ventasDService.getVentasDFinalIVA(id).subscribe(
        res => {
          console.log(res)
          this.listaIVA = res;

        },
        err => {
          console.log(err)
        }
      );
    }
    catch { }
  }

  buscarxCategoria(event: any) {

    const id = event.target.value
    this.idCategoria=id
    if (id == 0) {
      this.litarProductos();
    } else {
              let cat = this.idCategoria.toString()
              const url = '/lista/' + cat;
              this.location.replaceState(url);
              window.location.reload();
              

    }

  }

  buscarxCategoriaInicio(val: any) {

    const id = val

    if (id == 0) {
      this.litarProductos();
    } else {
      this.idCategoria=id
      this.productosService.getProductosXCategoria(id).subscribe(
        res => {
          this.productos = res;
        },
        err => console.error(err)
      );
    }

  }

  buscarxNombre(event: any) {

    const nombre = event.target.value

    if (nombre.length == 0) {
      this.litarProductos();
    } else {

      this.productosService.getProductosXNombre(nombre).subscribe(
        res => {
          this.productos = res;
        },
        err => console.error(err)
      );
    }

  }

  controlErrores(): boolean {

    this.longCantidad = this.productos['cantidad']?.length;



    return true;
  }

  guardarCarrito(valor: string, id: string, precio: string, iva: string, descuento: string) {

    const token = localStorage.getItem('token');
    if (token) {
      if (this.compras == 0) {
        if (this.controlErrores()) {
          delete this.envVentaF.id;
          this.envVentaF.usuario = this.idUs;
          this.envVentaF.estado = 0;
          this.envVentaF.subtotal = 0;
          this.envVentaF.total = 0;
          this.envVentaF.fpago = '';
          this.envVentaF.iva = 0;
          this.guardarValor = valor;
          this.guardarId = id;
          this.guardarPrecio = precio;
          this.guardarIva = iva;
          this.guardarDescuento = descuento;
          this.controlGuardar = 1;
          this.ventasFService.guardarVentasF(this.envVentaF)
            .subscribe(
              res => {
                //this.guardarProducto(valor,id)
                //this.compras = 1;
                this.litarVentasF(this.idUs.toString());



                // this.litarVentasF(this.idUs.toString());

              },
              err => { try { 
               
                this.litarVentasF(this.idUs.toString()); } catch { } }
            )
        }
      } else {
        this.guardarId = id;
        this.guardarPrecio = precio;
        this.guardarIva = iva;
        this.guardarDescuento=descuento;
        try { this.guardarProducto('1', this.guardarId, this.guardarPrecio, this.guardarIva, this.guardarDescuento); } catch { }
      }
      //this.guardarProducto(valor, id, precio, iva)
      //this.litarVentasF(this.idUs.toString());
    } else {
      this.router.navigate(['/login']);
    }
  }

  guardarProducto(valor: string, id: string, precio: string, iva: string, descuento: string) {

    if (this.compras == 1) {
      delete this.envVentaD.id;
      this.envVentaD.ventasf = this.vaIdVenta;
      this.envVentaD.producto = Number(id);
      this.envVentaD.precio = Number(precio);
      this.envVentaD.cantidad = Number(valor);
      if (iva != null) {
        this.envVentaD.iva = Number(iva);
      } else {
        this.envVentaD.iva = Number("0");
      }
      if (descuento != null) {
        this.envVentaD.descuento = Number(descuento);
      } else {
        this.envVentaD.descuento = Number("0");
      }
      this.controlGuardar = 0;
      console.log(this.envVentaD)
      this.ventasDService.guardarVentasD(this.envVentaD)
        .subscribe(
          res => {

            this.litarVentasF(this.idUs.toString());
          },
          err => console.error(err)
        )

    }
  }

  cargarImagenCategoria(index: number) {
    const datos = this.productos[index];
    console.log("Error al cargar")
    datos.imagen = '../../../../assets/producto.png';
  }

  actualizar(id: string, valor: number, maxi: number) {

    if (this.controlErrores()) {
      var control=0
      if (valor < 0) {
        valor = 0; // Establecer el valor mínimo si es menor a 1
        control=1
        
      } else if (valor > maxi) {
        valor = maxi; // Establecer el valor máximo si es mayor al máximo permitido
        control=1
        
      }
      if (control==1){
        alert("La cantidad es incorrecta debe estar entre 0 y "+maxi.toString());
      }
        this.ventasDService.actualizarVentasD(id, valor)
        .subscribe(
          res => {

            // this.rol = this.rolBlanco;
            //window.location.reload();
              let cat = this.idCategoria.toString()
              const url = '/lista/' + cat;
              this.location.replaceState(url);
              window.location.reload();
            
            
          },
          err =>{console.error(err); } 
        )
     
      
    }
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarVentaD(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  openF(content: any) {
    this.nombreEliminar = "";
    this.control = 1;
    this.modalF.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {
        if (result == 1) {
          this.estadoFactura = 1;
        }
        this.control = 0;
        console.log("Cerrar" + result)
        this.crearPDFComprobante1();
        // this.eliminarVentaD(id);
      },
      (reason) => {
        this.control = 0;
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  eliminarVentaD(id: string) {
    this.ventasDService.eliminarVentasD(id)
      .subscribe(
        res => {
          console.log(res);
          //this.litarVentasF(this.idUs.toString());
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  facturar() {
    this.ventasFService.actualizarVentasF(this.idUs.toString(), this.envVentaF, this.idUs.toString())
      .subscribe(
        res => {
          console.log(res);
          //this.litarVentasF(this.idUs.toString());
          //this.crearPDFComprobante1();
          window.location.reload();

        },
        err => console.error(err)
      )
  }

}
