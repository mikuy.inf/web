import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UtilidadesModule } from './utilidades/utilidades.module';
import { RolesService } from './services/roles/roles.service';
import { EmmitterService } from './services/emitter/emitter.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JWT_OPTIONS } from '@auth0/angular-jwt';
import { UsuariosService } from './services/usuarios/usuarios.service';
import { SeparadorComponent } from './utilidades/componentes/separador/separador.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    UtilidadesModule,
    HttpClientModule,

  ],
  providers: [
    RolesService,
    EmmitterService,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    JwtHelperService,
    UsuariosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
