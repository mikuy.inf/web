import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JWT_OPTIONS } from '@auth0/angular-jwt';
import { UtilidadesModule } from './utilidades/utilidades.module';


describe('Página Principal', () => {
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        UtilidadesModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
        JwtHelperService,
      ]
    });
  });

  it('Controlar Crear AppComponent', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();

  });


  it(`Controlar que menuPrincipal Inicie Oculto`, () => {
    localStorage.removeItem('token');
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    fixture.detectChanges();
    expect(app.menuPrincipal).toBeFalse();
  });
  
  it('Controlar que menuPrincipal se Muestre al tener Token', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    spyOn(localStorage, 'getItem').and.returnValue('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3siaWQiOiIxIiwiZW1haWwiOiJkYXJ3aW5AZ21haWwuY29tIiwibm9tYnJlIjoiRGFyd2luIiwiYXBlbGxpZG8iOiJBZG1pbiIsImltYWdlbiI6Imh0dHBzOlwvXC9taWt1eS5jb20uZWNcL2JkZFwvaW1nXC82NDkyMWExMzdhMDBiLmpwZyIsImRpcmVjY2lvbiI6IkNkbGEgUHJpbmNpcGFsIGFtYmF0byIsInNlc3Npb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlKOS5NalUuZlozMWUyajk4am1aSV83Sk41ZVQ0a3NtQTF4NUxTRGFzR0laLW9KQ3hNcyIsImNvbnRyYXNlbmEiOiJlM2FmZWQwMDQ3YjA4MDU5ZDBmYWRhMTBmNDAwYzFlNSIsImZpbmdyZXNvIjoiMjAyMi0xMi0xNCAwMzo1MjowNyJ9XQ.c8UUuNJhZQrV_p1vq6L90WUjruPOOv-YMfQLlauhxmo');
    app.enviarEstado();
    fixture.detectChanges(); // Borrar la caché
    expect(app.menuPrincipal).toBeTrue();
  });
  

});
