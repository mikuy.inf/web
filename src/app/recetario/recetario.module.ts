import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecetarioRoutingModule } from './recetario-routing.module';
import { PaginaComponent } from './pagina/pagina.component';
import { ElementosModule } from '../elementos/elementos.module';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UtilidadesModule } from '../utilidades/utilidades.module';
@NgModule({
  declarations: [
    PaginaComponent
  ],
  imports: [
    CommonModule,
    RecetarioRoutingModule,
    ElementosModule,
    FormsModule,
    NgbPaginationModule,
    NgbModule,
    UtilidadesModule
  ]
})
export class RecetarioModule { }
