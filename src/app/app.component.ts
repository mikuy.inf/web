import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { EmmitterService } from './services/emitter/emitter.service';
import { UsuariosService } from './services/usuarios/usuarios.service';
import { FuncionesService } from './services/funciones/funciones.service';
import { Funciones } from './models/Funciones';
import { map } from 'rxjs/operators';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  tabla: any = [];
  menuPrincipal = false;
  private idUs: number = 0;
  constructor(public _eventEmitter: EmmitterService,
    private usuariosServicios: UsuariosService
    , private funcionesServicios: FuncionesService) {

    this.enviarEstado();
  }

  enviarEstado() {

    const token = localStorage.getItem('token');
    //if(!localStorage.getItem('token')){
    console.log(token);
    if (token) {
      const datos: any = jwt_decode(token);
      this._eventEmitter.setHeader(this.usuariosServicios.isAuth());
      this._eventEmitter.setRol(datos[0]['rol'])
      this.idUs = datos[0]['id'];
      this.menuPrincipal = true
      this.funcionesServicios.getRolFuncion(this.idUs.toString());
      
    } else {
      this.menuPrincipal = false
     
    }

    //this.funcionesServicios.getRolFuncion(this._eventEmitter.getRol());





    return false;
  }
}
