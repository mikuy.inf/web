import { Component, OnInit,ElementRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Productos } from 'src/app/models/Productos';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { ProductosService } from 'src/app/services/productos/productos.service';
import { IvaService } from 'src/app/services/iva/iva.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  btnModificar = 0;
  btnCrear = 1;
  page = 1;
  pageSize = 12;

  productos: any = [];
  categorias: any = [];
  iva: any = [];
  nombreEliminar = '';
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;

  edit: boolean = false;

  producto: Productos = {
    id: 0,
    nombre: '',
    nombrecategoria: '',
    categoria: 0,
    descripcion: '',
    imagen: '',
    precio: '',
    estado: 0,
    cantidad: 0,
    descuento: 0,
    IVA: 0,
  }

  productoBlanco: Productos = {
    id: 0,
    nombre: '',
    nombrecategoria: '',
    categoria: 0,
    descripcion: '',
    imagen: '',
    precio: '',
    estado: 0,
    cantidad: 0,
    descuento: 0,
    IVA: 0,
  }

  public archivos: any = []

  nombreB: string = '';

  longNombre: number | undefined = 0;
  longDescripcion: number | undefined = 0;
  longPrecio: number | undefined = 0;
  longCantidad: number | undefined = 0;
  longIVA: number | undefined = 0;
  longDescuento: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strDescripcion: string = "* Descripción Obligatorio";
  strPrecio: string = "* Precio Obligatorio";
  strCantidad: string = "* Cantidad Obligatorio";
  strIVA: string = "* IVA Obligatorio";
  strDescuento: string = "* Descuento Obligatorio";

  errorNombre: boolean = false;
  errorDescripcion: boolean = false;
  errorPrecio: boolean = false;
  errorCantidad: boolean = false;
  errorIVA: boolean = false;
  errorDescuento: boolean = false;

  strCategoria: string = "* La Categoría es Obligatoria";
  errorCategoria: boolean = false;

  private idUs: number = 0;

  constructor(private productosService: ProductosService,
    private categoriaService: CategoriaService,
    private funcionesService: FuncionesService,
    private ivaService: IvaService,
    public modal: NgbModal,
    private mantenimientoService: MantenimientoService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private usuariosService: UsuariosService,
    private el: ElementRef) { }

 

  ngOnInit(): void {
    const token = localStorage.getItem('token');

    if (token) { 
      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];

      this.litarProductos();
      this.litarCategorias();
      this.litarIva();

      this.cargarFuncion();

    } else {
      this.router.navigate(['/login']);
    }

  }

  controlErrores(): boolean {

    this.longNombre = this.producto['nombre']?.length;
    this.longDescripcion = this.producto['descripcion']?.length;
    this.longPrecio = this.producto['precio']?.length;
    const vaCan: string | any = this.producto['cantidad'] as number;
    const vaDes: string | any = this.producto['descuento'] as number;
    const vaIVA: string | any = this.producto['IVA'] as number;
    this.longCantidad = vaCan.length;
    this.longIVA = vaIVA.length;
    this.longDescuento = vaDes.length;

    if (this.producto.categoria == null || this.producto.categoria == 0) {
      this.errorCategoria = true;

      return false;
    } else {
      this.errorCategoria = false;

    }

    if (this.longNombre == 0) {
      this.errorNombre = true;
      this.strNombre = "El Nombre es Obligatorio";
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longDescripcion == 0) {
      this.errorDescripcion = true;
      this.strDescripcion = "La Descripcion es Obligatoria";
      return false;
    } else {
      this.errorDescripcion = false;

    }


    if (this.longPrecio == 0) {
      this.errorPrecio = true;
      this.strPrecio = "El Precio es Obligatorio";
      return false;
    } else {
      this.errorPrecio = false;

    }

    if (this.longCantidad == 0) {
      this.errorCantidad = true;
      this.strCantidad = "La Cantidad es Obligatoria";
      return false;
    } else {
      this.errorCantidad = false;

    }

    if (this.longIVA == 0) {
      this.errorIVA = true;
      this.strIVA = "El IVA es Obligatorio";
      return false;
    } else {
      this.errorIVA = false;

    }

    if (this.longDescuento == 0) {
      this.errorDescuento = true;
      this.strDescuento = "El Descuento es Obligatorio";
      return false;
    } else {
      this.errorDescuento = false;

    }

    if (vaDes < 0) {
      this.errorDescuento = true;
      this.strDescuento = "Descuento Incorrecto";
      return false;
    } else {
      this.errorDescuento = false;

    }

    return true;
  }

  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'producto').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarProductos() {
    this.productosService.getProductos().subscribe(
      res => {
        this.productos = res;
      },
      err => console.error(err)
    );
  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Productos';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.productos.forEach((element, index, array) => {
      info.push([element.id, element.nombrecategoria, element.nombre, element.descripcion, element.precio, element.cantidad, element.IVA, element.descuento,element.estado === 1 ? 'Activo' : 'No Activo'])

    })

    autoTable(doc, {
      head: [["Id", "Categoría", "Nombre", "Descripción", "Precio", "Cantidad", "IVA", "Descuento", "Estado"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_productos.pdf");
  }


  litarCategorias() {
    this.categoriaService.getCategorias().subscribe(
      res => {
        this.categorias = res;
      },
      err => console.error(err)
    );
  }

  litarIva() {
    this.ivaService.getIvas().subscribe(
      res => {
        this.iva = res;
      },
      err => console.error(err)
    );
  }

  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.producto.id;
 
      delete this.producto.nombrecategoria;
      delete this.producto.cantidad;
      this.productosService.guardarProducto(this.producto,this.archivos[0])
        .subscribe(
          res => {
            this.guardarMantenimiento("Crear", "Producto");
            alert("Producto Registrado Correctamente")
            window.location.reload();
          },
          err => console.error(err)
        )
    }
  }

  seleccionarProducto(id: string) {
    this.usuariosService.controlSesion();
    this.productosService.getProducto(id).subscribe(
      res => {
        this.producto = res[0];
        this.btnModificar = 1;
        this.btnCrear = 0;
      },
      err => console.error(err)
    );
  }

  actualizar() {

    this.usuariosService.controlSesion();
    delete this.producto.nombrecategoria;
    delete this.producto.cantidad;
    this.productosService.actualizarProducto(this.producto,this.archivos[0])
      .subscribe(
        res => {
          this.guardarMantenimiento("Actualizar", "Producto");
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  eliminarProdcuto(id: string) {
    this.usuariosService.controlSesion();
    this.productosService.eliminarProducto(id)
      .subscribe(
        res => {

          this.guardarMantenimiento("Eliminar", "Producto");
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarProdcuto(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  capturarImagen(event: any): any {
    this.limpiarImage();
    this.usuariosService.controlSesion();
    const imagenCapturada = event.target.files[0];
    this.extraerBase64(imagenCapturada).then((imagen: any) =>
      this.producto.imagen = imagen.base
    )
    this.archivos.push(imagenCapturada)
  }


  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    // try {
    const unsafeImg = window.URL.createObjectURL($event);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    const reader = new FileReader();
    reader.readAsDataURL($event);
    reader.onload = () => {
      resolve({
        base: reader.result
      });

    };
    reader.onerror = error => {
      resolve({
        base: null
      });
    };

    /*} catch (e) {
     
      return null;
    }*/
  })


  /**
   * Limpiar imagen
   */

  limpiarImage(): any {
    this.producto.imagen = '';
    this.archivos = [];
  }

  cancelar() {
    this.usuariosService.controlSesion();
    this.producto = { ...this.productoBlanco};
    this.limpiarImage();
    const inputImagen = this.el.nativeElement.querySelector('#imagen');
    inputImagen.value = null;
    this.btnModificar = 0;
    this.btnCrear = 1;
  }

  validarDecimal(event: any) {
    const pattern = /^[0-9]+(\.[0-9]{0,2})?$/; // Expresión regular para números decimales con hasta dos decimales
    const inputVal = event.target.value;
  
    // Verificar si el valor coincide con el patrón y es mayor o igual a cero
    if (!pattern.test(inputVal) || parseFloat(inputVal) < 0) {
      // Si el valor no coincide o es negativo, eliminar los caracteres no válidos
      const validDecimal = inputVal.match(pattern);
      event.target.value = validDecimal ? validDecimal[0] : '';
    }
  }

  validarEntero(event: any) {
    const pattern = /^[0-9]*$/; // Expresión regular para números enteros
    const inputVal = event.target.value;

    // Verificar si el valor coincide con el patrón
    if (!pattern.test(inputVal)) {
      // Si el valor no coincide, eliminar los caracteres no válidos
      const validEntero = inputVal.match(pattern);
      event.target.value = validEntero ? validEntero[0] : '';
    }
  }

  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.productosService.getProductosXNombre0(this.nombreB).subscribe(
        res => {
         
          this.productos = res;
          this.cancelar()
        },
        err => {
          this.productos = [];
        }
      );
    } else {
      this.litarProductos();
    }

  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }
}
