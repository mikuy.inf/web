import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IMenu, MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-titulo',
  templateUrl: './titulo.component.html',
  styleUrls: ['./titulo.component.css']
})
export class TituloComponent implements OnInit {
  path:IMenu
  constructor(private menuService:MenuService,
    private activatedRoute:ActivatedRoute){ 
      const currentPath = '/'+this.activatedRoute.snapshot.pathFromRoot[1].routeConfig?.path
      this.path = this.menuService.getMenuByUrl(currentPath)
      
    } 

  ngOnInit(): void {
  }

}
