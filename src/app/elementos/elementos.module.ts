import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { ContenedorComponent } from './componentes/contenedor/contenedor.component';
import { TituloComponent } from './componentes/titulo/titulo.component'

@NgModule({ 
  declarations: [
    ContenedorComponent,
    TituloComponent
  ],
  imports: [
    CommonModule,
    
  ],
  exports:[
  //  PerfectScrollbarModule,
    
    ContenedorComponent
  ]
 
})
export class ElementosModule { }
