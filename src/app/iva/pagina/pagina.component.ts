
import { Component, OnInit, HostBinding } from '@angular/core';
import { Iva } from 'src/app/models/Iva';
import { IvaService } from 'src/app/services/iva/iva.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Funciones } from 'src/app/models/Funciones';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import jwt_decode from "jwt-decode";
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  page = 1;
  btnModificar = 0;
  btnCrear = 1;
  pageSize = 12;
  ivas: any = [];
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;

  iva: Iva = {
    id: 0,
    nombre: '',
    valor: '',
  };



  ivaBlanco: Iva = {
    id: 0,
    nombre: '',
    valor: '',
  };

  closeResult = '';
  nombreEliminar = '';

  nombreB: string = '';

  longNombre: number | undefined = 0;
  longValor: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strValor: string = "* Valor Obligatoria";
  errorNombre: boolean = false;
  errorValor: boolean = false;
  private idUs: number = 0;


  constructor(private ivaService: IvaService,
    public modal: NgbModal,
    private funcionesService: FuncionesService,
    private router: Router,
    private mantenimientoService: MantenimientoService,
    private usuariosService: UsuariosService
  ) { }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {

      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];

      this.litarIva();
      this.cargarFuncion();


    } else {
      this.router.navigate(['/login']);
    }


  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de IVA';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.ivas.forEach((element, index, array) => {
      info.push([element.id, element.nombre, element.valor])

    })

    autoTable(doc, {
      head: [["Id", "Nomrbe", "VALOR"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_iva.pdf");
  }

  validarDecimal(event: any) {
    const pattern = /^[0-9]+(\.[0-9]{0,2})?$/; // Expresión regular para números decimales con hasta dos decimales
    const inputVal = event.target.value;
  
    // Verificar si el valor coincide con el patrón y es mayor o igual a cero
    if (!pattern.test(inputVal) || parseFloat(inputVal) < 0) {
      // Si el valor no coincide o es negativo, eliminar los caracteres no válidos
      const validDecimal = inputVal.match(pattern);
      event.target.value = validDecimal ? validDecimal[0] : '';
    }
  }
  


  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'iva').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }


  controlErrores(): boolean {

    this.longNombre = this.iva['nombre']?.length;
    this.longValor = this.iva['valor']?.length;

    if (this.longNombre == 0) {
      this.errorNombre = true;
      this.strNombre = "El Nombre es Obligatorio";
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longValor == 0) {
      this.errorValor = true;
      this.strValor = "El Valor es Obligatoria";
      return false;
    } else {
      this.errorValor = false;

    }

    return true;
  }



  litarIva() {
    this.ivaService.getIvas().subscribe(
      res => {
        this.ivas = res;
        //this.roles=this.roles.slice(0,1);
      },
      err => console.error(err)
    );
  }

  eliminarIva(id: string) {
    this.usuariosService.controlSesion();
    this.ivaService.eliminarIva(id)
      .subscribe(
        res => {

          this.guardarMantenimiento("Eliminar", "Iva");
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.iva.id;
      //this.iva.usuario = this.idUs;
      this.ivaService.guardarIva(this.iva)
        .subscribe(
          res => {
            this.guardarMantenimiento("Crear", "Iva");
            window.location.reload();
            //this.litarVideos();

          },
          err => console.error(err)
        )
    }
  }

  seleccionarIva(id: string) {
    this.usuariosService.controlSesion();
    this.ivaService.getIva(id).subscribe(
      res => {
        this.btnModificar = 1;
        this.btnCrear = 0;
        this.iva = res[0];
      },
      err => console.error(err)
    );
  }

  actualizar() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {

      this.ivaService.actualizarIva(this.iva.id, this.iva)
        .subscribe(
          res => {
            //console.log(res);
            this.guardarMantenimiento("Actualizar", "Recetas");
            window.location.reload();
            //this.litarVideos();
            //this.video = this.videoBlanco;
          },
          err => console.error(err)
        )
    }
  }

  cancelar() {
    this.usuariosService.controlSesion();
    this.iva = this.ivaBlanco;
    this.btnModificar = 0;
    this.btnCrear = 1;
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarIva(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }



  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.ivaService.getIvaXNombre(this.nombreB).subscribe(
        res => {
          this.ivas = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => {
          //console.error(err)
          this.ivas = [];
        }
      );
    } else {
      this.litarIva();
    }

  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }

}
