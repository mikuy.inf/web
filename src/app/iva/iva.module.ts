import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IvaRoutingModule } from './iva-routing.module';
import { PaginaComponent } from './pagina/pagina.component';
import { ElementosModule } from '../elementos/elementos.module';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UtilidadesModule } from '../utilidades/utilidades.module';


@NgModule({
  declarations: [
    PaginaComponent

  ],
  imports: [
    CommonModule,
    IvaRoutingModule,
    ElementosModule,
    FormsModule,
    NgbPaginationModule,
    UtilidadesModule,
    NgbModule
  ]
})
export class IvaModule { }
