import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './utilidades/pagina/inicio/inicio.component';
import { IniciarsesionComponent } from './utilidades/componentes/iniciarsesion/iniciarsesion.component';
import { AuthGuard } from './guards/auth.guard';
import { AccesoProductosGuard } from './guards/accesoProductos.guard';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { PathLocationStrategy } from '@angular/common';

const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'login', component: IniciarsesionComponent },
  { path: 'usuarios', loadChildren: () => import('./usuarios/usuarios.module').then((m) => m.UsuariosModule) },
  { path: 'usuariosroles', loadChildren: () => import('./usuariosroles/usuariosroles.module').then((m) => m.UsuariosRolesModule) },
  { path: 'categorias', loadChildren: () => import('./categoria/categoria.module').then((m) => m.CategoriasModule) },
  { path: 'funciones', loadChildren: () => import('./funciones/funciones.module').then((m) => m.FuncionesModule) },
  { path: 'mantenimiento', loadChildren: () => import('./mantenimiento/mantenimiento.module').then((m) => m.MantenimientoModule) },
  { path: 'stock', loadChildren: () => import('./stock/stock.module').then((m) => m.StockModule) },
  { path: 'productos', loadChildren: () => import('./productos/productos.module').then((m) => m.ProductosModule), canActivate: [AuthGuard] },
  { path: 'lista/:cat', loadChildren: () => import('./lista/lista.module').then((m) => m.ListaModule) },
  { path: 'compras', loadChildren: () => import('./ventas/ventas.module').then((m) => m.VentasModule) },
  { path: 'ventas', loadChildren: () => import('./listaventas/listaventas.module').then((m) => m.ListaVentasModule) },
  { path: 'roles', loadChildren: () => import('./roles/roles.module').then((m) => m.RolesModule), pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'videos', loadChildren: () => import('./videos/videos.module').then((m) => m.VideosModule) },
  { path: 'iva', loadChildren: () => import('./iva/iva.module').then((m) => m.IvaModule) },
  { path: 'publicidad', loadChildren: () => import('./publicidad/publicidad.module').then((m) => m.PublicidadModule) },
  { path: 'perfil', loadChildren: () => import('./perfil/perfil.module').then((m) => m.PerfilModule) },
  { path: 'quienessomos', loadChildren: () => import('./quienessomos/quienessomos.module').then((m) => m.QuienesSomosModule) },
  { path: 'listacategorias', loadChildren: () => import('./listacategoria/listacategoria.module').then((m) => m.ListaCategoriasModule) },
  { path: 'noticias', loadChildren: () => import('./noticias/noticas.module').then((m) => m.NoticiasModule) },
  { path: 'experiencias', loadChildren: () => import('./experiencias/experiencias.module').then((m) => m.ExperienciasModule) },
  { path: 'recetario', loadChildren: () => import('./recetario/recetario.module').then((m) => m.RecetarioModule) },
  { path: 'contactenos', loadChildren: () => import('./contactenos/contactenos.module').then((m) => m.ContactenosModule) },
  { path: 'recetas', loadChildren: () => import('./listaVideos/listaVideos.module').then((m) => m.ListaVideosModule) },
  { path: 'carrito', loadChildren: () => import('./carrito/carrito.module').then((m) => m.CarritoModule) },
  { path: 'quienessomos', loadChildren: () => import('./quienessomos/quienessomos.module').then((m) => m.QuienesSomosModule) },
  { path: 'listacategorias', loadChildren: () => import('./listacategoria/listacategoria.module').then((m) => m.ListaCategoriasModule) },
  { path: 'noticias', loadChildren: () => import('./noticias/noticas.module').then((m) => m.NoticiasModule) },
  { path: 'roles/crear', loadChildren: () => import('./crear/roles/roles.module').then((m) => m.RolesModule) },
  { path: 'roles/editar/:id', loadChildren: () => import('./crear/roles/roles.module').then((m) => m.RolesModule) },
  { path: 'usuarios/crear', loadChildren: () => import('./crear/usuarios/usuarios.module').then((m) => m.UsuariosModule) },
  { path: 'usuarios/editar/:id', loadChildren: () => import('./crear/usuarios/usuarios.module').then((m) => m.UsuariosModule) },
  { path: 'categorias/crear', loadChildren: () => import('./crear/categoria/categoria.module').then((m) => m.CategoriaModule) },
  { path: 'categorias/editar/:id', loadChildren: () => import('./crear/categoria/categoria.module').then((m) => m.CategoriaModule) },
  { path: 'validar/:value', loadChildren: () => import('./validar/validar.module').then((m) => m.ValidarModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }]
  //providers: [{ provide: LocationStrategy, useClass: PathLocationStrategy }]
})
export class AppRoutingModule { }
