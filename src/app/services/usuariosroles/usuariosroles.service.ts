import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuariosRoles } from '../../models/UsuariosRoles'
import { Observable } from 'rxjs';
import { EmmitterService } from '../emitter/emitter.service';
import { Router, ActivatedRoute } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import jwt_decode from "jwt-decode";

@Injectable({
    providedIn: 'root'
})
export class UsuariosRolesService {

    URI = 'http://localhost:3000'
    URI1 = 'https://mikuy.com.ec/bdd'



    constructor(private http: HttpClient,
        private router: Router,
        private _evetEmiter: EmmitterService,
        private jwtHelper: JwtHelperService,
    ) { }

    private leerToken(): void { }
    private guardarToken(): void { }
    private handleError(): void { }

    getUsuarios() {
        return this.http.get(`${this.URI1}/usuariosroles/lista.php`);
        //return this.http.get(`${this.URI}/usuariosroles`);
    }

    getUsuariosXNombre(nombre: string) {
        var datos = {
            'nombre': nombre
        };
        return this.http.post(`${this.URI1}/usuariosroles/buscarxnombre.php`, datos);
        //return this.http.get(`${this.URI}/usuariosroles/buscar/${nombre}`);
    }



    getUsuario(id: string) {
        var datos = {
            'id': id
        };

        return this.http.post(`${this.URI1}/usuariosroles/seleccionar.php`, datos);
        //return this.http.get(`${this.URI}/usuariosroles/${id}`);
    }

    eliminarUsuario(id: string) {
        var datos = {
            'id': id
        };
        return this.http.post(`${this.URI1}/usuariosroles/eliminar.php`, datos);
        // return this.http.delete(`${this.URI}/usuariosroles/${id}`);
    }

    guardarUsuario(usuario: UsuariosRoles) {
        return this.http.post(`${this.URI1}/usuariosroles/crear.php`, usuario);
        //return this.http.post(`${this.URI}/usuariosroles`, usuario);
    }





    actualizarUsuario(id: string | number | undefined, datoActualizado: UsuariosRoles): Observable<UsuariosRoles> {
        return this.http.post(`${this.URI1}/usuariosroles/actualizar.php`, datoActualizado);
        //return this.http.put(`${this.URI}/usuariosroles/${id}`, datoActualizado);
    }


}
