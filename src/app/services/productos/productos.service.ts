import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Productos } from 'src/app/models/Productos';
import { Observable } from 'rxjs';
import { EmmitterService } from '../emitter/emitter.service';


@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient,
    private _evetEmiter: EmmitterService,) { }


  getProductos() {
    return this.http.get(`${this.URI}/productos/lista.php`);
    //return this.http.get(`${this.URI}/productos`);
  }

  getProductosLista() {
    return this.http.get(`${this.URI}/productos/listauno.php`);
    //return this.http.get(`${this.URI}/productos/lista/1`);
  }

  getProducto(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/productos/seleccionar.php`, datos);
    //return this.http.get(`${this.URI}/productos/${id}`);
  }

  getProductosXCategoria(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/productos/buscarxcategoria1.php`, datos);
    //return this.http.get(`${this.URI}/productos/categoria/${id}`);
  }

  getProductosXNombre(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/productos/buscarxnombre1.php`, datos);
    //return this.http.get(`${this.URI}/productos/nombre/${nombre}`);
  }

  getProductosXNombre0(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/productos/buscarxnombre.php`, datos);
    //return this.http.get(`${this.URI}/productos/nombre/${nombre}`);
  }

  eliminarProducto(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/productos/eliminar.php`, datos);
    //return this.http.delete(`${this.URI}/productos/${id}`);
  }

  guardarProducto(producto: Productos,archivos: any) {
    const formData = new FormData();
    formData.append('categoria', producto.categoria.toString());
    formData.append('nombre', producto.nombre);
    formData.append('descripcion', producto.descripcion);
    formData.append('precio', producto.precio);
    formData.append('estado', producto.estado.toString());
    formData.append('IVA', producto.IVA.toString());
    formData.append('descuento', producto.descuento.toString());
    formData.append('imagen', archivos);
    return this.http.post(`${this.URI}/productos/crear.php`, formData);
    //return this.http.post(`${this.URI}/productos`, producto);
  }

  actualizarProducto(datoActualizado: Productos,archivos: any): Observable<Productos> {
    const formData = new FormData();
    formData.append('id', datoActualizado.id.toString());
    formData.append('categoria', datoActualizado.categoria.toString());
    formData.append('nombre', datoActualizado.nombre);
    formData.append('descripcion', datoActualizado.descripcion);
    formData.append('precio', datoActualizado.precio);
    formData.append('estado', datoActualizado.estado.toString());
    formData.append('descuento', datoActualizado.descuento.toString());
    formData.append('IVA', datoActualizado.IVA.toString());
   
    
    formData.append('imagen', datoActualizado.imagen);
    formData.append('image', archivos);
    return this.http.post(`${this.URI}/productos/actualizar.php`, formData);
    //return this.http.put(`${this.URI}/productos/${id}`, datoActualizado);
  }
}
