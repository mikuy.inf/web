import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VentasD } from 'src/app/models/VentasD';
import { Observable } from 'rxjs';
import { EmmitterService } from '../emitter/emitter.service';


@Injectable({
  providedIn: 'root'
})
export class VentasDService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient,
    private _evetEmiter: EmmitterService,) { }


  getVentasD() {
    return this.http.get(`${this.URI}/ventasd/lista.php`);
  }



  getVentasDFinal(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasd/listaxusuario.php`, datos);
  }
 
  getVentasDFinalIVA(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasd/listaxusuarioIVA.php`, datos);
  }

  getVentasDSeleccionado(id: string) {
    
      
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasd/listaxventasf.php`, datos);
    //return this.http.get(`${this.URI}/ventasd/ventasf/${id}`);
  }

  getVentasDSeleccionadoIVA(id: string) {

    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasd/listaxventasfIVA.php`, datos);
    //return this.http.get(`${this.URI}/ventasd/ventasf/${id}`);
  }


  eliminarVentasD(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasd/eliminar.php`, datos);
    //return this.http.delete(`${this.URI}/ventasd/${id}`);
  }

  guardarVentasD(ventasD: VentasD) {
    return this.http.post(`${this.URI}/ventasd/crear.php`, ventasD);
    //return this.http.post(`${this.URI}/ventasd`, ventasD);
  }

  actualizarVentasD(id: string, cantidad: number) {
    var datos = {
      'id': id,
      'cantidad': cantidad
    };
    return this.http.post(`${this.URI}/ventasd/actualizar.php`, datos);
    //return this.http.put(`${this.URI}/ventasd/${id}/${cantidad}`, null);
  }


}
