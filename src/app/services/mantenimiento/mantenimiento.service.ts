import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Roles } from '../../models/Roles'
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MantenimientoService {

    URI = 'https://mikuy.com.ec/bdd'

    constructor(private http: HttpClient) { }

    getMantenimiento() {
        return this.http.get(`${this.URI}/mantenimiento/lista.php`);
        //return this.http.get(`${this.URI}/mantenimiento`);
    }


    guardarMantenimiento(usuario: String, proceso: String, tabla: String) {

        var datos = {
            'usuario': usuario,
            'proceso': proceso,
            'tabla': tabla
        };
        return this.http.post(`${this.URI}/mantenimiento/crear.php`, datos);
        //return this.http.post(`${this.URI}/mantenimiento/${usuario}/${proceso}/${tabla}`, null);
    }

    getMantenimientoXNombre(nombre: string) {
        var datos = {
            'nombre': nombre
        };
        return this.http.post(`${this.URI}/mantenimiento/buscarxnombre.php`, datos);
        ///return this.http.get(`${this.URI}/mantenimiento/buscar/${nombre}`);
    }

}
