import { Injectable } from '@angular/core';

export interface IMenu{
  title:string,
  url:string,
  icon : string
}

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private listMenu:IMenu[] = [
    {title:'Usuarios',url:'/usuarios',icon:'/assets/icons/usuarios.svg'},
    {title:'Roles',url:'/roles',icon:'/assets/icons/roles.svg'},
    {title:'Funciones',url:'/funciones',icon:'/assets/icons/funciones.svg'},
    {title:'Ventasf',url:'/ventasf',icon:'/assets/icons/ventas.svg'},
    {title:'Categoria',url:'/categoria',icon:'/assets/icons/categoria.svg'},
    {title:'Productos',url:'/producto',icon:'/assets/icons/producto.svg'}
  ]
 
  constructor() { }
  getMenu():IMenu[]{
    return[...this.listMenu]
  }

  getMenuByUrl(url:string):IMenu{
    return this.listMenu.find(
      (menu) => menu.url.toLowerCase() === url.toLowerCase()
    ) as IMenu
  }

}