import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Stock } from 'src/app/models/Stock';
import { Observable } from 'rxjs';
import { EmmitterService } from '../emitter/emitter.service';


@Injectable({
  providedIn: 'root'
})
export class StockService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient,
    private _evetEmiter: EmmitterService,) { }


  getStocks() {
    return this.http.get(`${this.URI}/stock/lista.php`);
  }

  getStockLista() {
    return this.http.get(`${this.URI}/stock/listauno.php`);
  }

  getStock(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/stock/seleccionar.php`, datos);
  }

  getStockXProducto(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/stock/buscarxnombre.php`, datos);
  }

  getStockXFecha(finicio: string, ffin: string,nombre: string,proceso: string) {
    var datos = {
      'finicio': finicio,
      'ffin': ffin,
      'nombre': nombre,
      'proceso': proceso,
    };
    return this.http.post(`${this.URI}/stock/buscarxfecha.php`, datos);
  }

  getStockSinFecha(nombre: string,proceso: string) {
    var datos = {
      'nombre': nombre,
      'proceso': proceso,
    };
    return this.http.post(`${this.URI}/stock/buscarsinfecha.php`, datos);
  }

  getStockXProceso(proceso: string) {
    var datos = {
      'proceso': proceso
    };
    return this.http.post(`${this.URI}/stock/buscarxproceso.php`, datos);
  }

  getStockXUsuario(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/stock/buscarxusuario.php`, datos);
  }

  eliminarStock(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/stock/eliminar.php`, datos);
  }

  guardarStock(stock: Stock) {
    const formData = new FormData();
    formData.append('producto', stock.producto.toString());
    formData.append('usuario', stock.usuario.toString());
    formData.append('proceso', stock.proceso.toString());
    formData.append('valor', stock.valor.toString());
    
    return this.http.post(`${this.URI}/stock/crear.php`, formData);
  }

  actualizarStock(datoActualizado: Stock): Observable<Stock> {
    const formData = new FormData();
    formData.append('id', datoActualizado.id.toString());
    formData.append('producto', datoActualizado.producto.toString());
    formData.append('usuario', datoActualizado.usuario.toString());
    formData.append('proceso', datoActualizado.proceso.toString());
    formData.append('valor', datoActualizado.valor.toString());
    return this.http.post(`${this.URI}/stock/actualizar.php`, formData);
  }
}
