import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VentasF } from 'src/app/models/VentasF';
import { Observable } from 'rxjs';
import { EmmitterService } from '../emitter/emitter.service';
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class VentasFService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient,
    private _evetEmiter: EmmitterService,) { }


  getVentasF() {
    return this.http.get(`${this.URI}/ventasf/lista.php`);

  }

  getVentasFR() {
    return this.http.get(`${this.URI}/ventasf/listaR.php`);

  }

  getVentasFLista(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasf/buscarUsu.php`, datos);
  }

  getVentasFListaR(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasf/buscarUsuR.php`, datos);
  }

  getVentasFXFecha(finicio: string, ffin: string, cedula: string, estado: string, orden: string) {
    var datos = {
      'finicio': finicio,
      'ffin': ffin,
      'cedula': cedula,
      'estado': estado,
      'orden': orden
    };
    return this.http.post(`${this.URI}/ventasf/buscarxfecha.php`, datos);
  }

  getVentasFXFechaR(finicio: string, ffin: string, cedula: string, estado: string, orden: string) {
    var datos = {
      'finicio': finicio,
      'ffin': ffin,
      'cedula': cedula,
      'estado': estado,
      'orden': orden
    };
    return this.http.post(`${this.URI}/ventasf/buscarxfechaR.php`, datos);
  }

  getVentasFsinFecha(cedula: string, estado: string, orden: string) {
    var datos = {
      'cedula': cedula,
      'estado': estado,
      'orden': orden
    };
    return this.http.post(`${this.URI}/ventasf/buscarsinfecha.php`, datos);
  }

  getVentasFsinFechaR(cedula: string, estado: string, orden: string) {
    var datos = {
      'cedula': cedula,
      'estado': estado,
      'orden': orden
    };
    return this.http.post(`${this.URI}/ventasf/buscarsinfechaR.php`, datos);
  }

  getVentasFXFechaUsuario(finicio: string, ffin: string, usuario: string, cedula: string, estado: string, orden: string) {
    var datos = {
      'finicio': finicio,
      'ffin': ffin,
      'usuario': usuario,
      'cedula': cedula,
      'estado': estado,
      'orden': orden
    };
    return this.http.post(`${this.URI}/ventasf/buscarxfechausuario.php`, datos);
  }

  getVentasFXFechaUsuarioR(finicio: string, ffin: string, usuario: string, cedula: string, estado: string, orden: string) {
    var datos = {
      'finicio': finicio,
      'ffin': ffin,
      'usuario': usuario,
      'cedula': cedula,
      'estado': estado,
      'orden': orden
    };
    return this.http.post(`${this.URI}/ventasf/buscarxfechausuarioR.php`, datos);
  }

  getVentasFsinFechaUsuario(usuario: string, cedula: string, estado: string, orden: string) {
    var datos = {
      'usuario': usuario,
      'cedula': cedula,
      'estado': estado,
      'orden': orden
    };
    return this.http.post(`${this.URI}/ventasf/buscarsinfechausuario.php`, datos);
  }

  getVentasFsinFechaUsuarioR(usuario: string, cedula: string, estado: string, orden: string) {
    var datos = {
      'usuario': usuario,
      'cedula': cedula,
      'estado': estado,
      'orden': orden
    };
    return this.http.post(`${this.URI}/ventasf/buscarsinfechausuarioR.php`, datos);
  }

  getVentaF(id: string) {

    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasf/seleccionar.php`, datos);
    //return this.http.get(`${this.URI}/ventasf/${id}`);
  }

  getVentaFUsuario(id: string) {

    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/ventasf/buscarxusuarioultima.php`, datos);
    //return this.http.get(`${this.URI}/ventasf/usuario/${id}`);
  }

  getVentaIdentificacion(cedula: string) {

    var datos = {
      'cedula': cedula
    };
    return this.http.post(`${this.URI}/ventasf/buscarxidentificacion.php`, datos);
    //return this.http.get(`${this.URI}/ventasf/identificacion/${cedula}`);
  }

  getVentaEstado(estado: string) {

    var datos = {
      'estado': estado
    };
    return this.http.post(`${this.URI}/ventasf/buscarxestado.php`, datos);
  }

  getVentaEstadoUsuario(estado: string,id: string) {

    var datos = {
      'estado': estado,
      'id': id
    };
    return this.http.post(`${this.URI}/ventasf/buscarxestadousuario.php`, datos);
  }

  getVentaFOrden(id: string) {

    var datos = {
      'orden': id
    };
    return this.http.post(`${this.URI}/ventasf/buscarxorden.php`, datos);
    //return this.http.get(`${this.URI}/ventasf/buscar/orden/lista/${id}`);
  }

  getVentaIdentificacionUsuario(cedula: string, usuario: string) {

    var datos = {
      'cedula': cedula,
      'usuario': usuario
    };
    return this.http.post(`${this.URI}/ventasf/buscarxidentificacionusuario.php`, datos);
    //return this.http.get(`${this.URI}/ventasf/identificacion/usuario/${cedula}/${usuario}`);
  }

  getVentaFOrdenUsuario(id: string, usuario: string) {

    var datos = {
      'orden': id,
      'usuario': usuario
    };
    return this.http.post(`${this.URI}/ventasf/buscarxordenusuario.php`, datos);
    //return this.http.get(`${this.URI}/ventasf/buscar/orden/lista/usuario/${id}/${usuario}`);
  }

  guardarVentasF(ventasf: VentasF) {
    return this.http.post(`${this.URI}/ventasf/crear.php`, ventasf);
    //return this.http.post(`${this.URI}/ventasf`, ventasf);
  }

  actualizarVentasF(id: string, datoActualizado: VentasF,usuario: string): Observable<VentasF> {
    var datos = {
      'id': id,
      'cedula': datoActualizado.cedula,
      'pasaporte': datoActualizado.pasaporte,
      'telefono': datoActualizado.telefono,
      'usuario': usuario,
    };
    return this.http.post(`${this.URI}/ventasf/actualizar.php`, datos);
    //return this.http.put(`${this.URI}/ventasf/${id}`, datoActualizado);
  }

  actualizarVentasFPagado(id: any,usuario: string): Observable<VentasF> {
    var datos = {
      'id': id,
      'usuario': usuario,
    };
    return this.http.post(`${this.URI}/ventasf/actualizarPagado.php`, datos);
    //return this.http.put(`${this.URI}/ventasf/${id}`, datoActualizado);
  }

  actualizarVentasFPagadoEliminar(id: any, usuario:string): Observable<VentasF> {
    var datos = {
      'id': id,
      'usuario': usuario,
    };
    return this.http.post(`${this.URI}/ventasf/actualizarEliminar.php`, datos);
    //return this.http.put(`${this.URI}/ventasf/${id}`, datoActualizado);
  }
}
