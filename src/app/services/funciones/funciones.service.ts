import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Funciones } from '../../models/Funciones'
import { Observable } from 'rxjs';
import { EmmitterService } from '../emitter/emitter.service';

@Injectable({
  providedIn: 'root'
})
export class FuncionesService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient,
    private _evetEmiter: EmmitterService,) { }

  getFunciones() {
    //return this.http.get(`${this.URI}/funciones`);
    return this.http.get('https://mikuy.com.ec/bdd/funciones/lista.php');
  }

  getFuncion(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post('https://mikuy.com.ec/bdd/funciones/buscar.php', datos);
  }

  getFuncionXNombre(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    //return this.http.get(`${this.URI}/funciones/buscar/${nombre}`);
    console.log(datos);
    return this.http.post('https://mikuy.com.ec/bdd/funciones/buscarxNombre.php', datos);
  }

  getRFun(id: string, tabla: string) {
    var scope = {
      'id': id,
      'tabla': tabla
    };
    return this.http.post('https://mikuy.com.ec/bdd/funciones/rfun.php', scope);
  }

  getRolFuncion(id: string) {
    let tabla: any = []
    var datos = {
      'id': id
    };
    return this.http.post('https://mikuy.com.ec/bdd/funciones/buscarRolFuncion.php', datos).subscribe(
      //return this.http.get(`${this.URI}/funciones/funcion/${id}`).subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['tabla'] == 'roles') {
            this._evetEmiter.setMenuRoles(true);
            console.log('roles ---' + this._evetEmiter.getMenuRoles());
          }
          if (tabla[key]['tabla'] == 'funciones') {
            this._evetEmiter.setMenuFunciones(true);
          }
          if (tabla[key]['tabla'] == 'categoria') {
            this._evetEmiter.setMenuCategorias(true);
          }
          if (tabla[key]['tabla'] == 'usuarios') {
            this._evetEmiter.setMenuUsuarios(true);
          }
          if (tabla[key]['tabla'] == 'producto') {
            this._evetEmiter.setMenuProductos(true);
          }
          if (tabla[key]['tabla'] == 'videos') {
            this._evetEmiter.setMenuVideos(true);
          }
          if (tabla[key]['tabla'] == 'publicidad') {
            this._evetEmiter.setMenuPublicidad(true);
          }
          if (tabla[key]['tabla'] == 'ventasf') {
            this._evetEmiter.setMenuVentas(true);
          }
          if (tabla[key]['tabla'] == 'mantenimiento') {
            this._evetEmiter.setMenuMantenimiento(true);
          }
          if (tabla[key]['tabla'] == 'iva') {
            this._evetEmiter.setMenuIva(true);
          }
          if (tabla[key]['tabla'] == 'stock') {
            this._evetEmiter.setMenuStock(true);
          }
          //console.log(this.tabla[key]['tabla']);
        });
        //console.log(this.tabla[0]['tabla'])
      },
      err => console.error(err)
    );
    //return this.http.get(`${this.URI}/roles/funcion/`,{params});
  }

  eliminarFuncion(id: string) {
    //return this.http.delete(`${this.URI}/funciones/${id}`);
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/funciones/eliminar.php`, datos);
  }

  guardarFuncion(funciones: Funciones) {
    return this.http.post(`${this.URI}/funciones/crear.php`, funciones);
    //return this.http.post(`${this.URI}/funciones`, funciones);
  }

  actualizarFuncion(id: string | number | undefined, datoActualizado: Funciones): Observable<Funciones> {
    return this.http.post(`${this.URI}/funciones/actualizar.php`, datoActualizado);
    //return this.http.put(`${this.URI}/funciones/${id}`, datoActualizado);
  }
}
