import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuarios } from '../../models/Usuarios'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UsuariosLogin } from 'src/app/models/UsuariosLogin';
import { EmmitterService } from '../emitter/emitter.service';
import { Router, ActivatedRoute } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import jwt_decode from "jwt-decode";
import { FuncionesService } from '../funciones/funciones.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  URI = 'http://localhost:3000'
  URI1 = 'https://mikuy.com.ec/bdd'



  constructor(private http: HttpClient,
    private router: Router,
    private _evetEmiter: EmmitterService,
    private jwtHelper: JwtHelperService,
    private funcionesService: FuncionesService) { }

  private leerToken(): void { }
  private guardarToken(): void { }
  private handleError(): void { }

  getUsuarios() {
    return this.http.get(`${this.URI1}/usuarios/lista.php`);
    //return this.http.get(`${this.URI}/usuarios`);
  }

  getUsuariosXNombre(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI1}/usuarios/buscarxnombre.php`, datos);
  }

  getAuth(email: any, contrasena: any) {

    var datos = {
      'email': email
    };

    return this.http.post(`${this.URI1}/usuarios/login.php`, datos);

  }


  verificarToken(): void {
    /*const token = localStorage.getItem('token');
    const expirado = this.jwtHelper.isTokenExpired(token);
    console.log()*/
  }

  getUsuario(id: string) {
    var datos = {
      'id': id
    };
    console.log(datos)
    return this.http.post(`${this.URI1}/usuarios/buscar.php`, datos);
    //return this.http.get(`${this.URI}/usuarios/${id}`);

  }

  eliminarUsuario(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI1}/usuarios/eliminar.php`, datos);
    //return this.http.delete(`${this.URI}/usuarios/${id}`);
  }

  guardarUsuario(usuario: Usuarios,archivos: any) {
     /*const img = new FormData();
     img.append('myImage', archivos);

    var datos = {
      'usuario': usuario,
      'image': img
    };*/

    const formData = new FormData();
    formData.append('email', usuario.email);
    formData.append('nombre', usuario.nombre);
    formData.append('apellido', usuario.apellido);
    formData.append('direccion', usuario.direccion);
    formData.append('session_token', usuario.session_token);
    formData.append('contrasena', usuario.contrasena);
    formData.append('imagen', archivos);

    return this.http.post(`${this.URI1}/usuarios/crear.php`, formData);
    //return this.http.post(`${this.URI}/usuarios`, usuario);
  }



  enviarContacto(nombre: string, detalle: string, email: string) {
    var datos = {
      'nombre': nombre,
      'detalle': detalle,
      'email': email
    };
    return this.http.post(`${this.URI1}/usuarios/enviar.php`, datos);
    //return this.http.get(`${this.URI}/usuarios/enviar/email/${nombre}/${email}/${detalle}`);

  }

  enviarToken(token: string, email: string) {
    var datos = {
      'token': token,
      'email': email
    };
    return this.http.post(`${this.URI1}/usuarios/enviar_token.php`, datos);
    //return this.http.get(`${this.URI}/usuarios/enviar/email/${nombre}/${email}/${detalle}`);

  }

  actualizarUsuario1(id: string | number | undefined, datoActualizado: Usuarios): Observable<Usuarios> {
    return this.http.post(`${this.URI1}/usuarios/actualizar.php`, datoActualizado);
    // return this.http.put(`${this.URI}/usuarios/${id}`, datoActualizado);
  }

  
  actualizarUsuario(usuario: Usuarios,archivos: any, email:any) {
    const formData = new FormData();
    formData.append('id', usuario.id.toString());
    formData.append('email', usuario.email);
    formData.append('nombre', usuario.nombre);
    formData.append('apellido', usuario.apellido);
    formData.append('direccion', usuario.direccion);
    formData.append('contrasena', usuario.contrasena);
    formData.append('imagen', usuario.imagen);
    formData.append('image', archivos);
    formData.append('control', email.toString());
    return this.http.post(`${this.URI1}/usuarios/actualizarUsuario.php`, formData);
    // return this.http.put(`${this.URI}/usuarios/${id}`, datoActualizado);
  }

  
  actualizarUsuarioContra(email: string | undefined, contrasena: string | undefined, datoActualizado: Usuarios): Observable<Usuarios> {
    var datos = {
      'contrasena': contrasena,
      'email': email,
      'contrasena1': datoActualizado.contrasena,
    };
    //return this.http.put(`${this.URI}/usuarios/contrasena/${email}/${contrasena}/`, datoActualizado);
    return this.http.post(`${this.URI1}/usuarios/actualizarContrasena.php`, datos);
  }



  logout(): boolean {
    localStorage.removeItem('token');
    localStorage.clear();
    return true;
  }

  isAuth(): boolean {
    const token = localStorage.getItem('token');
    //if(!localStorage.getItem('token')){

    if (token) {
      if (this.jwtHelper.isTokenExpired(token) || !localStorage.getItem('token')) {
        localStorage.removeItem('token');
        return false;
      }

    } else {
      localStorage.removeItem('token');
      return false;
    }

    return true;
  }

  acceso(tabla: string) {
    const token = localStorage.getItem('token');
    let rol: string = '';
    if (token) {
      const datos: any = jwt_decode(token);
      //console.log(datos['rol']);
      rol = datos['rol'];
    }
    return this.http.get(`${this.URI}/roles/funciones/${rol}/${tabla}`);
  }

  controlSesion(){
    
    const valor = localStorage.getItem('valor');
      const now = Date.now();
      const timeSinceLastAction = now - parseInt(valor);
      const fiveMinutes = 5 * 60 * 1000;
      if (timeSinceLastAction > fiveMinutes) {
        localStorage.clear();
        window.location.reload()
      }else{
        localStorage.setItem("valor", Date.now().toString());
      }
  }

}
