import { TestBed } from '@angular/core/testing';
import { UsuariosService } from './usuarios.service';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('UsuariosService', () => {
  let service: UsuariosService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JwtHelperService,
        HttpClient,
        HttpHandler,
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS }], // Asegúrate de proporcionar JwtHelperService aquí
    });
    service = TestBed.inject(UsuariosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // Otros casos de prueba...
});
