import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Publicidad } from 'src/app/models/Publicidad';
import { Observable } from 'rxjs';
import { EmmitterService } from '../emitter/emitter.service';


@Injectable({
  providedIn: 'root'
})
export class PublicidadService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient,
    private _evetEmiter: EmmitterService,) { }


  getPublicidades() {
    return this.http.get(`${this.URI}/publicidad/lista.php`);
  }

  getPublicidadLista() {
    return this.http.get(`${this.URI}/publicidad/lista`);
  }

  getPublicidadFecha() {
    return this.http.get(`${this.URI}/publicidad/buscarxfecha.php`);
  }

  getPublicidad(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/publicidad/seleccionar.php`, datos);
  }


  getPublicidadXNombre(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/publicidad/buscarxnombre.php`, datos);
  }

  eliminarPublicidad(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/publicidad/eliminar.php`, datos);
  }

  guardarPublicidad(publicidad: Publicidad,archivos: any) {
    const formData = new FormData();
    formData.append('nombre', publicidad.nombre);
    formData.append('finicio', publicidad.finicio);
    formData.append('ffin', publicidad.ffin);
    formData.append('usuario', publicidad.usuario.toString());
    formData.append('imagen', archivos);
    return this.http.post(`${this.URI}/publicidad/crear.php`, formData);
  }

  actualizarPublicidad(publicidad: Publicidad,archivos: any): Observable<Publicidad> {
    const formData = new FormData();
    formData.append('id', publicidad.id.toString());
    formData.append('nombre', publicidad.nombre);
    formData.append('finicio', publicidad.finicio);
    formData.append('ffin', publicidad.ffin);
    formData.append('imagen', publicidad.imagen);
    formData.append('image', archivos);
    return this.http.post(`${this.URI}/publicidad/actualizar.php`, formData);
  }
}
