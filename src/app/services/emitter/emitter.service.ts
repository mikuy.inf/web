import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class EmmitterService {

  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  dataEstado = new EventEmitter();
  constructor() { }
  enviarEstado(estado: boolean) {
    this.dataEstado.emit(estado);
  }

  private login: string = '';
  private rol: string = '';

  private showHeader: boolean = false;
  private showMenuRoles: boolean = false;
  private showMenuFunciones: boolean = false;
  private showMenuUsuarios: boolean = false;
  private showMenuCategorias: boolean = false;
  private showMenuProductos: boolean = false;
  private showMenuVentas: boolean = false;
  private showMenuVideos: boolean = false;
  private showMenuPublicidad: boolean = false;
  private showMenuMantenimiento: boolean = false;
  private showMenuIva: boolean = false;
  private showMenuStock: boolean = false;

  //funciones para roles
  private showFRolesC: boolean = false;
  private showFRolesR: boolean = false;
  private showFRolesU: boolean = false;
  private showFRolesD: boolean = false;

  //funciones para roles
  private showFFuncionesC: boolean = false;
  private showFFuncionesR: boolean = false;
  private showFFuncionesU: boolean = false;
  private showFFuncionesD: boolean = false;

  //funciones para roles
  private showFUsuariosC: boolean = false;
  private showFUsuariosR: boolean = false;
  private showFUsuariosU: boolean = false;
  private showFUsuariosD: boolean = false;

  //funciones para roles
  private showFCategoriasC: boolean = false;
  private showFCategoriasR: boolean = false;
  private showFCategoriasU: boolean = false;
  private showFCategoriasD: boolean = false;



  public setMenuMantenimiento(flag: boolean) {
    this.showMenuMantenimiento = flag;
  }

  public getMenuMantenimiento(): boolean {
    return this.showMenuMantenimiento;
  }

  public setRol(flag: string) {
    this.rol = flag;
  }

  public getRol(): string {
    return this.rol;
  }

  public setLogin(flag: string) {
    this.login = flag;
  }

  public getLogin(): string {
    return this.login;
  }

  public setMenuVentas(flag: boolean) {
    this.showMenuVentas = flag;
  }

  public getMenuVentas(): boolean {
    return this.showMenuVentas;
  }

  public setMenuProductos(flag: boolean) {
    this.showMenuProductos = flag;
  }

  public getMenuProductos(): boolean {
    return this.showMenuProductos;
  }

  public setMenuCategorias(flag: boolean) {
    this.showMenuCategorias = flag;
  }

  public getMenuCategorias(): boolean {
    return this.showMenuCategorias;
  }

  public setMenuUsuarios(flag: boolean) {
    this.showMenuUsuarios = flag;
  }

  public getMenuUsuarios(): boolean {
    return this.showMenuUsuarios;
  }

  public setMenuFunciones(flag: boolean) {
    this.showMenuFunciones = flag;
  }

  public getMenuFunciones(): boolean {
    return this.showMenuFunciones;
  }

  public setMenuVideos(flag: boolean) {
    this.showMenuVideos = flag;
  }

  public getMenuVideos(): boolean {
    return this.showMenuVideos;
  }

  public setMenuPublicidad(flag: boolean) {
    this.showMenuPublicidad = flag;
  }

  public getMenuPublicidad(): boolean {
    return this.showMenuPublicidad;
  }

  public setHeader(flag: boolean) {
    this.showHeader = flag;
  }

  public getHeader(): boolean {
    return this.showHeader;
  }

  public setMenuRoles(flag: boolean) {
    this.showMenuRoles = flag;
  }

  public getMenuRoles(): boolean {
    return this.showMenuRoles;
  }

  public setMenuIva(flag: boolean) {
    this.showMenuIva = flag;
  }

  public getMenuIva(): boolean {
    return this.showMenuIva;
  }

  public setMenuStock(flag: boolean) {
    this.showMenuStock = flag;
  }

  public getMenuStock(): boolean {
    return this.showMenuStock;
  }
}