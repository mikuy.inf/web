import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Iva } from '../../models/Iva'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IvaService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient) { }

  getIvas() {
    return this.http.get(`${this.URI}/iva/lista.php`);
  }

  getIva(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/iva/seleccionar.php`, datos);
  }

  getIvaXNombre(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/iva/buscarxnombre.php`, datos);
  }



  eliminarIva(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/iva/eliminar.php`, datos);
  }

  guardarIva(iva: Iva) {
    return this.http.post(`${this.URI}/iva/crear.php`, iva);
  }

  actualizarIva(id: string | number | undefined, datoActualizado: Iva): Observable<Iva> {
    return this.http.post(`${this.URI}/iva/actualizar.php`, datoActualizado);
  }

}
