import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tablas } from '../../models/Tablas'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TablasService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient) { }

  getTablas() {
    return this.http.get(`${this.URI}/tablas/lista.php`);
    //return this.http.get(`${this.URI}/tablas`);
  }



}
