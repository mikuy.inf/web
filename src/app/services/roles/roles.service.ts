import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Roles } from '../../models/Roles'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient) { }

  getRoles() {
    return this.http.get(`${this.URI}/roles/lista.php`);
  }

  getRol(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/roles/seleccionar.php`, datos);
  }

  getRolXNombre(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/roles/buscarxnombre.php`, datos);
  }

  getRolFuncion(id: string, tabla: string) {
    var datos = {
      'rol': id,
      'tabla': tabla
    };
    return this.http.post(`${this.URI}/roles/buscarfuncion.php`, datos);
  }

  eliminarRol(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/roles/eliminar.php`, datos);
  }

  guardarRol(usuario: Roles) {
    return this.http.post(`${this.URI}/roles/crear.php`, usuario);
  }

  actualizarRol(id: string | number | undefined, datoActualizado: Roles): Observable<Roles> {
    return this.http.post(`${this.URI}/roles/actualizar.php`, datoActualizado);
  }

}
