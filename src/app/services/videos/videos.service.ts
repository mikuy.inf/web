import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Videos } from '../../models/Videos'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient) { }

  getVideos() {
    return this.http.get(`${this.URI}/videos/lista.php`);
  }

  getVideo(id: string) {
    var datos = {
      'id': id
    };
    console.log(datos)
    return this.http.post(`${this.URI}/videos/seleccionar.php`, datos);
  }

  getVideoXNombre(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/videos/buscarxnombre.php`, datos);
  }



  eliminarVideo(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/videos/eliminar.php`, datos);
  }

  guardarVideo(videos: Videos) {
    return this.http.post(`${this.URI}/videos/crear.php`, videos);
  }

  actualizarVideo(id: string | number | undefined, datoActualizado: Videos): Observable<Videos> {
    return this.http.post(`${this.URI}/videos/actualizar.php`, datoActualizado);
  }

}
