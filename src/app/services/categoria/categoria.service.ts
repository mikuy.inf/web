import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Categorias } from '../../models/Categorias'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  URI = 'https://mikuy.com.ec/bdd'

  constructor(private http: HttpClient) { }

  getCategorias() {
    return this.http.get(`${this.URI}/categorias/lista.php`);
  }

  getCategoriasXNombre(nombre: string) {
    var datos = {
      'nombre': nombre
    };
    return this.http.post(`${this.URI}/categorias/buscarxnombre.php`, datos);
  }

  getCategoria(id: string) {
    var datos = {
      'id': id
    };
    console.log(datos)
    return this.http.post(`${this.URI}/categorias/seleccionar.php`, datos);
  }

  eliminarCategoria(id: string) {
    var datos = {
      'id': id
    };
    return this.http.post(`${this.URI}/categorias/eliminar.php`, datos);
  }

  guardarCategoria(categoria: Categorias,archivos: any) {
    const formData = new FormData();
    formData.append('nombre', categoria.nombre);
    formData.append('descripcion', categoria.descripcion);
    formData.append('imagen', archivos);
    return this.http.post(`${this.URI}/categorias/crear.php`, formData);
  }

  actualizarCategoria(categoria: Categorias,archivos: any): Observable<Categorias> {
    const formData = new FormData();
    formData.append('id', categoria.id.toString());
    formData.append('nombre', categoria.nombre);
    formData.append('descripcion', categoria.descripcion);
    formData.append('estado', categoria.estado.toString());
    formData.append('imagen', categoria.imagen);
    formData.append('image', archivos);
    return this.http.post(`${this.URI}/categorias/actualizar.php`, formData);
  }
}
