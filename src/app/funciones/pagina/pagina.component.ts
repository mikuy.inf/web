import { Component, OnInit } from '@angular/core';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { RolesService } from 'src/app/services/roles/roles.service';
import { TablasService } from 'src/app/services/tablas/tablas.service';
import { Roles } from 'src/app/models/Roles'
import { Funciones } from 'src/app/models/Funciones';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {
  page = 1;
  pageSize = 12;
  btnModificar = 0;
  btnCrear = 1;
  funciones: any = [];
  roles: any = [];
  tablas: any = [];
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;
  private idUs: number = 0;
  edit: boolean = false;

  rolSel: Roles = {
    id: 0,
    nombre: '',
    descripcion: ''
  };

  funcion: Funciones = {
    id: 0,
    rol: 0,
    tabla: 'Seleccionar Tabla',
    crear: 0,
    leer: 0,
    actualizar: 0,
    eliminar: 0,
    nombreRol: ''
  }

  funcionBlanco: Funciones = {
    id: 0,
    rol: 0,
    tabla: 'Seleccionar Tabla',
    crear: 0,
    leer: 0,
    actualizar: 0,
    eliminar: 0,
    nombreRol: ''
  }
  closeResult = '';
  nombreEliminar = '';
  tablaB: string = '';

  strRoles: string = "* El Rol es Obligatorio";
  strTablas: string = "* La Tabla Obligatoria";
  errorRoles: boolean = false;
  errorTablas: boolean = false;

  constructor(private funcionesService: FuncionesService,
    private rolesService: RolesService,
    private tablasService: TablasService,
    private mantenimientoService: MantenimientoService,
    private router: Router,
    public modal: NgbModal,
    private usuariosService: UsuariosService) { }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];
      this.litarFunciones();
      this.litarRoles();
      this.litarTablas();
      this.cargarFuncion();
    } else {
      this.router.navigate(['/login']);
    }


  }

  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'funciones').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarFunciones() {
    this.funcionesService.getFunciones().subscribe(
      res => {
        this.funciones = res;
      },
      err => console.error(err)
    );
  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Funciones';


    doc.text(header, 225, 45, { baseline: 'top' });
    doc.setFontSize(10);
    doc.text("Observación: 0 - No tiene permiso , 1 - Si tiene permiso", 40, 80);
    let info = []

    this.funciones.forEach((element, index, array) => {
      info.push([element.id, element.nombreRol, element.tabla, element.crear, element.leer, element.actualizar, element.eliminar])

    })

    autoTable(doc, {
      head: [["Id", "Nombre Rol", "Nombre Tabla", "Crear", "Leer", "Actualizar", "Eliminar"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_funciones.pdf");
  }

  litarRoles() {
    this.rolesService.getRoles().subscribe(
      res => {
        this.roles = res;
      },
      err => console.error(err)
    );
  }

  litarTablas() {
    this.tablasService.getTablas().subscribe(
      res => {
        console.log(res)
        this.tablas = res;
      },
      err => console.error(err)
    );
  }

  mostrarRol(id: string) {
    this.usuariosService.controlSesion();
    this.rolesService.getRol(id).subscribe(
      res => {
        this.rolSel = res;
      },
      err => console.error(err)
    );
    return this.rolSel.nombre;
  }

  controlErrores(): boolean {
    if (this.funcion.rol == null || this.funcion.rol <= 0) {
      this.errorRoles = true;

      return false;
    } else {
      this.errorRoles = false;

    }


    if (this.funcion.tabla == null || this.funcion.tabla == '0' || this.funcion.tabla == 'Seleccionar Tabla') {
      this.errorTablas = true;

      return false;
    } else {
      this.errorTablas = false;

    }

    return true;
  }

  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.funcion.id;
      delete this.funcion.nombreRol;
      console.log(this.funcion);
      this.funcionesService.guardarFuncion(this.funcion)
        .subscribe(
          res => {

            this.guardarMantenimiento("Crear", "Funciones");
            window.location.reload();
          },
          err => console.error(err)
        )
    }
  }

  seleccionarFuncion(id: string) {
    this.usuariosService.controlSesion();
    this.funcionesService.getFuncion(id).subscribe(
      res => {
        this.btnModificar = 1;
        this.btnCrear = 0;
        this.funcion = res[0];
      },
      err => console.error(err)
    );
  }

  cancelar() {
    this.usuariosService.controlSesion();
    this.funcion = { ...this.funcionBlanco};
    this.btnModificar = 0;
    this.btnCrear = 1;
  }

  actualizar() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.funcion.nombreRol;
      console.log(this.funcion);
      this.funcionesService.actualizarFuncion(this.funcion.id, this.funcion)
        .subscribe(
          res => {
            
            this.guardarMantenimiento("Actualizar", "Funciones");
            window.location.reload();
          },
          err => console.error(err)
        )
    }
  }

  eliminarFuncion(id: string) {
    this.usuariosService.controlSesion();
    this.funcionesService.eliminarFuncion(id)
      .subscribe(
        res => {


          this.guardarMantenimiento("Eliminar", "Funciones");
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarFuncion(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.tablaB.length > 0) {
      console.log("entro 1: " + this.tablaB)
      this.funcionesService.getFuncionXNombre(this.tablaB).subscribe(
        res => {
          console.log("entro 2")
          this.funciones = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => {
          console.log("entro 3: " + err)
          console.error(err)
          this.funciones = [];
        }
      );
    } else {
      console.log("entro 4")
      this.litarFunciones();
    }

  }


  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }

}
