import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Router, ActivatedRoute } from '@angular/router';
import { VideosService } from 'src/app/services/videos/videos.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css'],
  providers: [NgbCarouselConfig]
})
export class PaginaComponent implements OnInit {

  title = 'ng-carousel-demo';



  images = [

    { title: '', short: '', src: "../../../assets/imagenes/Fotoportadaweb5edit.jpg" },
    { title: '', short: '', src: "../../../assets/imagenes/2.jpg" },


  ];

  constructor(private videosService: VideosService,
    public modal: NgbModal,
    private sanitizer: DomSanitizer,
    private router: Router,
    config: NgbCarouselConfig,
    private usuariosService: UsuariosService) {
    config.interval = 2000;
    config.keyboard = true;
    config.pauseOnHover = true;
  }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
    }


  }




}
