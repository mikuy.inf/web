import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './pagina/inicio/inicio.component';


import { CabeceraComponent } from './componentes/cabecera/cabecera.component';
import { MenuComponent } from './componentes/menu/menu.component';

import { RouterModule } from '@angular/router';
import { AdministracionComponent } from './componentes/administracion/administracion.component';
import { FormsModule } from '@angular/forms';
import { IniciarsesionComponent } from './componentes/iniciarsesion/iniciarsesion.component';
import { SeparadorComponent } from './componentes/separador/separador.component';
import { PiedePaginaComponent } from './componentes/piedepagina/piedepagina.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    InicioComponent,
    CabeceraComponent,
    MenuComponent,
    AdministracionComponent,
    IniciarsesionComponent,
    SeparadorComponent,
    PiedePaginaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgbPaginationModule,
    NgbModule
  ],
  exports: [
    InicioComponent,
    CabeceraComponent,
    MenuComponent,
    AdministracionComponent,
    IniciarsesionComponent,
    SeparadorComponent,
    PiedePaginaComponent
  ]
})
export class UtilidadesModule { }