import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { EmmitterService } from 'src/app/services/emitter/emitter.service';


@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit {

  @Input() estado: boolean = true;
  
 
  constructor(public _eventEmitter: EmmitterService) {
    
   }

  ngOnInit(): void {
    //this.loginbtn=environment.login;
    
  }

}
