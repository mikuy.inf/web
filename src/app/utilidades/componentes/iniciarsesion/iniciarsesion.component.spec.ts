import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IniciarsesionComponent } from './iniciarsesion.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JWT_OPTIONS } from '@auth0/angular-jwt';

describe('Iniciar Sesión', () => {


    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
            ],
            declarations: [
                IniciarsesionComponent,
                
            ],
            providers: [
              { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
              JwtHelperService,
            ]
            
        }).compileComponents();

      
        
    });

   
    it('Controlar crear IniciarsesionComponent', () => {
        const fixture = TestBed.createComponent(IniciarsesionComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();

    });

    it('Al iniciar email y contraseña deben estar en blanco', () => {
        const fixture = TestBed.createComponent(IniciarsesionComponent);
        const app = fixture.componentInstance;
        expect(app.usuario.email).toBe('');
        expect(app.usuario.contrasena).toBe('');
      });

      it('Verificar el funcionamiento del boton iniciar sesión', () => {
        
        const fixture = TestBed.createComponent(IniciarsesionComponent);
        const component = fixture.componentInstance;
      
        spyOn(component, 'login'); // Espiar el método login
      
        const button = fixture.nativeElement.querySelector('.botonI');
        button.click();
      
        expect(component.login).toHaveBeenCalled();
      });

      it('Control que se abra el botón modal para recuperar contraseña', () => {
        const fixture = TestBed.createComponent(IniciarsesionComponent);
        const component = fixture.componentInstance;
        spyOn(component, 'openF');
        const link = fixture.nativeElement.querySelector('a');
        link.click();
        expect(component.openF).toHaveBeenCalled();
      });
});
