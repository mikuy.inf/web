import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UsuariosLogin } from 'src/app/models/UsuariosLogin';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { CabeceraComponent } from '../cabecera/cabecera.component';
import { EmmitterService } from 'src/app/services/emitter/emitter.service';
import * as CryptoJS from 'crypto-js';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import jwt_decode from "jwt-decode";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Usuarios } from 'src/app/models/Usuarios';

@Component({
  selector: 'app-iniciarsesion',
  templateUrl: './iniciarsesion.component.html',
  styleUrls: ['./iniciarsesion.component.css']
})
export class IniciarsesionComponent implements OnInit {

  valEmail: '';
  usuario: UsuariosLogin = {
    email: '',
    contrasena: ''
  }
  ocultar = false
  longEmail: number | undefined = 0;
  longPassword: number | undefined = 0;

  strEmail: string = "* Correo Electrónico";
  strPassword: string = "* Contraseña";
  strDatos: string = "Ingresar Datos de Usuario";

  errorEmail: boolean = false;
  errorPassword: boolean = false;
  errorDatos: boolean = false;

  error: boolean;

  strEmailR: string = "Email Obligarorio";

  errorEmailR: boolean = false;

  usuarioEnv: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(Date.now() + 24 * 60 * 60 * 1000),

  }

  constructor(private usuariosServicio: UsuariosService,
    private router: Router, public _evetEmiter: EmmitterService,
    private funcionesService: FuncionesService,
    public modalF: NgbModal,

  ) {

    this.error = false;
  }

  ngOnInit(): void {
    console.log("Ingresar");
    const token = localStorage.getItem('token');

    if (token) {
      this.router.navigate(['/']);

    }
    this.ocultar = false
  }

  vista() {
    this.ocultar = !this.ocultar
  }


  controlErrores(): boolean {

    this.longEmail = this.usuario['email']?.length;
    this.longPassword = this.usuario['contrasena']?.length;
    console.log(this.longPassword)
    if (this.longEmail == 0) {
      this.errorEmail = true;
      this.strEmail = "El Correo Electrónico es Obligatorio";
      return false;
    } else {
      this.errorEmail = false;
      this.strEmail = "* Correo Electrónico";
    }
    const valEma: string = this.usuario['email'] as string;

    if (!this.esEmailValido(valEma)) {
      this.errorEmail = true;
      this.strEmail = "El Correo Electrónico es Incorrecto";
      return false;
    } else {
      this.errorEmail = false;
      this.strEmail = "* Correo Electrónico";
    }

    if (this.longPassword == 0) {
      this.errorPassword = true;
      this.strPassword = "La Contraseña es Obligatoria";
      return false;
    } else {
      this.errorPassword = false;
      this.strPassword = "* Contraseña";
    }

    return true;
  }

  esEmailValido(email: string): boolean {
    let mailValido = false;
    'use strict';

    var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (email.match(EMAIL_REGEX)) {
      mailValido = true;
    }
    return mailValido;
  }

  login() {
    this.errorEmail = false;
    this.errorDatos = false;
    this.errorPassword = false;
    if (this.controlErrores()) {

      const en: string = this.usuario.contrasena as string;
      const hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(en));
      const md5 = hash.toString(CryptoJS.enc.Hex)

      this.usuariosServicio.getAuth(this.usuario.email, md5).
        subscribe((resp: any) => {

          
          if (resp.includes("No hay resultados")) {
            this.errorEmail = true;
            this.strEmail = "El Correo Electrónico No está registrado";
  
          }else{
            const datos: any = jwt_decode(resp);
          
          if (datos[0]['session_token'] != null && datos[0]['session_token'].length > 0) {
            if (datos[0]['contrasena'] == md5) {
              localStorage.setItem("token", resp);
              localStorage.setItem("valor", Date.now().toString());
              this.funcionesService.getRolFuncion(this._evetEmiter.getRol());
              this._evetEmiter.setHeader(true)
              window.location.reload();

            } else {
              this.errorPassword = true;
              this.strPassword = "La contraseña es incorrecta";
            }

          } else {
            this.errorDatos = true;
            this.strDatos = "No ha validado la cuenta";
          }
          }
          
        }, (err) => {

          //this.errorDatos = true;
          //this.strDatos = "No existe Correo Electrónico";
          this.errorEmail = true;
          this.strEmail = "El Correo Electrónico No está registrado";

        }
        );

    }
  }

  openF(content: any) {

    this.modalF.open(content as never, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        var password = '';
        for (let c = 0; c < 8; c++) {
          password += this.getPasswordChar();
        }

        delete this.usuarioEnv.id;
        delete this.usuarioEnv.nombre;
        delete this.usuarioEnv.apellido;
        delete this.usuarioEnv.imagen;
        delete this.usuarioEnv.direccion;
        delete this.usuarioEnv.fingreso;

        delete this.usuarioEnv.email;

        const en: string = password as string;
        const hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(en));
        const md5 = hash.toString(CryptoJS.enc.Hex)

        this.usuarioEnv.contrasena = md5;
        this.usuariosServicio.actualizarUsuarioContra(this.valEmail, password, this.usuarioEnv)
          .subscribe(
            res => {


              window.location.reload();
            },
            err => console.error(err)
          )

      },
      (reason) => {
        // this.control = 0;
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  getPasswordChar(): string {

    let option: number = Math.round(Math.random() * (4 - 1) + 1);
    let ch: string = '';

    switch (option) {
      case 1:

        return this.getCaracter(this.letras).toUpperCase();
        break;

      case 2:

        return this.getCaracter(this.letras);
        break;

      case 3:

        return this.getNumero();
        break;

      case 4:

        return this.getCaracter(this.simbolos);
        break;
    }
    return ch;
  }

  private letras: string = 'abcdefghijklmnñopqrstuvwxyz';
  private simbolos: string = '!$%/()=?_-@*#';
  getCaracter(chs: string): string {
    let chsCount = chs.length - 1;
    let chsSelect = Math.round(Math.random() * (chsCount - 0) + 0);
    return chs[chsSelect];
  }

  private getNumero(): string {
    return String(Math.round(Math.random() * (9 - 0) + 0));
  }


}
