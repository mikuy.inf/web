import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { EmmitterService } from 'src/app/services/emitter/emitter.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-piedepagina',
  templateUrl: './piedepagina.component.html',
  styleUrls: ['./piedepagina.component.css']
})
export class PiedePaginaComponent implements OnInit {
  @Output() onToggleExpanded: EventEmitter<boolean> = new EventEmitter<boolean>()  //emitir un valor de salida

  constructor(
  ) {

  }

  toggleExpanded() {


  }


  ngOnInit(): void {

  }



}