import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { EmmitterService } from 'src/app/services/emitter/emitter.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styleUrls: ['./administracion.component.css']
})
export class AdministracionComponent implements OnInit {
  @Output() onToggleExpanded: EventEmitter<boolean> = new EventEmitter<boolean>()  //emitir un valor de salida

  expanded = true
  valImagen: string = '';
  valNombre: string = '';
  imgDefecto: string = '../../../../assets/usuario.jpg';

  constructor(
    public _evetEmiter: EmmitterService,
    private usuariosService: UsuariosService,
    private router: Router
  ) {

  }

  toggleExpanded() {


  }


  ngOnInit(): void {
    const token = localStorage.getItem('token');

    if (token) {
      const datos: any = jwt_decode(token);
      //console.log(datos)
      this.valImagen = datos[0]['imagen'];
      this.valNombre = datos[0]['nombre'] + " " + datos[0]['apellido'];
    }
  }

  getNombreRecortado(nombre: string): string {
    // Verifica si el nombre es más largo de 5 caracteres y devuelve solo los primeros 5 caracteres
    return nombre.length > 15 ? nombre.substring(0, 15) : nombre;
  }

  cargarImagenError() {
    console.log("Error entro imagen")
    this.valImagen='../../../../assets/usuario.jpg';
  }

  cerrarSesion() {
    environment.login = true
    console.log("cerrar")
    this.usuariosService.logout();
    window.location.reload();
    this._evetEmiter.setHeader(false)
    this.router.navigate(['/']);
  }

}