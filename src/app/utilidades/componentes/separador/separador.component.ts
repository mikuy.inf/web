import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { EmmitterService } from 'src/app/services/emitter/emitter.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-separador',
  templateUrl: './separador.component.html',
  styleUrls: ['./separador.component.css']
})
export class SeparadorComponent implements OnInit {
  @Output() onToggleExpanded: EventEmitter<boolean> = new EventEmitter<boolean>()  //emitir un valor de salida

  constructor(
  ) {

  }

  toggleExpanded() {


  }


  ngOnInit(): void {

  }



}