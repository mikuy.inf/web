import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { EmmitterService } from 'src/app/services/emitter/emitter.service';
import { environment } from 'src/environments/environment';
import { MenuService, IMenu } from '../../../services/menu.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { RolesService } from 'src/app/services/roles/roles.service';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @Output() onToggleExpanded: EventEmitter<boolean> = new EventEmitter<boolean>()  //emitir un valor de salida
  listMenu: IMenu[]

  expanded = true

  menuRoles: boolean = false;
  menuFunciones: boolean = false;
  menuUsuarios: boolean = false;
  menuCategoria: boolean = false;
  menuProductos: boolean = false;
  menuVentas: boolean = false;
  menuMantenimiento: boolean = false;
  menuIva: boolean = false;
  menuStock: boolean = false;
  valImagen: string = '';

  tabla: any = [];
  constructor(
    private menuService: MenuService,
    private router: Router,
    public _evetEmiter: EmmitterService,
    private usuariosService: UsuariosService,
    private rolesService: RolesService,
    private funcionesServicios: FuncionesService
  ) {
    this.listMenu = menuService.getMenu()


  }

  toggleExpanded() {
    this.expanded = !this.expanded
    this.onToggleExpanded.emit(this.expanded)

  }


  ngOnInit(): void {
    console.log('OnInit Menu');
    const token = localStorage.getItem('token');

    if (token) {
      const datos: any = jwt_decode(token);
      console.log(datos)
      this.valImagen = datos['imagen'];
      this.menuRoles = this._evetEmiter.getMenuRoles();
      this.menuFunciones = this._evetEmiter.getMenuFunciones();
      this.menuUsuarios = this._evetEmiter.getMenuUsuarios();
      this.menuCategoria = this._evetEmiter.getMenuCategorias();
      this.menuProductos = this._evetEmiter.getMenuProductos();
      this.menuVentas = this._evetEmiter.getMenuVentas();
      this.menuMantenimiento = this._evetEmiter.getMenuMantenimiento();
      this.menuIva = this._evetEmiter.getMenuIva();
      this.menuStock = this._evetEmiter.getMenuStock();
    } else {

    }




  }

  cerrarSesion() {
    environment.login = true
    console.log("cerrar")
    this.usuariosService.logout();
    this._evetEmiter.setHeader(false)
    this.router.navigate(['/']);
  }

  ocultar(ventana: string) {
    //console.log('prueba consulta')
    //if(this.rolesService.getRolFuncion(vantana))
    /*this.usuariosService.acceso(ventana).subscribe(
      res => {
        //this.roles = res;
        console.log(res)
      },
      err => console.error(err)
    );*/
    //console.log(datos)
    return false;
  }

}