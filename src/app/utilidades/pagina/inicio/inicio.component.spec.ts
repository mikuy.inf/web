import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { InicioComponent } from './inicio.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UtilidadesModule } from '../../utilidades.module';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JWT_OPTIONS } from '@auth0/angular-jwt';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';


describe('Inicio - Home', () => {

    let usuariosService: UsuariosService;
   

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                UtilidadesModule,
                
            ],
            declarations: [
                InicioComponent,
                
            ],
            providers: [
              { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
              JwtHelperService,
            ]
        }).compileComponents();

        usuariosService = TestBed.inject(UsuariosService);
        
    });

   
    it('Controlar crear InicioComponent', () => {
        const fixture = TestBed.createComponent(InicioComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();

    });

    it('Controlar que se llame a controlSesion si el token existe', () => {
        const spyOnControlSesion = spyOn(usuariosService, 'controlSesion').and.callThrough();
        localStorage.setItem('token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3siaWQiOiIxIiwiZW1haWwiOiJkYXJ3aW5AZ21haWwuY29tIiwibm9tYnJlIjoiRGFyd2luIiwiYXBlbGxpZG8iOiJBZG1pbiIsImltYWdlbiI6Imh0dHBzOlwvXC9taWt1eS5jb20uZWNcL2JkZFwvaW1nXC82NDkyMWExMzdhMDBiLmpwZyIsImRpcmVjY2lvbiI6IkNkbGEgUHJpbmNpcGFsIGFtYmF0byIsInNlc3Npb25fdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlKOS5NalUuZlozMWUyajk4am1aSV83Sk41ZVQ0a3NtQTF4NUxTRGFzR0laLW9KQ3hNcyIsImNvbnRyYXNlbmEiOiJlM2FmZWQwMDQ3YjA4MDU5ZDBmYWRhMTBmNDAwYzFlNSIsImZpbmdyZXNvIjoiMjAyMi0xMi0xNCAwMzo1MjowNyJ9XQ.c8UUuNJhZQrV_p1vq6L90WUjruPOOv-YMfQLlauhxmo');
        const fixture = TestBed.createComponent(InicioComponent);
        const app = fixture.componentInstance;
        app.ngOnInit();
        fixture.detectChanges(); // Borrar la caché
        expect(spyOnControlSesion).toHaveBeenCalled();
      });

      it('Controlar que NO se llame a controlSesion si el token No existe', () => {
        const spyOnControlSesion = spyOn(usuariosService, 'controlSesion').and.callThrough();
        localStorage.removeItem('token');
        const fixture = TestBed.createComponent(InicioComponent);
        const app = fixture.componentInstance;
        app.ngOnInit();
        fixture.detectChanges(); // Borrar la caché
        expect(spyOnControlSesion).not.toHaveBeenCalled();
      });
});
