import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { PublicidadService } from 'src/app/services/publicidad/publicidad.service';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers: [NgbCarouselConfig]
})
export class InicioComponent implements OnInit {

  title = 'ng-carousel-demo';



  publicidad: any = [];
  categorias: any = [];
  constructor(private publicidadService: PublicidadService,
    private categoriasService: CategoriaService,
    config: NgbCarouselConfig,
    private usuariosService: UsuariosService
  ) {
    config.interval = 2000;
    config.keyboard = true;
    config.pauseOnHover = true;
  }

  ngOnInit(): void {
    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
    }

    
    this.litarPublicidad();
    this.litarCategoria();;

  }

  cargarImagenCategoria(index: number) {
    const datos = this.categorias[index];
    console.log("Error al cargar")
    datos.imagen = '../../../../assets/categoria.png';
  }

  litarPublicidad() {
    this.publicidadService.getPublicidadFecha().subscribe(
      res => {
        
        this.publicidad = res;

      },
      err => console.error(err)
    );
  }

  litarCategoria() {
    this.categoriasService.getCategorias().subscribe(
      res => {
        this.categorias = res;
      },
      err => console.error(err)
    );
  }

}
