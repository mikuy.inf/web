
import { Component, OnInit, HostBinding } from '@angular/core';
import { Roles } from 'src/app/models/Roles';
import { RolesService } from 'src/app/services/roles/roles.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Funciones } from 'src/app/models/Funciones';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  btnModificar = 0;
  btnCrear = 1;
  page = 1;
  pageSize = 12;
  roles: any = [];
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;

  rol: Roles = {
    id: 0,
    nombre: '',
    descripcion: ''
  };

  funcion: Funciones = {
    id: 0,
    rol: 0,
    tabla: '',
    crear: 0,
    actualizar: 0,
    leer: 0,
    eliminar: 0
  };

  rolBlanco: Roles = {
    id: 0,
    nombre: '',
    descripcion: ''
  };

  closeResult = '';
  nombreEliminar = '';

  nombreB: string = '';

  longNombre: number | undefined = 0;
  longDescripcion: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strDescripcion: string = "* Descripción Obligatorio";
  errorNombre: boolean = false;
  errorDescripcion: boolean = false;
  private idUs: number = 0;


  constructor(private rolesService: RolesService,
    public modal: NgbModal,
    private funcionesService: FuncionesService,
    private mantenimientoService: MantenimientoService,
    private router: Router,
    private usuariosService: UsuariosService
  ) { }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {

      const datos: any = jwt_decode(token);
      
      this.usuariosService.controlSesion();

      this.idUs = datos[0]['id'];

      this.litarRol();
      this.cargarFuncion();
    } else {
      this.router.navigate(['/login']);
    }

  }

  controlErrores(): boolean {

    this.longNombre = this.rol['nombre']?.length;
    this.longDescripcion = this.rol['descripcion']?.length;

    if (this.longNombre == 0) {
      this.errorNombre = true;
      this.strNombre = "El Nombre es Obligatorio";
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longDescripcion == 0) {
      this.errorDescripcion = true;
      this.strDescripcion = "La Descripcion es Obligatoria";
      return false;
    } else {
      this.errorDescripcion = false;

    }

    return true;
  }

  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'roles').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarRol() {
    console.log('cargando')
    this.rolesService.getRoles().subscribe(
      res => {
        this.roles = res;
        console.log(res)
        //this.roles=this.roles.slice(0,1);
      },
      err => console.error(err)
    );
  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Roles';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.roles.forEach((element, index, array) => {
      info.push([element.id, element.nombre, element.descripcion])

    })

    autoTable(doc, {
      head: [["Id", "Nomrbe", "Descripción"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_roles.pdf");
  }

  eliminarRol(id: string) {
    this.usuariosService.controlSesion();
    this.rolesService.eliminarRol(id)
      .subscribe(
        res => {
          console.log(res);
          try{
            const dataString = JSON.stringify(res); 
            const data = JSON.parse(dataString); 
            if (data.ok==false) { 
              alert("No puede eliminar, dato tiene relación con otras tablas"); 
            } else {
              this.guardarMantenimiento("Eliminar", "Roles");
              window.location.reload();
            }
          }catch(error){
            alert("No puede eliminar, dato tiene relación con otras tablas"); 
          }
          
        },
      err => {
        alert("No puede eliminar, dato tiene relación con otras tablas");
        //console.error(err)
      }
      )
  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }

  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.rol.id;

      this.rolesService.guardarRol(this.rol)
        .subscribe(
          res => {
            //this.rol = this.rolBlanco;

            this.guardarMantenimiento("Crear", "Roles");
            window.location.reload();

          },
          err => console.error(err)
        )
    }
  }

  seleccionarRol(id: string) {
    this.usuariosService.controlSesion();
    this.rolesService.getRol(id).subscribe(
      res => {
        this.btnModificar = 1;
        this.btnCrear = 0;
        this.rol = res[0];
        console.log(res);
      },
      err => console.error(err)
    );
  }

  actualizar() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {

      this.rolesService.actualizarRol(this.rol.id, this.rol)
        .subscribe(
          res => {
            this.guardarMantenimiento("Actualizar", "Roles");
            // this.rol = this.rolBlanco;
            window.location.reload();
          },
          err => console.error(err)
        )
    }
  }

  cancelar() {
    this.usuariosService.controlSesion();
    this.rol = { ...this.rolBlanco};
    this.btnModificar = 0;
    this.btnCrear = 1;
  }

  open(content: any, id: string, nombre: string) {
    this.usuariosService.controlSesion();
    this.nombreEliminar = nombre;
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarRol(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.rolesService.getRolXNombre(this.nombreB).subscribe(
        res => {
          this.roles = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => {
          //console.error(err)
          this.roles = [];
        }
      );
    } else {
      this.litarRol();
    }

  }
}
