import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InicioRoutingModule } from './inicio-routing.module';
import { PaginaComponent } from './pagina/pagina.component';
import { ElementosModule } from '../elementos/elementos.module';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    PaginaComponent
  ],
  imports: [ 
    CommonModule,
    InicioRoutingModule,
    ElementosModule,
    FormsModule,
    NgbPaginationModule
  ]
})  
export class InicioModule { }
