import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriaRoutingModule } from './categoria-routing.module';
import { ElementosModule } from '../../elementos/elementos.module';
import { PaginaComponent } from './pagina/pagina.component';
import { FormsModule } from '@angular/forms'; 
import { RouterLinkActive } from '@angular/router';

@NgModule({
  declarations: [
    PaginaComponent
  ],
  imports: [ 
    CommonModule,
    CategoriaRoutingModule,
    ElementosModule,
    FormsModule,
    RouterLinkActive
  ]
})  
export class CategoriaModule { }
