import { Component, HostBinding } from '@angular/core';
import { Categorias } from 'src/app/models/Categorias'
import { CategoriaService } from 'src/app/services/categoria/categoria.service'
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent {

  @HostBinding('class') clases = 'row';

  datos: Categorias = {
    id: 0,
    nombre: '',
    descripcion: '',
    estado: 1
  };

  edit: boolean = false;

  constructor(private categoriasService: CategoriaService, 
    private router: Router, 
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    
   if(params['id']){
    this.categoriasService.getCategoria(params['id'])
    .subscribe(
      res=>{
        this.datos = res;
        this.edit = true;
      },
      err=>console.error(err)
    )
   } 
  }

  saveNewCategoria() {
    
    delete this.datos.id;
    this.categoriasService.guardarCategoria(this.datos,"")
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/categorias']);
        },
        err => console.error(err)
      )
  }

  actualizar() {
    
    this.categoriasService.actualizarCategoria(this.datos,"")
      .subscribe(
        res => { 
          console.log(res);
          this.router.navigate(['/categorias']);
        },
        err => console.error(err)
      )
  }
}
