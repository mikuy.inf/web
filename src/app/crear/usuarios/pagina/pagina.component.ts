import { Component, HostBinding } from '@angular/core';
import { Usuarios } from 'src/app/models/Usuarios'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service'
import { Router, ActivatedRoute } from '@angular/router';
import { data } from 'jquery';
import { RolesService } from 'src/app/services/roles/roles.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as CryptoJS from 'crypto-js';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import validator from 'validator';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponentUsuario {
  @HostBinding('class') clases = 'row';


  roles: any = [];
  vista = false
  ocultar = 0;
  usuarios: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(),

  };
 
  usuario: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(Date.now() + 24 * 60 * 60 * 1000),

  };

  datoBlanco: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(Date.now() + 24 * 60 * 60 * 1000),

  };

  longNombre: number | undefined = 0;
  longApellido: number | undefined = 0;
  longEmail: number | undefined = 0;
  longCedula: number | undefined = 0;
  longTelefono: number | undefined = 0;
  longDireccion: number | undefined = 0;
  longContrasena: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strApellido: string = "* Apellido Obligatorio";
  strEmail: string = "* Email Obligatorio";
  strCedula: string = "* Cédula Obligatoria";
  strTelefono: string = "* Teléfono Obligatorio";
  strDireccion: string = "* Dirección Obligatorio";
  strContrasena: string = "* Contraseña Obligatorio";
  strRol: string = "* Rol Obligatorio";

  errorNombre: boolean = false;
  errorApellido: boolean = false;
  errorEmail: boolean = false;
  errorCedula: boolean = false;
  errorTelefono: boolean = false;
  errorDireccion: boolean = false;
  errorContrasena: boolean = false;
  errorRol: boolean = false;

  public archivos: any = []

  edit: boolean = false;

  constructor(private usuariosService: UsuariosService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rolesService: RolesService,
    private sanitizer: DomSanitizer) { }


  ngOnInit() {

    const token = localStorage.getItem('token');
    
    if (token) {


      this.router.navigate(['/']);

    } else {
     
      const params = this.activatedRoute.snapshot.params;
      this.vista = false
    }
   
  }

  vistaPas() {
    this.vista = !this.vista
  }

  validarCedula(cedula: string) {

    if (cedula.length === 10) {


      const digitoRegion = cedula.substring(0, 2);


      if (digitoRegion >= String(1) && digitoRegion <= String(24)) {


        const ultimoDigito = Number(cedula.substring(9, 10));


        const pares = Number(cedula.substring(1, 2)) + Number(cedula.substring(3, 4)) + Number(cedula.substring(5, 6)) + Number(cedula.substring(7, 8));


        let numeroUno: any = cedula.substring(0, 1);
        numeroUno = (numeroUno * 2);
        if (numeroUno > 9) {
          numeroUno = (numeroUno - 9);
        }

        let numeroTres: any = cedula.substring(2, 3);
        numeroTres = (numeroTres * 2);
        if (numeroTres > 9) {
          numeroTres = (numeroTres - 9);
        }

        let numeroCinco: any = cedula.substring(4, 5);
        numeroCinco = (numeroCinco * 2);
        if (numeroCinco > 9) {
          numeroCinco = (numeroCinco - 9);
        }

        let numeroSiete: any = cedula.substring(6, 7);
        numeroSiete = (numeroSiete * 2);
        if (numeroSiete > 9) {
          numeroSiete = (numeroSiete - 9);
        }

        let numeroNueve: any = cedula.substring(8, 9);
        numeroNueve = (numeroNueve * 2);
        if (numeroNueve > 9) {
          numeroNueve = (numeroNueve - 9);
        }

        const impares = numeroUno + numeroTres + numeroCinco + numeroSiete + numeroNueve;

        // Suma total
        const sumaTotal = (pares + impares);


        const primerDigitoSuma = String(sumaTotal).substring(0, 1);


        const decena = (Number(primerDigitoSuma) + 1) * 10;


        let digitoValidador = decena - sumaTotal;


        if (digitoValidador === 10) {
          digitoValidador = 0;
        }


        if (digitoValidador === ultimoDigito) {
          return true;
        } else {
          return false;
        }

      } else {

        return false;
      }
    } else {

      return false;
    }

  }

  controlErrores(): boolean {

    this.longNombre = this.usuario['nombre']?.length;
    this.longApellido = this.usuario['apellido']?.length;
    this.longEmail = this.usuario['email']?.length;
    // this.longCedula = this.usuario['cedula']?.length;
    this.longDireccion = this.usuario['direccion']?.length;
    //this.longTelefono = this.usuario['telefono']?.length;
    this.longContrasena = this.usuario['contrasena']?.length;

    if (this.longNombre == 0) {
      this.errorNombre = true;
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longApellido == 0) {
      this.errorApellido = true;
      return false;
    } else {
      this.errorApellido = false;

    }

    if (this.longEmail == 0) {
      this.errorEmail = true;
      this.strEmail = "* Email Obligatorio";
      return false;
    } else {
      this.errorEmail = false;

    }

    const valEma: string = this.usuario['email'] as string;

    if (!this.esEmailValido(valEma)) {
      this.errorEmail = true;
      this.strEmail = "El Correo Electrónico es Incorrecto";
      return false;
    } else {
      this.errorEmail = false;

    }

    /*if (this.longCedula == 0) {
      this.errorCedula = true;
      return false;
    } else {
      this.errorCedula = false;

    }*/

    if (this.longDireccion == 0) {
      this.errorDireccion = true;
      return false;
    } else {
      this.errorDireccion = false;

    }

    /*if (this.longTelefono == 0) {
      this.errorTelefono = true;
      return false;
    } else {
      this.errorTelefono = false;

    }*/

    if (this.longContrasena == 0) {
      this.errorContrasena = true;
      return false;
    } else {

      const contContr = validator.isStrongPassword(this.usuario['contrasena'], {
        minLength: 8,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1,
        returnScore: false,
        pointsPerUnique: 1,
        pointsPerRepeat: 0.5,
        pointsForContainingLower: 10,
        pointsForContainingUpper: 10,
        pointsForContainingNumber: 10,
        pointsForContainingSymbol: 10,
      })
      if (contContr == false) {
        console.log("Entro");
        this.errorContrasena = true;
        this.strContrasena = "La Contraseña es insegura. (long 8, 1+ Num, 1+ Mayus, 1+ Minus, 1+ Simb)";
        return false;
      } else {
        this.errorContrasena = false;

      }


    }






    return true;
  }

  esEmailValido(email: string): boolean {
    let mailValido = false;
    'use strict';

    var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (email.match(EMAIL_REGEX)) {
      mailValido = true;
    }
    return mailValido;
  }

  patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        // if control is empty return no error
        return null;
      }

      // test the value of the control against the regexp supplied
      const valid = regex.test(control.value);

      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }

  crear() {

    if (this.controlErrores()) {
      delete this.usuario.id;

      delete this.usuario.fingreso;
      this.usuario.session_token = '';
      const en: string = this.usuario.contrasena as string;
      const hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(en));
      const md5 = hash.toString(CryptoJS.enc.Hex)
      this.usuario.contrasena = md5;
      this.usuariosService.guardarUsuario(this.usuario,this.archivos[0])
        .subscribe(
          res => {
           
            
            this.usuariosService.getAuth(this.usuario.email, md5).
            subscribe((resp: any) => {

              const datos: any = jwt_decode(resp);
          
                  this.usuariosService.enviarToken(resp,this.usuario.email)
                  .subscribe(
                    res => {
                      this.router.navigate(['/login']);
                    },
                    err => {
                     
                    }
                  )
             

            }, (err) => {

              this.errorEmail = true;
              this.strEmail = "El Correo Electrónico No está registrado";

            }
            );
          },
          err => {
            this.errorEmail = true;
            this.strEmail = "El Correo Electrónico ya existe";
            this.usuario.contrasena = '';
            console.error(err)
          }
        )

      //this.usuario.imagen=this.previsualizacion;
    }
  }

  capturarImagen(event: any): any {
    this.limpiarImage();
    const imagenCapturada = event.target.files[0];
    this.extraerBase64(imagenCapturada).then((imagen: any) =>
      this.usuario.imagen = imagen.base
    )
    this.archivos.push(imagenCapturada)
  }


  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    // try {
    const unsafeImg = window.URL.createObjectURL($event);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    const reader = new FileReader();
    reader.readAsDataURL($event);
    reader.onload = () => {
      resolve({
        base: reader.result
      });

    };
    reader.onerror = error => {
      resolve({
        base: null
      });
    };

    /*} catch (e) {
     
      return null;
    }*/
  })


  /**
   * Limpiar imagen
   */

  limpiarImage(): any {
    this.usuario.imagen = '';
    this.archivos = [];
  }

}
