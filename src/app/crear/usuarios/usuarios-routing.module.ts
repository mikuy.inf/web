import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaginaComponentUsuario } from './pagina/pagina.component';
 

const routes: Routes = [
  {path:'',component:PaginaComponentUsuario}
  
];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
