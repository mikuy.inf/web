import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { ElementosModule } from '../../elementos/elementos.module';
import { PaginaComponentUsuario } from './pagina/pagina.component';
import { FormsModule } from '@angular/forms';
import { UtilidadesModule } from '../../utilidades/utilidades.module';

@NgModule({
  declarations: [
    PaginaComponentUsuario
  ],
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    ElementosModule,
    FormsModule,
    UtilidadesModule
  ]
})
export class UsuariosModule { }
