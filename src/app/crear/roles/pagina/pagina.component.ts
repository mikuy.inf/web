import { Component, HostBinding } from '@angular/core';
import { Roles } from 'src/app/models/Roles'
import { RolesService } from 'src/app/services/roles/roles.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent {
  @HostBinding('class') clases = 'row';

  rol: Roles = {
    id: 0,
    nombre: '',
    descripcion: ''
  };

  edit: boolean = false;

  constructor(private rolesService: RolesService, 
    private router: Router, 
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    if(params['id']){
      this.rolesService.getRol(params['id'])
      .subscribe(
        res=>{
          
          this.rol = res;
          this.edit = true;
        },
        err=>console.error(err)
      )
     } 
  }

  saveNewRol() {
    
    delete this.rol.id;
    this.rolesService.guardarRol(this.rol)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/roles']);
        },
        err => console.error(err)
      )
  }

  actualizar() {
    
    this.rolesService.actualizarRol(this.rol.id, this.rol)
      .subscribe(
        res => { 
          console.log(res);
          this.router.navigate(['/roles']);
        },
        err => console.error(err)
      )
  }
}
