import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './productos-routing.module';
import { ElementosModule } from '../../elementos/elementos.module';
import { PaginaComponent } from './pagina/pagina.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PaginaComponent
  ],
  imports: [
    CommonModule,
    RolesRoutingModule,
    ElementosModule,
    FormsModule
  ]
})
export class RolesModule { }
