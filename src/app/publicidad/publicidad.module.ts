import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicidadRoutingModule } from './publicidad-routing.module';
import { PaginaComponent } from './pagina/pagina.component';
import { ElementosModule } from '../elementos/elementos.module';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UtilidadesModule } from '../utilidades/utilidades.module';

@NgModule({
  declarations: [
    PaginaComponent

  ],
  imports: [
    CommonModule,
    PublicidadRoutingModule,
    ElementosModule,
    FormsModule,
    NgbPaginationModule,
    NgbModule,
    UtilidadesModule
  ]
})
export class PublicidadModule { }
