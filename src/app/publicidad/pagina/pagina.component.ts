import { Component, OnInit,ElementRef } from '@angular/core';
import { NgbModal, NgbAlertModule, NgbDatepickerModule, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Publicidad } from 'src/app/models/Publicidad';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { DomSanitizer } from '@angular/platform-browser';
import { PublicidadService } from 'src/app/services/publicidad/publicidad.service';
import jwt_decode from "jwt-decode";
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  btnModificar = 0;
  btnCrear = 1;
  page = 1;
  pageSize = 12;
  model: any;
  modelf: any;
  publicidades: any = [];
  nombreEliminar = '';
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;
  edit: boolean = false;


  publicidad: Publicidad = {
    id: 0,
    nombre: '',
    finicio: '',
    ffin: '',
    imagen: '',
    usuario: 0,
  }

  publicidadBlanco: Publicidad = {
    id: 0,
    nombre: '',
    finicio: '',
    ffin: '',
    imagen: '',
    usuario: 0,
  }

  public archivos: any = []

  nombreB: string = '';

  longNombre: number | undefined = 0;
  longFinicio: number | undefined = 0;
  longFfin: number | undefined = 0;


  strNombre: string = "* Nombre Obligatorio";
  strFinicio: string = "* F. Inicio Obligatorio";
  strFfin: string = "* F. Fin Obligatorio";


  errorNombre: boolean = false;
  errorFinicio: boolean = false;
  errorFfin: boolean = false;
  private idUs: number = 0;

  constructor(private publicidadService: PublicidadService,
    private funcionesService: FuncionesService,
    public modal: NgbModal,
    private mantenimientoService: MantenimientoService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private usuariosService: UsuariosService,
    private el: ElementRef
    ) {


  }



  ngOnInit(): void {
    const token = localStorage.getItem('token');

    if (token) {

      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];

      this.litarPublicidad();


      this.cargarFuncion();

    } else {
      this.router.navigate(['/login']);
    }


  }

  controlErrores(): boolean {

    this.longNombre = this.publicidad['nombre']?.length;
    this.longFinicio = this.publicidad['finicio']?.length;
    this.longFfin = this.publicidad['ffin']?.length;




    if (this.longNombre == 0) {
      this.errorNombre = true;
      this.strNombre = "El Nombre es Obligatorio";
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longFinicio == 0) {
      this.errorFinicio = true;
      this.strFinicio = "La F. Inicio es Obligatoria";
      return false;
    } else {
      this.errorFinicio = false;

    }


    if (this.longFfin == 0) {
      this.errorFfin = true;
      this.strFfin = "La F. Fin es Obligatoria";
      return false;
    } else {
      this.errorFfin = false;

    }


    return true;
  }

  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'publicidad').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarPublicidad() {
    this.publicidadService.getPublicidades().subscribe(
      res => {

        this.publicidades = res;
        console.log(res);
      },
      err => console.error(err)
    );
  }

  generarPdf() {
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Publicidad';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []

    this.publicidades.forEach((element, index, array) => {
      info.push([element.id, element.nombre, element.finicio, element.ffin])

    })

    autoTable(doc, {
      head: [["Id", "Nombre", "Fecha Inicio", "Fecha Fin"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_publicidad.pdf");
  }


  soloFecha(antes: string) {
    const nueva = antes.split("T");
    if (nueva.length > 0) {
      return nueva[0]
    } else {
      return antes
    }
  }



  crear() {
    //if (this.controlErrores()){
      this.usuariosService.controlSesion();
    delete this.publicidad.id;
    var envfinicio = this.model['year'] + "-" + this.model['month'] + "-" + this.model['day'];
    var envffin = this.modelf['year'] + "-" + this.modelf['month'] + "-" + this.modelf['day'];

    var envfinicio1 = new Date(this.model['year'], this.model['month'] - 1, this.model['day']); //crea un objeto fecha con la fecha de inicio
    var envffin1 = new Date(this.modelf['year'], this.modelf['month'] - 1, this.modelf['day']); //crea un objeto fecha con la fecha de fin

    this.errorFinicio = false;
      this.strFinicio = "";
    if (envfinicio1 > envffin1) { //compara si la fecha de inicio es mayor a la fecha de fin
      
      this.errorFinicio = true;
      this.strFinicio = "La F. Inicio no puede ser mayor que la F. Fin";
      return; 
    }else{
      this.publicidad.finicio = envfinicio;
      this.publicidad.ffin = envffin;
      this.publicidad.usuario = this.idUs;
      console.log(this.archivos[0])
      this.publicidadService.guardarPublicidad(this.publicidad,this.archivos[0])
        .subscribe(
          res => {
            this.guardarMantenimiento("Crear", "Publicidad");
            window.location.reload();
          },
          err => console.error(err)
        )
    }

   
    
  }

  seleccionarPublicidad(id: string) {
    this.usuariosService.controlSesion();
    this.publicidadService.getPublicidad(id).subscribe(
      res => {
        this.btnModificar = 1;
        this.btnCrear = 0;
        this.publicidad = res[0];
        console.log(res[0])
        //this.model = this.publicidad['finicio'];
        //console.log(this.publicidad['finicio'])

        const [year, month, day] = this.publicidad['finicio'].split('-');
        const obj = {
          year: parseInt(year), month: parseInt(month), day:
            parseInt(day.split(' ')[0].trim())
        };
        this.model = obj;

        const [yearf, monthf, dayf] = this.publicidad['ffin'].split('-');
        const objf = {
          year: parseInt(yearf), month: parseInt(monthf), day:
            parseInt(dayf.split(' ')[0].trim())
        };
        this.modelf = objf;



      },
      err => console.error(err)
    );
  }

  actualizar() {
    this.usuariosService.controlSesion();

    var envfinicio = this.model['year'] + "-" + this.model['month'] + "-" + this.model['day'];
    var envffin = this.modelf['year'] + "-" + this.modelf['month'] + "-" + this.modelf['day'];

    var envfinicio1 = new Date(this.model['year'], this.model['month'] - 1, this.model['day']); //crea un objeto fecha con la fecha de inicio
    var envffin1 = new Date(this.modelf['year'], this.modelf['month'] - 1, this.modelf['day']); //crea un objeto fecha con la fecha de fin

    this.errorFinicio = false;
      this.strFinicio = "";
    if (envfinicio1 > envffin1) { //compara si la fecha de inicio es mayor a la fecha de fin
      
      this.errorFinicio = true;
      this.strFinicio = "La F. Inicio no puede ser mayor que la F. Fin";
      console.log("Error")
      return; 
    }else{
    this.publicidad.finicio = envfinicio;
    this.publicidad.ffin = envffin;
    this.publicidadService.actualizarPublicidad(this.publicidad, this.archivos[0])
      .subscribe(
        res => {
          this.guardarMantenimiento("Actualizar", "Publicidad");
          window.location.reload();
        },
        err => console.error(err)
      )
    }
  }

  isFechaActualInRange(finicio: string, ffin: string): boolean {
    const fechaActual = new Date();
    const inicio = new Date(finicio);
    const fin = new Date(ffin);
    return fechaActual >= inicio && fechaActual <= fin;
  }
  
  cancelar() {
    this.errorFinicio = false;
      this.strFinicio = "";
    this.usuariosService.controlSesion();
    this.limpiarImage();
    const inputImagen = this.el.nativeElement.querySelector('#imagen');
    inputImagen.value = null;
    const inputFin = this.el.nativeElement.querySelector('#finicio');
    inputFin.value = null;
    const inputFfin = this.el.nativeElement.querySelector('#ffin');
    inputFfin.value = null;
    this.publicidad = { ...this.publicidadBlanco};
    this.btnModificar = 0;
    this.btnCrear = 1;
  }

  eliminarPublicidad(id: string) {
    this.errorFinicio = false;
      this.strFinicio = "";
    this.usuariosService.controlSesion();
    this.publicidadService.eliminarPublicidad(id)
      .subscribe(
        res => {

          this.guardarMantenimiento("Eliminar", "Publicidad");
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarPublicidad(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  capturarImagen(event: any): any {
    this.limpiarImage();
    this.usuariosService.controlSesion();
    const imagenCapturada = event.target.files[0];
    this.extraerBase64(imagenCapturada).then((imagen: any) =>
      this.publicidad.imagen = imagen.base
    )
    this.archivos.push(imagenCapturada)
  }


  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    // try {
    const unsafeImg = window.URL.createObjectURL($event);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    const reader = new FileReader();
    reader.readAsDataURL($event);
    reader.onload = () => {
      resolve({
        base: reader.result
      });

    };
    reader.onerror = error => {
      resolve({
        base: null
      });
    };

    /*} catch (e) {
     
      return null;
    }*/
  })


  /**
   * Limpiar imagen
   */

  limpiarImage(): any {
    this.publicidad.imagen = '';
    this.archivos = [];
  }

  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.publicidadService.getPublicidadXNombre(this.nombreB).subscribe(
        res => {
          this.publicidades = res;
          this.cancelar()
          //this.roles=this.roles.slice(0,1);
        },
        err => {
          //console.error(err)
          this.publicidades = [];
        }
      );
    } else {
      this.litarPublicidad();
    }

  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }

}
