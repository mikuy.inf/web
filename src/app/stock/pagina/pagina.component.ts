import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Productos } from 'src/app/models/Productos';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { ProductosService } from 'src/app/services/productos/productos.service';
import { StockService } from 'src/app/services/stock/stock.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MantenimientoService } from 'src/app/services/mantenimiento/mantenimiento.service';
import jwt_decode from "jwt-decode";
import { Router, ActivatedRoute } from '@angular/router';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { Stock } from 'src/app/models/Stock';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  private fechaActual: Date;
  private intervalo: Subscription;
  btnModificar = 0;
  btnCrear = 1;
  page = 1;
  pageSize = 12;
  vaFInicio: string = '';
  vaFFin: string = '';
  errorFecha: string = '';
  estadoFecha: Boolean = false;
  productos: any = [];
  categorias: any = [];
  stocks: any = []; 
  iva: any = [];
  nombreEliminar = '';
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;
  valorSeleccionado: string="3";
  edit: boolean = false;

  stock: Stock = {
    id: 0,
    producto: 0,
    nombreproducto:'',
    usuario: 0,
    nombreusuario:'',
    fecha: '',
    proceso: 0,
    valor: 0,
  }

  stockBlanco: Stock = {
    id: 0,
    producto: 0,
    nombreproducto:'',
    usuario: 0,
    nombreusuario:'',
    fecha: '',
    proceso: 0,
    valor: 0,
  }

  public archivos: any = []

  nombreB: string = '';

  longValor: number | undefined = 0;

  strValor: string = "* Valor Obligatorio";

  errorValor: boolean = false;

  strProducto: string = "* El Producto es Obligatorio";
  errorProducto: boolean = false;

  private idUs: number = 0;

  constructor(private productosService: ProductosService,
    private categoriaService: CategoriaService,
    private funcionesService: FuncionesService,
    private stockService: StockService,
    public modal: NgbModal,
    private mantenimientoService: MantenimientoService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private usuariosService: UsuariosService) { }

  ngOnInit(): void {
    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);

      this.idUs = datos[0]['id'];

      this.litarProductos();
      this.litarStock();

      this.cargarFuncion();

      //this.intervalo = interval(1000).subscribe(() => {
        this.fechaActual = new Date();
      //});

    } else {
      this.router.navigate(['/login']);
    }

  }

  controlErrores(): boolean {

   
    if (this.stock.producto == null || this.stock.producto == 0) {
      this.errorProducto = true;

      return false;
    } else {
      this.errorProducto = false;

    }


    return true;
  }

  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun(this.idUs.toString(), 'producto').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarProductos() {
    this.productosService.getProductos().subscribe(
      res => {
        this.productos = res;
      },
      err => console.error(err)
    );
  }

  generarPdf() {
    this.usuariosService.controlSesion();
    var doc = new jsPDF('p', 'pt', 'a4');
    var img = new Image()
    img.src = '../../../assets/Logos/Logomikuy.png'
    doc.addImage(img, 'png', 40, 30, 80, 30)
    const header = 'Reporte de Stock';

    doc.text(header, 225, 45, { baseline: 'top' });
    let info = []
    let contador=0
    let nomP="";
    let auxi=0
    let tot=0
    this.stocks.forEach((element, index, array) => {
      contador=contador+1
      if(contador==1){
        nomP = element.nombreproducto
      }else{
        if(element.nombreproducto!=nomP){
          auxi=1
        }
      }
      let procesoText = '';
      switch (element.proceso) {
        case '0':
          procesoText = 'Ingreso';
          tot=tot+Number(element.valor)
          break;
        case '1':
          procesoText = 'Venta';
          tot=tot-Number(element.valor)
          break;
        case '2':
          procesoText = 'Cancelado';
          tot=tot+Number(element.valor)
          break;
        default:
          procesoText = '';
      }
      
      info.push([element.id, element.nombreproducto, element.nombreusuario, element.fecha, procesoText, element.valor])

    })
    if(auxi==0 ){
      info.push(["", "", "", "", "", "______"])
      if(this.valorSeleccionado=="3"){
        
      }else{
        if(tot<0){
          tot=tot*-1;
        }
      }
      
      info.push(["", "", "", "", "TOTAL:", tot.toFixed(2)])
    }
    
    autoTable(doc, {
      head: [["Id", "Producto", "Usuario", "Fecha", "Proceso", "Valor"]],
      margin: { top: 100 },
      body: info
    });
    doc.save("reporte_stock.pdf");
  }



  litarStock() {
    this.stockService.getStocks().subscribe(
      res => {
        this.stocks = res;
      },
      err => console.error(err)
    );
  }

  crear() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {
      delete this.stock.id;
 
      delete this.stock.nombreproducto;
      delete this.stock.nombreusuario;
      delete this.stock.fecha;
      this.stock.usuario =this.idUs;
      this.stockService.guardarStock(this.stock)
        .subscribe(
          res => {
            this.guardarMantenimiento("Ingresar", "Stock");
           // window.location.reload();
           this.fechaActual = new Date();
           this.litarStock();
           this.cancelar();
          },
          err => console.error(err)
        )
    }
  }

  seleccionarStock(id: string) {
    this.usuariosService.controlSesion();
    this.stockService.getStock(id).subscribe(
      res => {
        this.stock = res[0];
        this.btnModificar = 1;
        this.btnCrear = 0;
      },
      err => console.error(err)
    );
  }

  actualizar() {

    this.usuariosService.controlSesion();
    delete this.stock.nombreproducto;
    delete this.stock.nombreusuario;
    delete this.stock.fecha;
    this.stock.usuario =this.idUs;
    this.stockService.actualizarStock(this.stock)
      .subscribe(
        res => {
          this.guardarMantenimiento("Actualizar", "Stock");
          //window.location.reload();
          this.fechaActual = new Date();
           this.litarStock();
           this.cancelar();
        },
        err => console.error(err)
      )
  }

  eliminarStock(id: string) {
    this.usuariosService.controlSesion();
    this.stockService.eliminarStock(id)
      .subscribe(
        res => {

          this.guardarMantenimiento("Eliminar", "Stock");
          //window.location.reload();
          this.fechaActual = new Date();
           this.litarStock();
           this.cancelar();
        },
        err => console.error(err)
      )
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.usuariosService.controlSesion();
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminarStock(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  cancelar() {
    this.usuariosService.controlSesion();
    this.stock = { ...this.stockBlanco};
    this.btnModificar = 0;
    this.btnCrear = 1;
  }

  validarEntero(event: any) {
    const pattern = /^[0-9]*$/; // Expresión regular para números enteros
    const inputVal = event.target.value;

    // Verificar si el valor coincide con el patrón
    if (!pattern.test(inputVal)) {
      // Si el valor no coincide, eliminar los caracteres no válidos
      const validEntero = inputVal.match(pattern);
      event.target.value = validEntero ? validEntero[0] : '';
    }
  }

  public buscarxNombre() {
    this.usuariosService.controlSesion();
    if (this.nombreB.length > 0) {
      this.stockService.getStockXProducto(this.nombreB).subscribe(
        res => {
          console.log(res)
          this.stocks = res;
          this.cancelar()
        },
        err => {
          console.log("error")
          console.log(err)
          this.stocks = [];
        }
      );
    } else {
      this.litarStock();
    }

  }

  guardarMantenimiento(proceso: String, tabla: String) {
    try {
      this.usuariosService.controlSesion();
      this.mantenimientoService.guardarMantenimiento(this.idUs.toString(), proceso, tabla)
        .subscribe(
          res => { },
          err => console.error(err)
        )
    } catch { }
  }

  buscarPorFechas() {
    this.usuariosService.controlSesion();
    
    this.estadoFecha = false
    if (this.vaFInicio.length > 0) {
      if (this.vaFFin.length <= 0) {
        this.errorFecha = "Las fechas no pueden estar vacías"
        this.estadoFecha = true
        return;
      }
    }
    if (this.vaFFin.length > 0) {
      if (this.vaFInicio.length <= 0) {
        this.errorFecha = "Las fechas no pueden estar vacías"
        this.estadoFecha = true
        return;
      }
    }

    if (this.vaFInicio.length <= 0 && this.vaFFin.length <= 0) {
      this.stockService.getStockSinFecha(this.nombreB,this.valorSeleccionado).subscribe(
        res => {
          this.stocks = res;

        },
        err => { this.stocks = [] }
      );
    }else{
      this.estadoFecha = false
      var envfinicio = this.vaFInicio['year'] + "-" + this.vaFInicio['month'] + "-" + this.vaFInicio['day'];
      var envffin = this.vaFFin['year'] + "-" + this.vaFFin['month'] + "-" + this.vaFFin['day'];
      let fIn = new Date(envfinicio);
      let fFin = new Date(envffin);
      if (fIn.getTime() > fFin.getTime()) {
        this.errorFecha = "Las fecha de Inicio no puede ser mayor a la Fecha Final"
        this.estadoFecha = true
        return;
      }
      this.stockService.getStockXFecha(envfinicio, envffin,this.nombreB,this.valorSeleccionado).subscribe(
        res => {
          this.stocks = res;

        },
        err => { this.stocks = [] }
      );
    }
   
  }

  buscarTodos() {
    window.location.reload();
  }

  procesoSeleccionado(event: Event) {
    this.valorSeleccionado = (event.target as HTMLSelectElement).value;
    
  }

  calcularDiferenciaMinutos(fechaString: Date): number {

    console.log("entro")
    const fecha = new Date(fechaString);
    const diferenciaMilisegundos = this.fechaActual.getTime() - fecha.getTime();

    const valor=Math.floor(diferenciaMilisegundos / (1000 * 60))
    console.log(valor)
    return valor;
  }
  

}
