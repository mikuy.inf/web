import { Component, HostBinding, ViewChild } from '@angular/core';
import { Usuarios } from 'src/app/models/Usuarios'
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service'
import { Router, ActivatedRoute } from '@angular/router';
import { data } from 'jquery';
import { RolesService } from 'src/app/services/roles/roles.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as CryptoJS from 'crypto-js';
import jwt_decode from "jwt-decode";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import validator from 'validator';
import { environment } from 'src/environments/environment';
import { EmmitterService } from 'src/app/services/emitter/emitter.service';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponentUsuario {
  @HostBinding('class') clases = 'row';
  @ViewChild('contentFac', { static: false }) contentFac;

  roles: any = [];
  ocultar = 0;
  usuarios: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(),

  };

  vista = false
  vista1 = false
  usuario: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(Date.now() + 24 * 60 * 60 * 1000),

  };

  datoBlanco: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(Date.now() + 24 * 60 * 60 * 1000),

  };

  longNombre: number | undefined = 0;
  longApellido: number | undefined = 0;
  longEmail: number | undefined = 0;
  longCedula: number | undefined = 0;
  longTelefono: number | undefined = 0;
  longDireccion: number | undefined = 0;
  longContrasena: number | undefined = 0;

  contrasena: string = "";
  strNombre: string = "* Nombre Obligatorio";
  strApellido: string = "* Apellido Obligatorio";
  strEmail: string = "* Email Obligatorio";
  strCedula: string = "* Cédula Obligatoria";
  strTelefono: string = "* Teléfono Obligatorio";
  strDireccion: string = "* Dirección Obligatorio";
  strContrasena: string = "* Contraseña Obligatorio";
  strContrasena1: string = "* Contraseñas No son Iguales";
  strRol: string = "* Rol Obligatorio";
  miEmail: string = "";

  errorNombre: boolean = false;
  errorApellido: boolean = false;
  errorEmail: boolean = false;
  errorCedula: boolean = false;
  errorTelefono: boolean = false;
  errorDireccion: boolean = false;
  errorContrasena: boolean = false;
  errorContrasena1: boolean = false;
  errorRol: boolean = false;

  public archivos: any = []

  edit: boolean = false;

  constructor(private usuariosService: UsuariosService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rolesService: RolesService,
    private sanitizer: DomSanitizer,
    public modalF: NgbModal,
    public _evetEmiter: EmmitterService,) { }

  ngOnInit() {

    const params = this.activatedRoute.snapshot.params;
    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
      const datos: any = jwt_decode(token);
      var id = datos[0]['id'];
      this.seleccionarUsuario(id);
    } else {
      this.router.navigate(['/login']);
    }


  }

  vistaPas() {
    this.vista = !this.vista
  }
  vistaPas1() {
    this.vista1 = !this.vista1
  }

  seleccionarUsuario(id: string) {
    this.limpiarImage();
    this.usuariosService.controlSesion();
    this.usuariosService.getUsuario(id).subscribe(
      res => {
        this.usuario = res[0];
        this.usuario.contrasena = ""
        this.miEmail=this.usuario.email;

      },
      err => console.error(err)
    );
  }

  validarCedula(cedula: string) {

    if (cedula.length === 10) {


      const digitoRegion = cedula.substring(0, 2);


      if (digitoRegion >= String(1) && digitoRegion <= String(24)) {


        const ultimoDigito = Number(cedula.substring(9, 10));


        const pares = Number(cedula.substring(1, 2)) + Number(cedula.substring(3, 4)) + Number(cedula.substring(5, 6)) + Number(cedula.substring(7, 8));


        let numeroUno: any = cedula.substring(0, 1);
        numeroUno = (numeroUno * 2);
        if (numeroUno > 9) {
          numeroUno = (numeroUno - 9);
        }

        let numeroTres: any = cedula.substring(2, 3);
        numeroTres = (numeroTres * 2);
        if (numeroTres > 9) {
          numeroTres = (numeroTres - 9);
        }

        let numeroCinco: any = cedula.substring(4, 5);
        numeroCinco = (numeroCinco * 2);
        if (numeroCinco > 9) {
          numeroCinco = (numeroCinco - 9);
        }

        let numeroSiete: any = cedula.substring(6, 7);
        numeroSiete = (numeroSiete * 2);
        if (numeroSiete > 9) {
          numeroSiete = (numeroSiete - 9);
        }

        let numeroNueve: any = cedula.substring(8, 9);
        numeroNueve = (numeroNueve * 2);
        if (numeroNueve > 9) {
          numeroNueve = (numeroNueve - 9);
        }

        const impares = numeroUno + numeroTres + numeroCinco + numeroSiete + numeroNueve;

        // Suma total
        const sumaTotal = (pares + impares);


        const primerDigitoSuma = String(sumaTotal).substring(0, 1);


        const decena = (Number(primerDigitoSuma) + 1) * 10;


        let digitoValidador = decena - sumaTotal;


        if (digitoValidador === 10) {
          digitoValidador = 0;
        }


        if (digitoValidador === ultimoDigito) {
          return true;
        } else {
          return false;
        }

      } else {

        return false;
      }
    } else {

      return false;
    }

  }

  controlErrores(): boolean {

    this.longNombre = this.usuario['nombre']?.length;
    this.longApellido = this.usuario['apellido']?.length;
    this.longEmail = this.usuario['email']?.length;
    this.longDireccion = this.usuario['direccion']?.length;
    this.longContrasena= this.usuario['contrasena']?.length;
   
    if (this.longNombre == 0) {
      this.errorNombre = true;
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longApellido == 0) {
      this.errorApellido = true;
      return false;
    } else {
      this.errorApellido = false;

    }

    if (this.longEmail == 0) {
      this.errorEmail = true;
      this.strEmail = "* Email Obligatorio";
      return false;
    } else {
      this.errorEmail = false;

    }

    const valEma: string = this.usuario['email'] as string;

    if (!this.esEmailValido(valEma)) {
      this.errorEmail = true;
      this.strEmail = "El Correo Electrónico es Incorrecto";
      return false;
    } else {
      this.errorEmail = false;

    }

    if (this.longDireccion == 0) {
      this.errorDireccion = true;
      return false;
    } else {
      this.errorDireccion = false;

    }
    console.log(this.longContrasena)
    console.log("******************")
    if (this.longContrasena > 0) {
      const contContr = validator.isStrongPassword(this.usuario['contrasena'], {
        minLength: 8,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1,
        returnScore: false,
        pointsPerUnique: 1,
        pointsPerRepeat: 0.5,
        pointsForContainingLower: 10,
        pointsForContainingUpper: 10,
        pointsForContainingNumber: 10,
        pointsForContainingSymbol: 10,
      })
      console.log(this.usuario['contrasena'])
      console.log("------------>")
      if (contContr == false) {
       
        this.errorContrasena = true;
        this.strContrasena = "La Contraseña es insegura. (long 8, 1+ Num, 1+ Mayus, 1+ Minus, 1+ Simb)";
        return false;
      } else {
        this.errorContrasena = false;
        this.strContrasena = "La Contraseña es obligatoria";
        if (this.usuario['contrasena']!=this.contrasena){
          this.errorContrasena1 = true;
          this.strContrasena1 = "Las Contraseñas no coinciden";
          return false;
        }else{
          this.errorContrasena1 = false;
        }
      }


    }

    

    return true;
  }

  esEmailValido(email: string): boolean {
    let mailValido = false;
    'use strict';

    var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (email.match(EMAIL_REGEX)) {
      mailValido = true;
    }
    return mailValido;
  }

  actualizar() {
    this.usuariosService.controlSesion();
    if (this.controlErrores()) {

      if (this.usuario.contrasena != '') {
        const en: string = this.usuario.contrasena as string;
        const hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(en));
        const md5 = hash.toString(CryptoJS.enc.Hex)

        this.usuario.contrasena = md5;

      } else {
      }
      var email=1
      console.log(this.miEmail)
      console.log(this.usuario.email)
      if(this.miEmail==this.usuario.email){
        email=0
        console.log("entro")
      }
      console.log(email)
      this.usuariosService.actualizarUsuario(this.usuario,this.archivos[0],email)
        .subscribe(
          res => {
            
            if (res['ok'] === false && res['mensaje'].includes('Duplicate entry')) {
                this.errorEmail = true;
                this.strEmail = "El Correo Electrónico ya existe";
            } else {
              if (res['ok'] === true && res['mensaje'].includes('Proceso Correcto')) {
                this.openF("Datos Actualizados. Para ver los cambios en la información actualizada es necesario cerrar sesión e ingresar nuevamente.");
              } else {
                console.log(res['mensaje']);
              }
            }
           
          },
          err =>{
            console.error(err)
            
            
          }
           
        )
    }
  }

  openF(content: any) {
    this.usuariosService.controlSesion();
    this.modalF.open(this.contentFac, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {
       
      if(this.miEmail!=this.usuario.email){
        this.usuariosService.enviarToken("token",this.usuario.email)
                  .subscribe(
                    res => {
                     this.cerrarSesion();
                     //console.log("enviando token");
                    },
                    err => {
                     
                    }
                  )
      }else{
        this.cerrarSesion();
       //console.log("sin enviar");
      }
        
        
      },
      (reason) => {
      },
    );
  }

  capturarImagen(event: any): any {
    this.usuariosService.controlSesion();
    this.limpiarImage();
    const imagenCapturada = event.target.files[0];
    this.extraerBase64(imagenCapturada).then((imagen: any) =>
      this.usuario.imagen = imagen.base
    )
    this.archivos.push(imagenCapturada)
  }


  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    // try {
    const unsafeImg = window.URL.createObjectURL($event);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    const reader = new FileReader();
    reader.readAsDataURL($event);
    reader.onload = () => {
      resolve({
        base: reader.result
      });

    };
    reader.onerror = error => {
      resolve({
        base: null
      });
    };

    /*} catch (e) {
     
      return null;
    }*/
  })


  /**
   * Limpiar imagen
   */

  limpiarImage(): any {
    this.usuario.imagen = '';
    this.archivos = [];
  }

  cerrarSesion() {
    environment.login = true
    console.log("cerrar")
    this.usuariosService.logout();
    window.location.reload();
    this._evetEmiter.setHeader(false)
    this.router.navigate(['/']);
  }

  cancelar(){
    this.router.navigate(['/']);
  }

}
