import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { ElementosModule } from '../elementos/elementos.module';
import { PaginaComponentUsuario } from './pagina/pagina.component';
import { FormsModule } from '@angular/forms';
import { UtilidadesModule } from '../utilidades/utilidades.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    PaginaComponentUsuario
  ],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    ElementosModule,
    FormsModule,
    UtilidadesModule,
    NgbModule
  ]
})
export class PerfilModule { }
