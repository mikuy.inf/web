import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactenosRoutingModule } from './contactenos-routing.module';
import { PaginaComponent } from './pagina/pagina.component';
import { ElementosModule } from '../elementos/elementos.module';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UtilidadesModule } from '../utilidades/utilidades.module';

@NgModule({
  declarations: [
    PaginaComponent
  ],
  imports: [
    CommonModule,
    ContactenosRoutingModule,
    ElementosModule,
    FormsModule,
    NgbPaginationModule,
    NgbModule,
    UtilidadesModule
  ]
})
export class ContactenosModule { }
