import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Router, ActivatedRoute } from '@angular/router';
import { VideosService } from 'src/app/services/videos/videos.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service'


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css'],
  providers: [NgbCarouselConfig]
})
export class PaginaComponent implements OnInit {

  title = 'ng-carousel-demo';


  public nombre: string = "";
  public email: string = "";
  public detalle: string = "";
  strNombre: string = "* Nombre Obligatorio";
  strDetalle: string = "* Detalle Obligatorio";
  strEmail: string = "* Email Obligatorio";


  errorNombre: boolean = false;
  errorDetalle: boolean = false;
  errorEmail: boolean = false;

  longNombre: number | undefined = 0;
  longDetalle: number | undefined = 0;
  longEmail: number | undefined = 0;

  images = [

    { title: '', short: '', src: "../../../assets/Fotografias/Foto portada web 1.jpg" },

    { title: '', short: '', src: "../../../assets/Fotografias/Foto portada web 2.jpg" },

    { title: '', short: '', src: "./../../assets/Fotografias/Foto portada web 3.jpg" }

  ];

  constructor(private videosService: VideosService,
    public modal: NgbModal,
    private sanitizer: DomSanitizer,
    private router: Router,
    config: NgbCarouselConfig, private usuariosService: UsuariosService) {
    config.interval = 2000;

    config.keyboard = true;

    config.pauseOnHover = true;
  }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
    }


  }



  enviar() {

    if (this.controlErrores()) {


      this.usuariosService.enviarContacto(this.nombre, this.detalle, this.email)
        .subscribe(
          res => {
            console.log(res)
            window.location.reload();
          },
          err => {
            console.log(err)
            this.errorDetalle = true;
            this.strDetalle = "Error al enviar Información";

          }
        )

      //this.usuario.imagen=this.previsualizacion;
    }else{
      console.log("con errores")
    }


  }

  controlErrores(): boolean {

    this.longNombre = this.nombre.length;
    this.longDetalle = this.detalle.length;
    this.longEmail = this.email.length;

    if (this.longNombre == 0) {
      this.errorNombre = true;
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longDetalle == 0) {
      this.errorDetalle = true;
      return false;
    } else {
      this.errorDetalle = false;

    }

    if (this.longEmail == 0) {
      this.errorEmail = true;
      this.strEmail = "* Email Obligatorio";
      return false;
    } else {
      this.errorEmail = false;

    }

    const valEma: string = this.email;

    if (!this.esEmailValido(valEma)) {
      this.errorEmail = true;
      this.strEmail = "El Correo Electrónico es Incorrecto";
      return false;
    } else {
      this.errorEmail = false;

    }

    return true;
  }

  esEmailValido(email: string): boolean {
    let mailValido = false;
    'use strict';

    var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (email.match(EMAIL_REGEX)) {
      mailValido = true;
    }
    return mailValido;
  }




}
