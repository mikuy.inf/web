import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import jwt_decode from "jwt-decode";
import { Usuarios } from 'src/app/models/Usuarios';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  value: any = [];
  mensaje: string = 'Validando';
  error: boolean = false;
  usuario: Usuarios = {
    id: 0,
    nombre: '',
    apellido: '',
    email: '',
    imagen: '',
    direccion: '',
    contrasena: '',
    fingreso: new Date(Date.now() + 24 * 60 * 60 * 1000),

    session_token: '',
  }
  constructor(private rutaActiva: ActivatedRoute,
    private usuariosService: UsuariosService) { }
  ngOnInit() {
    this.value = this.rutaActiva.snapshot.params;

    try {
      const datos: any = jwt_decode(this.value['value']);

      this.usuario.id = datos;
      this.usuario.session_token = this.value['value']
      this.actualizar();
    } catch {
      this.mensaje = "Ups! Hay un problema al validar su cuenta, comuniquese con el administrador del sistema.";
      this.error = true;
    }
  }

  actualizar() {
    delete this.usuario.nombre;
    delete this.usuario.apellido;
    delete this.usuario.email;
    delete this.usuario.imagen;
    delete this.usuario.direccion;
    delete this.usuario.fingreso;

    delete this.usuario.contrasena;

    const en: string = this.usuario.email as string;
        const hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(en));
        const md5 = hash.toString(CryptoJS.enc.Hex)

    this.usuariosService.actualizarUsuario1(this.usuario.id, this.usuario)
      .subscribe(
        res => {

          this.mensaje = "El Usuario se ha validado correctamente, ya puede ingresar al sistema.";

        },
        err => {
          this.error = true;
          this.mensaje = "Ups! Hay un problema al validar su cuenta, comuniquese con el administrador del sistema.";
        }
      )



  }
}
