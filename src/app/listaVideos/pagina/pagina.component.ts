import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Videos } from 'src/app/models/Videos';
import { DomSanitizer,SafeResourceUrl } from '@angular/platform-browser';
import { VentasFService } from 'src/app/services/ventasf/ventasf.service';
import jwt_decode from "jwt-decode";
import { VentasF } from 'src/app/models/VentasF';
import { VentasDService } from 'src/app/services/ventasd/ventasd.service';
import { VentasD } from 'src/app/models/VentasD';
import { Router, ActivatedRoute } from '@angular/router';
import { VideosService } from 'src/app/services/videos/videos.service';

 
@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit{

  public videos: any= [];
  public safeSrc: SafeResourceUrl = '';
  nombreB: string = '';
  
  constructor(private videosService: VideosService,
    public modal: NgbModal,
    private sanitizer: DomSanitizer,
    private router: Router){}

    ngOnInit(): void {
      
      this.litarVideos();
      
      
    }
 
    mostrar(url:string){
      this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/"+url);
      return this.safeSrc;
    }

    litarVideos() {
      this.videosService.getVideos().subscribe(
        res => {

          this.videos = res;
          
        },
        err => console.error(err)
      );
    }

    public buscarxNombre() {
      if (this.nombreB.length > 0) {
        this.videosService.getVideoXNombre(this.nombreB).subscribe(
          res => {
            this.videos = res;
            //this.roles=this.roles.slice(0,1);
          },
          err => console.error(err)
        );
      } else {
        this.litarVideos();
      }
  
    }
    
}
