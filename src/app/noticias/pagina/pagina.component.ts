import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Router, ActivatedRoute } from '@angular/router';
import { VideosService } from 'src/app/services/videos/videos.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css'],
  providers: [NgbCarouselConfig]
})
export class PaginaComponent implements OnInit {

  title = 'ng-carousel-demo';

  

  images = [
    { title: 'EXPOSICIÓN FERIA ALFARO', short: "El jueves 13 de abril del 2023, el proyecto Producción, seguridad alimentaria y nutricional post pandemia en los cantones Colta y Riobamba , de cual forma parte Mikuy se presentó a un exposición en la feria Alfaro, donde las facultades de la Espoch estuvieron presentes, Nutrición, Recursos Naturales y Diseño gráfico. Las mesas presentaban los productos de consorcio desde insuflados de maiz, quinua y trigo hasta bebidas elaboradas de granos ancestrales. ", src: "../../../assets/imagenes/3.jpg" },
    
  ];

  constructor(private videosService: VideosService,
    public modal: NgbModal,
    private sanitizer: DomSanitizer,
    private router: Router,
    private usuariosService: UsuariosService,
    config: NgbCarouselConfig) {
    config.interval = 2000;

    config.keyboard = true;

    config.pauseOnHover = true;
  }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
    }

  }




}
