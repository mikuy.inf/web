import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UsuariosService } from '../services/usuarios/usuarios.service';
import { EmmitterService } from '../services/emitter/emitter.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private usuariosServicio:UsuariosService,
    private router:Router,
    private _evetEmiter:EmmitterService){}
  canActivate(): boolean{
    console.log("Auth ingreso");
    if(!this.usuariosServicio.isAuth()){
      console.log("Token incorrecto o ha expirado");
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

  canAccessProductos():boolean{
    if(!this._evetEmiter.getMenuProductos()){
      console.log("No tiene Acceso a Productos");
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
  
}
