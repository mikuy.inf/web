import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UsuariosService } from '../services/usuarios/usuarios.service';
import { EmmitterService } from '../services/emitter/emitter.service';

@Injectable({
  providedIn: 'root'
})
export class AccesoProductosGuard implements CanActivate {

  constructor(private usuariosServicio: UsuariosService,
    private router:Router,
    private _evetEmiter:EmmitterService){

  }
  canActivate(): boolean{
    console.log("Ingreso");
    if(!this._evetEmiter.getMenuProductos()){
      console.log("No tiene Acceso a Productos");
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
  
}
