import { Component, OnInit, HostBinding } from '@angular/core';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Funciones } from 'src/app/models/Funciones';
import { FuncionesService } from 'src/app/services/funciones/funciones.service';
import { Categorias } from 'src/app/models/Categorias';
import { DomSanitizer } from '@angular/platform-browser';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';


@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent {

  @HostBinding('class') classes = 'row';

  page = 1;
  pageSize = 7;
  roles: any = [];
  vcrear: boolean = false;
  vactualizar: boolean = false;
  vleer: boolean = false;
  veliminar: boolean = false;
  vopciones: boolean = true;

  categorias: any = [];

  categoria: Categorias = {
    id: 0,
    nombre: '',
    descripcion: '',
    imagen: ''
  };

  funcion: Funciones = {
    id: 0,
    rol: 0,
    tabla: '',
    crear: 0,
    actualizar: 0,
    leer: 0,
    eliminar: 0
  };

  categoriaBlanco: Categorias = {
    id: 0,
    nombre: '',
    descripcion: '',
    imagen: '',
  };

  closeResult = '';
  nombreEliminar = '';
  public archivos: any = []

  nombreB: string = '';

  longNombre: number | undefined = 0;
  longDescripcion: number | undefined = 0;

  strNombre: string = "* Nombre Obligatorio";
  strDescripcion: string = "* Descripción Obligatorio";
  errorNombre: boolean = false;
  errorDescripcion: boolean = false;

  constructor(private categoriasService: CategoriaService,
    public modal: NgbModal,
    private funcionesService: FuncionesService,
    private sanitizer: DomSanitizer,
    private usuariosService: UsuariosService) { }

  ngOnInit(): void {
    const token = localStorage.getItem('token');

    if (token) {
      this.usuariosService.controlSesion();
    }

    this.litarCategoria();
    this.cargarFuncion();
  }

  capturarImagen(event: any): any {
    this.limpiarImage();
    const imagenCapturada = event.target.files[0];
    this.extraerBase64(imagenCapturada).then((imagen: any) =>
      this.categoria.imagen = imagen.base
    )
    this.archivos.push(imagenCapturada)
  }

  limpiarImage(): any {
    this.categoria.imagen = '';
    this.archivos = [];
  }

  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    // try {
    const unsafeImg = window.URL.createObjectURL($event);
    const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
    const reader = new FileReader();
    reader.readAsDataURL($event);
    reader.onload = () => {
      resolve({
        base: reader.result
      });

    };
    reader.onerror = error => {
      resolve({
        base: null
      });
    };

    /*} catch (e) {
     
      return null;
    }*/
  })

  controlErrores(): boolean {

    this.longNombre = this.categoria['nombre']?.length;
    this.longDescripcion = this.categoria['descripcion']?.length;

    if (this.longNombre == 0) {
      this.errorNombre = true;
      this.strNombre = "El Nombre es Obligatorio";
      return false;
    } else {
      this.errorNombre = false;

    }

    if (this.longDescripcion == 0) {
      this.errorDescripcion = true;
      this.strDescripcion = "La Descripcion es Obligatoria";
      return false;
    } else {
      this.errorDescripcion = false;

    }

    return true;
  }


  cargarImagenCategoria(index: number) {
    const datos = this.categorias[index];
    console.log("Error al cargar")
    datos.imagen = '../../../assets/categoria.png';
  }

  cargarFuncion() {
    let tabla: any = []
    this.funcionesService.getRFun('1', 'categoria').subscribe(
      res => {
        tabla = res;
        Object.keys(res).map((key) => {
          if (tabla[key]['leer'] == 1) {
            this.vleer = true;
          }
          if (tabla[key]['crear'] == 1) {
            this.vcrear = true;
          }
          if (tabla[key]['actualizar'] == 1) {
            this.vactualizar = true;
          }
          if (tabla[key]['eliminar'] == 1) {
            this.veliminar = true;
          }
          if (tabla[key]['crear'] == 0 && tabla[key]['actualizar'] == 0) {
            this.vopciones = false;
          }
        })

      },
      err => console.error(err)
    );

  }

  litarCategoria() {
    this.categoriasService.getCategorias().subscribe(
      res => {
        this.categorias = res;
      },
      err => console.error(err)
    );
  }

  eliminar(id: string) {
    this.categoriasService.eliminarCategoria(id)
      .subscribe(
        res => {
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  crear() {
    if (this.controlErrores()) {
      delete this.categoria.id;

      this.categoriasService.guardarCategoria(this.categoria,this.archivos[0])
        .subscribe(
          res => {


            this.categoria = this.categoriaBlanco;
            window.location.reload();
          },
          err => console.error(err)
        )
    }
  }

  seleccionarCategoria(id: string) {
    this.categoriasService.getCategoria(id).subscribe(
      res => {
        this.categoria = res;
      },
      err => console.error(err)
    );
  }

  actualizar() {



    this.categoriasService.actualizarCategoria(this.categoria,this.archivos[0])
      .subscribe(
        res => {
          //console.log(res);
          //this.litarCategoria();
          this.categoria = this.categoriaBlanco;
          window.location.reload();
        },
        err => console.error(err)
      )
  }

  open(content: any, id: string, nombre: string) {
    this.nombreEliminar = nombre;
    this.modal.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {

        this.eliminar(id);
      },
      (reason) => {
        //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public buscarxNombre() {
    if (this.nombreB.length > 0) {
      this.categoriasService.getCategoriasXNombre(this.nombreB).subscribe(
        res => {
          this.categorias = res;
          //this.roles=this.roles.slice(0,1);
        },
        err => console.error(err)
      );
    } else {
      this.litarCategoria();
    }

  }

}
