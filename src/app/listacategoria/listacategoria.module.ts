import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListaCategoriaRoutingModule } from './listacategoria-routing.module';
import { PaginaComponent } from './pagina/pagina.component';
import { ElementosModule } from '../elementos/elementos.module';
import { RouterLinkActive } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { UtilidadesModule } from '../utilidades/utilidades.module';

@NgModule({
  declarations: [
    PaginaComponent
  ],
  imports: [
    CommonModule,
    ListaCategoriaRoutingModule,
    ElementosModule,
    RouterLinkActive,
    FormsModule,
    NgbPaginationModule,
    UtilidadesModule
  ]
})
export class ListaCategoriasModule { }
