"use strict";(self.webpackChunkmikuy=self.webpackChunkmikuy||[]).push([[431],{4072:(g,l,e)=>{e.d(l,{w:()=>t});var d=e(6895),i=e(1571);class t{}t.\u0275fac=function(m){return new(m||t)},t.\u0275mod=i.oAB({type:t}),t.\u0275inj=i.cJS({imports:[d.ez]})},7431:(g,l,e)=>{e.r(l),e.d(l,{NoticiasModule:()=>u});var d=e(6895),i=e(646),t=e(9837),o=e(1571),m=e(131),U=e(1481),v=e(6592),p=e(5749);function f(a,s){if(1&a&&(o.TgZ(0,"div",10),o._UZ(1,"img",11),o.TgZ(2,"div",12),o._UZ(3,"img",13),o.qZA(),o.TgZ(4,"div",14)(5,"h1",15),o._uU(6),o.qZA(),o.TgZ(7,"h6",16),o._uU(8),o.qZA()()(),o._UZ(9,"div",17)),2&a){const n=o.oxw().$implicit;o.xp6(1),o.Q6J("src",n.src,o.LSH),o.xp6(5),o.hij("",n.title," "),o.xp6(2),o.Oqu(n.short)}}function h(a,s){1&a&&o.YNc(0,f,10,3,"ng-template",9)}class c{constructor(s,n,E,y,F,Z){this.videosService=s,this.modal=n,this.sanitizer=E,this.router=y,this.usuariosService=F,this.title="ng-carousel-demo",this.images=[{title:"EXPOSICI\xd3N FERIA ALFARO",short:"El jueves 13 de abril del 2023, el proyecto Producci\xf3n, seguridad alimentaria y nutricional post pandemia en los cantones Colta y Riobamba , de cual forma parte Mikuy se present\xf3 a un exposici\xf3n en la feria Alfaro, donde las facultades de la Espoch estuvieron presentes, Nutrici\xf3n, Recursos Naturales y Dise\xf1o gr\xe1fico. Las mesas presentaban los productos de consorcio desde insuflados de maiz, quinua y trigo hasta bebidas elaboradas de granos ancestrales. ",src:"../../../assets/imagenes/3.jpg"}],Z.interval=2e3,Z.keyboard=!0,Z.pauseOnHover=!0}ngOnInit(){localStorage.getItem("token")&&this.usuariosService.controlSesion()}}c.\u0275fac=function(s){return new(s||c)(o.Y36(m.Y),o.Y36(t.FF),o.Y36(U.H7),o.Y36(i.F0),o.Y36(v.J),o.Y36(t.Lu))},c.\u0275cmp=o.Xpm({type:c,selectors:[["app-pagina"]],features:[o._Bn([t.Lu])],decls:68,vars:1,consts:[[1,"col-sm-12","titulo","text-center"],[1,""],[4,"ngFor","ngForOf"],[1,"fondocafe"],[1,"row"],[1,"col-md-4","col-sm-12","text-center"],[1,"fondoblanco","hover-bg-white"],[1,"tituloEventos"],[1,"subtituloEventos"],["ngbSlide",""],[1,"col-sm-12","contenedorImgN"],["alt","Noticias","width","100%","height","90%",3,"src"],[1,"centradoArteImgN"],["src","../../../../assets/fondos/fondoArte.png","alt","Arte","width","100%","height","250px"],[1,"centradoTextoImgN",2,"bottom","15%"],[1,"font-italic","mb-0","fuente-mon",2,"color","#755018"],[1,"fuente-mon"],[1,"carousel-caption"]],template:function(s,n){1&s&&(o.TgZ(0,"div")(1,"div",0),o._uU(2," Noticias Mikuy "),o.qZA()(),o.TgZ(3,"div",1)(4,"ngb-carousel"),o.YNc(5,h,1,0,null,2),o.qZA()(),o.TgZ(6,"div")(7,"div",0),o._uU(8," Eventos Mikuy "),o.qZA()(),o._UZ(9,"app-separador"),o.TgZ(10,"div",3),o._UZ(11,"br"),o.TgZ(12,"div",4)(13,"div",5)(14,"div",6),o._UZ(15,"br"),o.TgZ(16,"h2",7),o._uU(17,"Exposici\xf3n Sumak Joven"),o.qZA(),o._UZ(18,"br"),o.TgZ(19,"h4",8),o._uU(20," Martes"),o._UZ(21,"br"),o._uU(22," 12 am"),o._UZ(23,"br"),o._uU(24," Escuelas"),o._UZ(25,"br"),o._uU(26," Radiof\xf3nicas"),o._UZ(27,"br"),o._uU(28," Populareds "),o.qZA(),o._UZ(29,"br")(30,"br"),o.qZA()(),o.TgZ(31,"div",5)(32,"div",6),o._UZ(33,"br"),o.TgZ(34,"h2",7),o._uU(35,"Inauguraci\xf3n Sumak Joven"),o.qZA(),o._UZ(36,"br"),o.TgZ(37,"h4",8),o._uU(38," Martes"),o._UZ(39,"br"),o._uU(40," 12 am"),o._UZ(41,"br"),o._uU(42," Escuelas"),o._UZ(43,"br"),o._uU(44," Radiof\xf3nicas"),o._UZ(45,"br"),o._uU(46," Populareds "),o.qZA(),o._UZ(47,"br")(48,"br"),o.qZA()(),o.TgZ(49,"div",5)(50,"div",6),o._UZ(51,"br"),o.TgZ(52,"h2",7),o._uU(53,"Exposici\xf3n Sumak Joven"),o.qZA(),o._UZ(54,"br"),o.TgZ(55,"h4",8),o._uU(56," Martes"),o._UZ(57,"br"),o._uU(58," 12 am"),o._UZ(59,"br"),o._uU(60," Escuelas"),o._UZ(61,"br"),o._uU(62," Radiof\xf3nicas"),o._UZ(63,"br"),o._uU(64," Populareds "),o.qZA(),o._UZ(65,"br")(66,"br"),o.qZA()()(),o._UZ(67,"br"),o.qZA()),2&s&&(o.xp6(5),o.Q6J("ngForOf",n.images))},dependencies:[d.sg,t.uo,t.xl,p.e]});const b=[{path:"",component:c}];class r{}r.\u0275fac=function(s){return new(s||r)},r.\u0275mod=o.oAB({type:r}),r.\u0275inj=o.cJS({imports:[i.Bz.forChild(b),i.Bz]});var A=e(4072),T=e(433),_=e(8390);class u{}u.\u0275fac=function(s){return new(s||u)},u.\u0275mod=o.oAB({type:u}),u.\u0275inj=o.cJS({imports:[d.ez,r,A.w,T.u5,t.jF,t.IJ,_.U]})}}]);